﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lendfoundry.Security.Identity.Tests.Fakes
{
    public class FakeTokenHandler : ITokenHandler
    {
        public FakeTokenHandler(ISecurityTokenConfig securityTokenConfig,ITokenReader tokenReader,ITokenWriter tokenWriter)
        {
            SecurityTokenConfig = securityTokenConfig;
            TokenReader = tokenReader;
            TokenWriter = TokenWriter;
        }
        private ISecurityTokenConfig SecurityTokenConfig { get; set; }
        private ITokenReader TokenReader { get; set; }
        private ITokenWriter TokenWriter { get; set; }
        public IToken Issue(string tenant, string issuer)
        {
            return new Token {
                IssuedAt = DateTime.Now,
                Issuer = issuer,
                Expiration = DateTime.Now.AddDays(30),
                Subject = "kinjal.s",
                Tenant = tenant,
                Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM"

            };
        }

        public IToken Issue(string tenant, string issuer, DateTime? expiration, string subject, string[] scope)
        {
            return new Token
            {
                IssuedAt = DateTime.Now,
                Issuer = issuer,
                Expiration = expiration,
                Subject = subject,
                Tenant = tenant,
                Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM",
                Scope = scope
            };
        }

        public IToken Parse(string value = null)
        {
            if(value == "scope-null")
                return new Token
                {
                    IssuedAt = DateTime.Now,
                    Issuer = "kinjal.s",
                    Expiration = DateTime.Now.AddDays(30),
                    Subject = "kinjal.s",
                    Tenant = "my-tenant",
                    
          
                    Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM"

                };
            else if (value == "token-expiry")
                return new Token
                {
                    IssuedAt = DateTime.Now,
                    Issuer = "kinjal.s",
                 
                    Subject = "kinjal.s",
                    Tenant = "my-tenant",


                    Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM"

                };
            else 

            return new Token
            {
                IssuedAt = DateTime.Now,
                Issuer = "kinjal.s",
                Expiration = DateTime.Now.AddDays(30),
                Subject = "kinjal.s",
                Tenant = "my-tenant",
                Scope= new string[] { "{*any}",
    "back-office" }
            ,
                Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM"
                
            };
        }

        public void Write(IToken token)
        {
            throw new NotImplementedException();
        }
    }
}
