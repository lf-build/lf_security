﻿using LendFoundry.Security.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Lendfoundry.Security.Identity.Tests.Fakes
{
    public class FakeUserRepository : IUserRepository
    {
        public List<IUser> InMemoryData { get; set; } = new List<IUser>();
        public async Task Add(IUser user)
        {
            user.Id = (InMemoryData.Count + 1).ToString();
            if (user.Username == "kinjal.s.expired")
            {
                user.PasswordExpirationDate = DateTime.UtcNow.AddDays(-5);
            }
            if (user.Username == "kinjal.s.failedattempt")
            {
                user.FailedLoginAttempts = 2;
                user.LastFailedLoginAttempt = DateTime.UtcNow;
            }
            if (user.Username == "kinjal.s.resettoken")
            {
                user.ResetToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJMZW5kRm91bmRyeSIsImlhdCI6IjIwMTgtMDItMTJUMTA6MjA6MTcuNzMwMTcyNloiLCJleHAiOm51bGwsInN1YiI6bnVsbCwidGVuYW50IjoidGVuYW50MDEiLCJzY29wZSI6bnVsbCwiSXNWYWxpZCI6dHJ1ZX0.7ZucWZTryNvMFTeu9HMpqItjcq7FOHKqdq0L4md_JvU";
                user.ResetTokenExpiration = DateTime.UtcNow.AddDays(24);
            }
            await Task.Run(() => InMemoryData.Add(user));
        }



        public async Task<IUser> Get(string username)
        {
            return await Task.Run(() =>
           {
               return InMemoryData
                   .AsQueryable().FirstOrDefault(x => x.Username == username);
           });
        }



        public async Task ChangePassword(IUserInfo user, string password, DateTimeOffset? passwordExpirationDate, List<UserPassword> oldPasswords)
        {
            await Task.Run(() =>
          {
              var userInfo = InMemoryData.Where(p => p.Username == user.Username).FirstOrDefault();

              var index = InMemoryData.IndexOf(userInfo);
              userInfo.Password = password;
              userInfo.PasswordExpirationDate = passwordExpirationDate;
              userInfo.PasswordHistory = oldPasswords;
              InMemoryData[index] = userInfo;


          });
        }



        public async Task ChangeName(IUserInfo user, string name)
        {
            await Task.Run(() =>
           {
               var userInfo = InMemoryData.Where(p => p.Id == user.Id).FirstOrDefault();

               var index = InMemoryData.IndexOf(userInfo);
               userInfo.Name = user.Name;
               InMemoryData[index] = userInfo;
           });
        }



        public async Task<ICollection<IUser>> GetUsersByRoles(List<string> roles)
        {
            return await Task.FromResult<ICollection<IUser>>(InMemoryData);
        }

        public async Task<IEnumerable<IUserInfo>> GetUsersByPasswordExpiring(int days)
        {
            var passwordExpirationThreasHold = DateTimeOffset.UtcNow.Date.AddDays(days);

            return await Task.Run(() =>
           {
               return new List<IUserInfo>(InMemoryData
                   .AsQueryable().Where(x => x.PasswordExpirationDate == passwordExpirationThreasHold).Select(
                       u => new UserInfo()
                       {
                           Username = u.Username,
                           Email = u.Email,
                           Id = u.Id,
                           IsActive = u.IsActive,
                           Name = u.Name,
                           Roles = u.Roles
                       })).AsEnumerable();
           });
        }

        public async Task<ICollection<IUser>> All()
        {
            return await Task.FromResult<ICollection<IUser>>(InMemoryData);
        }

        public async Task Update(IUserInfo user)
        {
            await Task.Run(() =>
            {
                var userInfo = InMemoryData.Where(p => p.Id == user.Id).FirstOrDefault();

                var index = InMemoryData.IndexOf(userInfo);
                userInfo = MapUserData(user);
                InMemoryData[index] = userInfo;
            });
        }

        private IUser MapUserData(IUserInfo user)
        {
            if (user == null)
                return null;

            IUser mappedUserData = new User();
            mappedUserData.Id = user.Id;

            mappedUserData.Name = user.Name;
            mappedUserData.Email = user.Email;
            mappedUserData.Username = user.Username;
            mappedUserData.Roles = user.Roles;

            return mappedUserData;
        }
        public async Task Remove(IUserInfo user)
        {
            await Task.Run(() =>
            {
                var userInfo = InMemoryData.Where(p => p.Id == user.Id).FirstOrDefault();
                InMemoryData.Remove(userInfo);
            });
        }

        async Task<ICollection<IUser>> IUserRepository.GetUsersByRoles(List<string> roles)
        {
            return await Task.FromResult<ICollection<IUser>>(InMemoryData);
        }
        async Task<IUser> IUserRepository.GetById(string id)
        {
            var users = All().Result;
            return await Task.FromResult(users.Where(p => p.Id == id).FirstOrDefault());
        }

        public async Task SetIsActive(string name, bool IsActive)
        {
            await Task.Run(() =>
            {
                var userInfo = InMemoryData.Where(p => p.Username == name).FirstOrDefault();
                var index = InMemoryData.IndexOf(userInfo);
                userInfo.IsActive = IsActive;
                InMemoryData[index] = userInfo;
            });


        }

        public async Task<IEnumerable<IUser>> AllWithRole(string role)
        {
            return await Task.FromResult<ICollection<IUser>>(InMemoryData);
        }

        public async Task SetResetToken(IUser user, string token, DateTime expiration)
        {
            await Task.Run(() =>
            {
                var userInfo = InMemoryData.Where(p => p.Username == user.Name).FirstOrDefault();
                var index = InMemoryData.IndexOf(userInfo);
                userInfo.ResetToken = token;
                InMemoryData[index] = userInfo;
            });
        }

        public async Task AddOrUpdateUserRoles(string name, string[] roles)
        {
            await Task.Run(() =>
            {
                var userInfo = InMemoryData.Where(p => p.Username == name).FirstOrDefault();
                var index = InMemoryData.IndexOf(userInfo);
                userInfo.Roles = roles;
                InMemoryData[index] = userInfo;
            });

        }

        public async Task UpdateFailedLoginAttempts(IUser user)
        {
            await Task.Run(() =>
            {
                var userInfo = InMemoryData.Where(p => p.Username == user.Username).FirstOrDefault();
                var index = InMemoryData.IndexOf(userInfo);
                userInfo.FailedLoginAttempts = user.FailedLoginAttempts;
                userInfo.LastFailedLoginAttempt = user.LastFailedLoginAttempt;
                InMemoryData[index] = userInfo;
            });
        }


    }
}
