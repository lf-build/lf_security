﻿using LendFoundry.Security.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
namespace Lendfoundry.Security.Identity.Tests.Fakes
{
    public class FakeUserOtpRepository : IUserOtpRepository
    {

        public List<IUserOtp> InMemoryData { get; set; } = new List<IUserOtp>();

        public FakeUserOtpRepository() { }
        public FakeUserOtpRepository(IUserOtp userOtp)
        {
            InMemoryData.Add(userOtp);
        }
        public async Task Add(IUserOtp userOtp)
        {
            await Task.Run(() => InMemoryData.Add(userOtp));
        }

        public async Task Delete(string userName)
        {
            await Task.Run(() =>
            {

                var userInfo = InMemoryData.Where(p => p.Username == userName).FirstOrDefault();
                InMemoryData.Remove(userInfo);
            });
        }

        public async Task<IUserOtp> GetByOtpCode(string userName, string otpCode)
        {

            return await Task.Run(() => InMemoryData.Where(p => p.Username == userName && p.OtpCode == otpCode).FirstOrDefault());
        }

        public async Task<IUserOtp> GetByUserName(string userName)
        {
            return await Task.Run(() => InMemoryData.Where(p => p.Username == userName).FirstOrDefault());
        }

        public async Task Update(IUserOtp userOtp)
        {
            await Task.Run(() =>
            {
                var userInfo = InMemoryData.Where(p => p.Id == userOtp.Id).FirstOrDefault();

                var index = InMemoryData.IndexOf(userInfo);
                userInfo = MapOtpUserData(userOtp);
                InMemoryData[index] = userInfo;
            });
        }

        private IUserOtp MapOtpUserData(IUserOtp user)
        {
            if (user == null)
                return null;

            IUserOtp mappedUserData = new UserOtp();
            mappedUserData.Id = user.Id;

            mappedUserData.Username = user.Username;
            mappedUserData.OtpCode = user.OtpCode;

            return mappedUserData;
        }
    }
}
