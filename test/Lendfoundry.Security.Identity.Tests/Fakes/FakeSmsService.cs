﻿using LendFoundry.Sms;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lendfoundry.Security.Identity.Tests.Fakes
{
    public class FakeSmsService : ISmsService
    {
        public async Task<SmsResult> Send(string entityType, string entityId, string templateName, string templateVersion, object data)
        {
            return await Task.FromResult(new SmsResult { SmsReferenceNumber = "12344" });
        }

        public async Task<SmsResult> Send(string entityType, string entityId, string templateName, object data)
        {
            return await Task.FromResult(new SmsResult { SmsReferenceNumber = "12344" });
        }

        public async Task<SmsResult> SendSms(string entityType, string entityId, object data)
        {
            return await Task.FromResult( new SmsResult { SmsReferenceNumber = "12344" });
        }

        public async Task<SmsResult> ResendOtp(string entityType, string entityId, OtpResendRequest otpResendRequest)
        {
            return await Task.FromResult( new SmsResult { SmsReferenceNumber = "12344" });
        }

        public async Task<SmsResult> SendOtp(string entityType, string entityId, OtpSendRequest otpSendRequest)
        {
            return await Task.FromResult( new SmsResult { SmsReferenceNumber = "12344" });
        }

        public async Task<SmsResult> VerifyOtp(string entityType, string entityId, OtpVerifyRequest otpVerifyRequest)
        {
            return await Task.FromResult( new SmsResult { SmsReferenceNumber = "12344" });
        }
    }
}
