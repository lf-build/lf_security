﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Security.Identity;
using Xunit;

namespace Lendfoundry.Security.Identity.Tests
{
    public class RoleToPermissionTest
    {
        private IRoleToPermissionConverter GetConverter()
        {
            var configurationJson = "{											     " +
                                    "	\"sessionTimeout\": 10,												 " +
                                    "	\"tokenTimeout\": 60,                                                " +
                                    "	\"roles\": {                                                         " +
                                    "		\"VP Of Sales\": {                                               " +
                                    "			\"permissions\": [\"vpOfSales\"],                               " +
                                    "			\"roles\": {                                                 " +
                                    "				\"Loan Coordinator Manager\": {                          " +
                                    "					\"permissions\": [\"LoanCoordinatorManager\"],                       " +
                                    "					\"roles\": {                                         " +
                                    "						\"Loan Coordinator\": {                          " +
                                    "							\"permissions\": [\"loancordinator\"]                " +
                                    "						}                                                " +
                                    "					}                                                    " +
                                    "				},                                                       " +
                                    "				\"Sr. Relationship Manager\": {                          " +
                                    "					\"permissions\": [\"srrelationshipmanager\"],                       " +
                                    "					\"roles\": {                                         " +
                                    "						\"Relationship Manager\": {                      " +
                                    "							\"permissions\": [\"relationshipmanager\"]                " +
                                    "						}                                                " +
                                    "					}                                                    " +
                                    "				}                                                        " +
                                    "			}                                                            " +
                                    "		},                                                               " +
                                    "		\"VP Of Operations\": {                                          " +
                                    "			\"permissions\": [\"vpofoperations\"],                               " +
                                    "			\"roles\": {                                                 " +
                                    "				\"Sr. Loan Processor\": {                                " +
                                    "					\"permissions\": [\"srloanprocessor\"],                       " +
                                    "					\"roles\": {                                         " +
                                    "						\"Loan Processor\": {                            " +
                                    "							\"permissions\": [\"loanprocessor\"]                " +
                                    "						}                                                " +
                                    "					}                                                    " +
                                    "				},                                                       " +
                                    "				\"Credit Specialist Manager\": {                         " +
                                    "					\"permissions\": [\"creditspecialistmanager\"],                       " +
                                    "					\"roles\": {                                         " +
                                    "						\"Credit Specialist\": {                         " +
                                    "							\"permissions\": [\"creditspecialist\"]                " +
                                    "						}                                                " +
                                    "					}                                                    " +
                                    "				},                                                       " +
                                    "				\"Merchant Underwriter Manager\": {                      " +
                                    "					\"permissions\": [\"merchantunderwritermanager\"],                       " +
                                    "					\"roles\": {                                         " +
                                    "						\"Merchant Underwriter\": {                      " +
                                    "							\"permissions\": [\"merchantunderwriter\"]                " +
                                    "						}                                                " +
                                    "					}                                                    " +
                                    "				},                                                       " +
                                    "				\"Customer Service Manager\": {                          " +
                                    "					\"permissions\": [\"customerservicemanager\"],                       " +
                                    "					\"roles\": {                                         " +
                                    "						\"Customer Service Representative\": {           " +
                                    "							\"permissions\": [\"customerservicerepresentative\",\"customerrepresentative1\"]                " +
                                    "						}                                                " +
                                    "					}                                                    " +
                                    "				},                                                       " +
                                    "				\"QA Manager\": {                                        " +
                                    "					\"permissions\": [\"qamanager\"],                       " +
                                    "					\"roles\": {                                         " +
                                    "						\"QA\": {                                        " +
                                    "							\"permissions\": [\"qa\",\"q2\"]                " +
                                    "						}                                                " +
                                    "					}                                                    " +
                                    "				}                                                        " +
                                    "			}                                                            " +
                                    "		},                                                               " +
                                    "		\"Director Of Merchant Support\": {                              " +
                                    "			\"permissions\": [\"{*any}\"],                               " +
                                    "			\"roles\": {                                                 " +
                                    "				\"Merchant Support Specialist\": {                       " +
                                    "					\"permissions\": [\"{*any}\"]                        " +
                                    "				}                                                        " +
                                    "			}                                                            " +
                                    "		},                                                               " +
                                    "		\"Marketing\": {                                                 " +
                                    "			\"permissions\": [\"{*any}\"],                               " +
                                    "			\"roles\": {                                                 " +
                                    "				\"Social Media Coordinator\": {                          " +
                                    "					\"permissions\": [\"{*any}\"]                        " +
                                    "				}                                                        " +
                                    "			}                                                            " +
                                    "		}                                                                " +
                                    "	}                                                                    " +
                                    "}                                                                       ";
            var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Configuration>(configurationJson);
            if (configuration.Roles == null || !configuration.Roles.Any())
                throw new InvalidOperationException("Roles are required");
            return new RoleToPermissionConverter(configuration);
        }

        [Fact]
        public void ItShouldReturnRoleFromTopElements()
        {
            var converter=GetConverter();
            var permission = converter.ToPermission(new List<string> { "VP Of Sales"}).ToList();
            Assert.True(permission.Count== 5);
            Assert.Contains("vpOfSales", permission);
            Assert.Contains("LoanCoordinatorManager", permission);
            Assert.Contains("loancordinator", permission);
            Assert.Contains("srrelationshipmanager", permission);
            Assert.Contains("relationshipmanager", permission);
        }

        [Fact]
        public void ItShouldReturnRoleFromBottomElements()
        {
            var converter = GetConverter();
            var permission = converter.ToPermission(new List<string> { "Loan Coordinator" }).ToList();
            Assert.True(permission.Count == 1);
            Assert.Contains("loancordinator", permission);
        }

        [Fact]
        public void ItShouldReturnRoleFromMiddleElements()
        {
            var converter = GetConverter();
            var permission = converter.ToPermission(new List<string> { "Loan Coordinator Manager" }).ToList();
            Assert.True(permission.Count == 2);
            Assert.Contains("loancordinator", permission);
            Assert.Contains("LoanCoordinatorManager", permission);

        }

        [Fact]
        public void ItShouldReturnRoleFromLAstMiddleElements()
        {
            var converter = GetConverter();
            var permission = converter.ToPermission(new List<string> { "QA" }).ToList();
            Assert.True(permission.Count == 2);
            Assert.Contains("qa", permission);
            Assert.Contains("q2", permission);

        }

        [Fact]
        public void ItShouldReturnRoleWithCaseIgnored()
        {
            var converter = GetConverter();
            var permission = converter.ToPermission(new List<string> { "qa" }).ToList();
            Assert.True(permission.Count == 2);
            Assert.Contains("qa", permission);
        }

        [Fact]
        public void ItShouldNotReturnRoleFromNotExistingRole()
        {
            var converter = GetConverter();
            var permission = converter.ToPermission(new List<string> { "ThisDoesNotExist" }).ToList();
            Assert.True(permission.Count == 0);
        }
    }
}
