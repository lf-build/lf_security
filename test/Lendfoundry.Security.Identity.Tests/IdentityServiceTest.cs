﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lendfoundry.Security.Identity.Tests.Fakes;
using LendFoundry.Configuration.Client;
using LendFoundry.Email;
using LendFoundry.EventHub;
using LendFoundry.Security.Identity;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;
using Xunit;
using LendFoundry.Sms;
using System.Security.Authentication;
using LendFoundry.Security.Identity.Api.Models;
using LendFoundry.Configuration;

namespace Lendfoundry.Security.Identity.Tests
{
    public class IdentityServiceTest
    {
        [Fact]
        public async Task LoginWhenMissingUsername()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = await BuildService();
                await service.Login(new LoginRequest() { Password = "notexist", Portal = "notExist" });
            });
        }

        [Fact]
        public async Task LoginWhenMissingPassword()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = await BuildService();
                await service.Login(new LoginRequest() { Username = "notexist", Portal = "notExist" });
            });
        }
        [Fact]
        public async Task LoginWhenMissingPortal()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = await BuildService();
                await service.Login(new LoginRequest() { Username = "notexist", Password = "notexist" });
            });
        }

        [Fact]
        public async Task LoginWhenUserNotExists()
        {
            await Assert.ThrowsAsync<InvalidUserOrPasswordException>(async () =>
            {
                var service = await BuildService();
                await service.Login(new LoginRequest() { Username = "notexist", Password = "notexist", Portal = "notExist" });
            });
        }



        [Fact]
        public async Task LoginUserHasReachedMaxFailedLoginAttempts()
        {
            await Assert.ThrowsAsync<AccountLockedException>(async () =>
            {
                var user =

                     new User
                     {
                         Email = "kinjal.s@sigmainfo.net",
                         Name = "kinjal",
                         Password = "test123",
                         PasswordSalt = "test",
                         Username = "kinjal.s.failedattempt",
                         Roles = new[] { "VP Of Sales" },
                         IsActive = true
                     };

                var service = await BuildService(new List<IUser> { user });

                await service.Login(new LoginRequest() { Username = "kinjal.s.failedattempt", Password = "test123", Portal = "borrower" });
            });
        }

        [Fact]
        public async Task LoginUserHasInValidPassword()
        {
            await Assert.ThrowsAsync<InvalidUserOrPasswordException>(async () =>
            {
                var user =

                     new User
                     {
                         Email = "kinjal.s@sigmainfo.net",
                         Name = "kinjal",
                         Password = "test123",
                         PasswordSalt = "test",
                         Username = "kinjal.s",
                         Roles = new[] { "VP Of Sales" }

                     };

                var service = await BuildService(new List<IUser> { user });

                await service.Login(new LoginRequest() { Username = "kinjal.s", Password = "noExists", Portal = "borrower" });
            });
        }

        [Fact]
        public async Task LoginUserHasPasswordExpiration()
        {
            await Assert.ThrowsAsync<PasswordExpiredException>(async () =>
            {
                var user =

                     new User
                     {
                         Email = "kinjal.s@sigmainfo.net",
                         Name = "kinjal",
                         Password = "test123",
                         PasswordSalt = "test",
                         Username = "kinjal.s.expired",
                         Roles = new[] { "VP Of Sales" },
                     };

                var service = await BuildService(new List<IUser> { user });

                await service.Login(new LoginRequest() { Username = "kinjal.s.expired", Password = "test123", Portal = "borrower" });
            });
        }

        [Fact]
        public async Task LoginUserHasRoleNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var user =

                     new User
                     {
                         Email = "kinjal.s@sigmainfo.net",
                         Name = "kinjal",
                         Password = "test123",
                         PasswordSalt = "test",
                         Username = "kinjal.s"
                     };

                var service = await BuildService(new List<IUser> { user });

                await service.Login(new LoginRequest() { Username = "kinjal.s", Password = "test123", Portal = "borrower" });
            });
        }

        [Fact]
        public async Task LoginSucceeded()
        {
            var user =

                     new User
                     {
                         Email = "kinjal.s@sigmainfo.net",
                         Name = "kinjal",
                         Password = "test123",
                         PasswordSalt = "test",
                         Username = "kinjal.s",
                         Roles = new[] { "VP Of Sales" }

                     };

            var service = await BuildService(new List<IUser> { user });


            var result = await service.Login(new LoginRequest() { Username = "kinjal.s", Password = "test123", Portal = "back-office" });

            Assert.NotNull(result);
        }


        [Fact]
        public async Task LoginUserUsernameNotAvailable()
        {
            var user =

                    new User
                    {
                        Email = "kinjal.s@sigmainfo.net",
                        Name = "kinjal",
                        Password = "test123",
                        PasswordSalt = "test",
                        Username = "kinjal.s",
                        Roles = new[] { "VP Of Sales" }

                    };

            var service = await BuildService(new List<IUser> { user });
            var result = await service.IsUsernameAvailable(string.Empty);

            Assert.True(result);
        }
        [Fact]
        public async Task LoginUserUsernameAvailable()
        {
            var user =

                    new User
                    {
                        Email = "kinjal.s@sigmainfo.net",
                        Name = "kinjal",
                        Password = "test123",
                        PasswordSalt = "test",
                        Username = "kinjal.s",
                        Roles = new[] { "VP Of Sales" }

                    };

            var service = await BuildService(new List<IUser> { user });
            var result = await service.IsUsernameAvailable("kinjal.s");

            Assert.False(result);
        }


        [Fact]
        public async Task GetCurrentUserWhenTokenInvalid()
        {

            await Assert.ThrowsAsync<AuthenticationException>(async () =>
            {
                TokenReader.Setup(c => c.Read()).Returns("12eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");
                var service = await BuildService();


                await service.GetCurrentUser();
            });
        }

        [Fact]
        public async Task GetCurrentUserWhenSucceded()
        {

            await SessionManager.Create(new Token { Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM" });

            TokenReader.Setup(c => c.Read()).Returns("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");
            var user =

                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                };

            var service = await BuildService(new List<IUser> { user });

            var result = await service.GetCurrentUser();
            Assert.NotNull(result);
        }

        [Fact]
        public async Task CreateUserWhenUserNull()
        {

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();


                await service.CreateUser(null);
            });
        }

        [Fact]
        public async Task CreateUserWhenNameNull()
        {
            var user =

                  new CreateUserRequest
                  {
                      Email = "kinjal.s@sigmainfo.net",

                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();


                await service.CreateUser(user);
            });
        }
        [Fact]
        public async Task CreateUserWhenUserNameNull()
        {
            var user =

                  new CreateUserRequest
                  {
                      Email = "kinjal.s@sigmainfo.net",
                      Name = "kinjal.s",
                      Password = "test123",
                      PasswordSalt = "test",

                      Roles = new[] { "VP Of Sales" }

                  };
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();


                await service.CreateUser(user);
            });
        }
        [Fact]
        public async Task CreateUserWhenEmailNull()
        {
            var user =

                  new CreateUserRequest
                  {

                      Name = "kinjal.s",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();


                await service.CreateUser(user);
            });
        }

        [Fact]
        public async Task CreateUserWhenPasswordNull()
        {
            var user =

                  new CreateUserRequest
                  {

                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();


                await service.CreateUser(user);
            });
        }
        [Fact]
        public async Task CreateUserWhenPasswordComplexityRulesInvalid()
        {
            var userRequest =

                  new CreateUserRequest
                  {

                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };
            Mock<IPasswordComplexityRules> passwordComplexityRules = new Mock<IPasswordComplexityRules>();
            passwordComplexityRules.Setup(c => c.Complies(It.IsAny<string>())).Returns(false);
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var user = new User
                {
                    Name = "kinjal.s",
                    Email = "kinjal.s@sigmainfo.net",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s@sigmainfo.net",
                    Roles = new[] { "VP Of Sales" }
                };

                var service = await BuildService(new List<IUser> { user }, 12, passwordComplexityRules);

                await service.CreateUser(userRequest);
            });
        }

        [Fact]
        public async Task CreateUserWhenUserAlreadyExists()
        {
            await Assert.ThrowsAsync<UsernameAlreadyExists>(async () =>
           {
               var userRequest =

                      new CreateUserRequest
                 {

                     Name = "kinjal.s",
                     Email = "kinjal.s@sigmainfo.net",
                     Password = "test123",
                     PasswordSalt = "test",
                     Username = "kinjal.s@sigmainfo.net",
                     Roles = new[] { "VP Of Sales" }

                 };


               var service = await BuildService(new List<IUser> {
                    new User{

                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }
                    }});

               await service.CreateUser(userRequest);

           });
        }

        [Fact]
        public async Task GetAllUsersSucceded()
        {
            var user =

                 new User
                 {

                     Name = "kinjal.s",
                     Email = "kinjal.s@sigmainfo.net",
                     Password = "test123",
                     PasswordSalt = "test",
                     Username = "kinjal.s@sigmainfo.net",
                     Roles = new[] { "VP Of Sales" }

                 };

            var service = await BuildService(new List<IUser> { user });


            var result = await service.GetAllUsers();
            Assert.NotNull(result);
        }

        [Fact]
        public async Task GetUsersByRolesWhenNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var user =

                      new User
                      {

                          Name = "kinjal.s",
                          Email = "kinjal.s@sigmainfo.net",
                          Password = "test123",
                          PasswordSalt = "test",
                          Username = "kinjal.s@sigmainfo.net",
                          Roles = new[] { "VP Of Sales" }

                      };


                var service = await BuildService(new List<IUser> { user });


                await service.GetUsersByRoles(null);



            });
        }
        [Fact]
        public async Task GetUsersByRolesWhenEmpty()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var user =

                      new User
                      {

                          Name = "kinjal.s",
                          Email = "kinjal.s@sigmainfo.net",
                          Password = "test123",
                          PasswordSalt = "test",
                          Username = "kinjal.s@sigmainfo.net",
                          Roles = new[] { "VP Of Sales" }

                      };


                var service = await BuildService(new List<IUser> { user });


                await service.GetUsersByRoles(new List<string> { });



            });
        }

        [Fact]
        public async Task GetUsersByRolesSucceded()
        {

            var user =

                  new User
                  {

                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };


            var service = await BuildService(new List<IUser> { user });


            var result = await service.GetUsersByRoles(new List<string> { "VP Of Sales" });
            Assert.NotNull(result);



        }

        [Fact]
        public async Task GetUsersByPasswordExpiringSucceded()
        {

            var user =

                  new User
                  {

                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };


            var service = await BuildService(new List<IUser> { user });


            var result = await service.GetUsersByPasswordExpiring();
            Assert.NotNull(result);



        }

        [Fact]
        public async Task GetUserByIdWhenIdNullOrEmpty()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {


                var service = await BuildService();


                await service.GetUserById(string.Empty);



            });
        }

        [Fact]
        public async Task GetUserByIdSucceded()
        {
            var user =

                  new User
                  {
                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };

            var service = await BuildService(new List<IUser> { user });

            var result = await service.GetUserById("1");
            Assert.NotNull(result);


        }

        [Fact]
        public async Task UpdateUserWhenUserNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {


                var service = await BuildService();


                await service.UpdateUser(null);



            });
        }
        [Fact]
        public async Task UpdateUserSucceded()
        {
            var user =

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };


            var service = await BuildService(new List<IUser> { user });

            IUserInfo userInfo = new UserInfo
            {
                Id = "1",
                Name = "kinjal.s1",
                Email = "kinjal.s1@sigmainfo.net",
                Username = "kinjal.s@sigmainfo.net",
                Roles = new[] { "VP Of Sales", "VP Of Operations" }
            };
            var result = await service.UpdateUser(userInfo);
            Assert.NotNull(result);

            result = await service.GetUserById("1");

            Assert.NotNull(result);
            Assert.Equal("kinjal.s1", result.Name);
            Assert.Equal("kinjal.s1@sigmainfo.net", result.Email);
            Assert.Equal(2, result.Roles.Count());
        }

        [Fact]
        public async Task RemoveUserWhenUserNameNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {


                var service = await BuildService();


                await service.RemoveUser(null);



            });
        }
        [Fact]
        public async Task RemoveUserWhenUserNameNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {

                var user =

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  };


                var service = await BuildService(new List<IUser> { user });

                await service.RemoveUser("kinjal.s1@sigmainfo.net");



            });
        }

        [Fact]
        public async Task RemoveUserSucceded()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  },
                  new User
                  {

                      Id = "2",
                      Name = "kinjal.s1",
                      Email = "kinjal.s1@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s1@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  }
            };


            var service = await BuildService(user);


            await service.RemoveUser("kinjal.s@sigmainfo.net");

            var result = await service.GetUserById("1");

            Assert.Null(result);

        }
        [Fact]
        public async Task ChangePasswordWhenUserNameNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {



                var service = await BuildService();

                await service.ChangePassword(null, "test123", "test1234");



            });
        }
        [Fact]
        public async Task ChangePasswordWhenCurrentPasswordNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {



                var service = await BuildService();

                await service.ChangePassword("kinjal.s@sigmainfo.net", null, "test1234");



            });
        }
        [Fact]
        public async Task ChangePasswordWhenNotNewPassword()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  }
            };



                var service = await BuildService(user);

                await service.ChangePassword("kinjal.s@sigmainfo.net", "test123", "test123");



            });
        }

        [Fact]
        public async Task ChangePasswordWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  }
            };



                var service = await BuildService(user);

                await service.ChangePassword("notfound", "test123", "test123");



            });
        }
        [Fact]
        public async Task ChangePasswordWhenSucceded()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "John.p",
                      Email = "John.p@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "John.p",
                      Roles = new[] { "VP Of Sales" }

                  }
            };



            var service = await BuildService(user);

            await service.ChangePassword("John.p", "test123", "test1234");

            var result = await service.Login(new LoginRequest() { Username = "John.p", Password = "test1234", Portal = "back-office" });

            Assert.NotNull(result);




        }

        [Fact]
        public async Task RequestPasswordResetWhenUsernameNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  }
            };



                var service = await BuildService(user);

                var requestPasswordResetRequest = new RequestPasswordResetRequest()
                {
                    Portal = "back-office"
                };
                await service.RequestPasswordReset(requestPasswordResetRequest);



            });
        }
        [Fact]
        public async Task RequestPasswordResetWhenPortalNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  }
            };



                var service = await BuildService(user);

                var requestPasswordResetRequest = new RequestPasswordResetRequest()
                {
                    Username = "kinjal.s"
                };
                await service.RequestPasswordReset(requestPasswordResetRequest);



            });
        }
        [Fact]
        public async Task RequestPasswordResetWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s@sigmainfo.net",
                      Roles = new[] { "VP Of Sales" }

                  }
            };



                var service = await BuildService(user);

                var requestPasswordResetRequest = new RequestPasswordResetRequest()
                {
                    Username = "kinjal11.s",
                    Portal = "back-office"
                };
                await service.RequestPasswordReset(requestPasswordResetRequest);



            });
        }
        [Fact]
        public async Task RequestPasswordResetWhenPortalNotFound()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" }

                  }
            };



                var service = await BuildService(user);

                var requestPasswordResetRequest = new RequestPasswordResetRequest()
                {
                    Username = "kinjal.s",
                    Portal = "back1-office"
                };
                await service.RequestPasswordReset(requestPasswordResetRequest);



            });
        }
        [Fact]
        public async Task RequestPasswordResetWhenSucceeded()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" },
                      ResetToken = "tokenold"
                  }
            };



            var service = await BuildService(user);

            var requestPasswordResetRequest = new RequestPasswordResetRequest()
            {
                Username = "kinjal.s",
                Portal = "back-office"
            };
            await service.RequestPasswordReset(requestPasswordResetRequest);
            var result = await service.GetUser(requestPasswordResetRequest.Username);
            Assert.NotNull(result);
            Assert.NotEqual("ResetToken", result.ResetToken);


        }
        [Fact]
        public async Task ResetPasswordWhenUsernameNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {



                var service = await BuildService();


                await service.ResetPassword(string.Empty, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM", "test123");



            });
        }
        [Fact]
        public async Task ResetPasswordWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {

                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" }

                  }
            };



                var service = await BuildService(user);


                await service.ResetPassword("kinjal1.s", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM", "test123");



            });
        }
        [Fact]
        public async Task ResetPasswordWhenInvalidToken()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {

                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" },
                     ResetToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJMZW5kRm91bmRyeSIsImlhdCI6IjIwMTgtMDItMTJUMTA6MjA6MTcuNzMwMTcyNloiLCJleHAiOm51bGwsInN1YiI6bnVsbCwidGVuYW50IjoidGVuYW50MDEiLCJzY29wZSI6bnVsbCwiSXNWYWxpZCI6dHJ1ZX0.7ZucWZTryNvMFTeu9HMpqItjcq7FOHKqdq0L4md_JvU",
                     ResetTokenExpiration = DateTime.UtcNow.AddDays(24)
                  }
            };



                var service = await BuildService(user);


                await service.ResetPassword("kinjal.s", "invalid", "test123");



            });
        }
        [Fact]
        public async Task ResetPasswordWhenTokenExpiried()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {

                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" },
                     ResetToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJMZW5kRm91bmRyeSIsImlhdCI6IjIwMTgtMDItMTJUMTA6MjA6MTcuNzMwMTcyNloiLCJleHAiOm51bGwsInN1YiI6bnVsbCwidGVuYW50IjoidGVuYW50MDEiLCJzY29wZSI6bnVsbCwiSXNWYWxpZCI6dHJ1ZX0.7ZucWZTryNvMFTeu9HMpqItjcq7FOHKqdq0L4md_JvU",
                     ResetTokenExpiration = DateTime.UtcNow.AddDays(-24)
                  }
            };



                var service = await BuildService(user);


                await service.ResetPassword("kinjal.s", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJMZW5kRm91bmRyeSIsImlhdCI6IjIwMTgtMDItMTJUMTA6MjA6MTcuNzMwMTcyNloiLCJleHAiOm51bGwsInN1YiI6bnVsbCwidGVuYW50IjoidGVuYW50MDEiLCJzY29wZSI6bnVsbCwiSXNWYWxpZCI6dHJ1ZX0.7ZucWZTryNvMFTeu9HMpqItjcq7FOHKqdq0L4md_JvU", "test1234");



            });
        }

        [Fact]
        public async Task ResetPasswordWhenSuceeded()
        {
            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s.resettoken",
                      Roles = new[] { "VP Of Sales" },
                  }
            };



            var service = await BuildService(user);


            await service.ResetPassword("kinjal.s.resettoken", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJMZW5kRm91bmRyeSIsImlhdCI6IjIwMTgtMDItMTJUMTA6MjA6MTcuNzMwMTcyNloiLCJleHAiOm51bGwsInN1YiI6bnVsbCwidGVuYW50IjoidGVuYW50MDEiLCJzY29wZSI6bnVsbCwiSXNWYWxpZCI6dHJ1ZX0.7ZucWZTryNvMFTeu9HMpqItjcq7FOHKqdq0L4md_JvU", "test1234x");

            var result = await service.Login(new LoginRequest() { Username = "kinjal.s.resettoken", Password = "test1234x", Portal = "back-office" });

            Assert.NotNull(result);


        }
        [Fact]
        public async Task SendOtpWhenMissingUsername()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = await BuildService();
                await service.SendOtp(new SendOtpRequest() { });
            });
        }
        [Fact]
        public async Task SendOtpWhenUserNotFound()
        {
            await Assert.ThrowsAsync<InvalidUserOrPasswordException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" }
                  }
            };

                var service = await BuildService(user);
                await service.SendOtp(new SendOtpRequest() { Username = "NotExists" });
            });
        }
        [Fact]
        public async Task OtpWhenSendMailToUser()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" }
                  }
            };
            var service = await BuildService(user, 0, null, GetTemplateConfiguration());
            var result = await service.SendOtp(new SendOtpRequest() { Username = "kinjal.s" });
            Assert.NotNull(result);

        }
        [Fact]
        public async Task OtpWhenSendSmsToUser()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" }
                  }
            };
            var service = await BuildService(user);
            var result = await service.SendOtp(new SendOtpRequest() { Username = "kinjal.s" });
            Assert.NotNull(result);

        }
        [Fact]
        public async Task OtpWhenAddUser()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" }
                  }
            };
            var service = await BuildService(user);
            var result = await service.SendOtp(new SendOtpRequest() { Username = "kinjal.s" });
            Assert.NotNull(result);

        }
        [Fact]
        public async Task OtpWhenUpdateUser()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "john.p",
                      Email = "john.p@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "john.p",
                      Roles = new[] { "VP Of Sales" }
                  }
            };
            var service = await BuildService(user);

            var result = await service.SendOtp(new SendOtpRequest() { Username = "john.p" });
            Assert.NotNull(result);

        }
        [Fact]
        public async Task OtpLoginWhenMissingUsername()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = await BuildService();
                await service.Login(new OtpLoginRequest() { Otp = "notexist", Portal = "notExist" });
            });
        }
        [Fact]
        public async Task OtpLoginWhenMissingOtp()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = await BuildService();
                await service.Login(new OtpLoginRequest() { Username = "notexist", Portal = "notExist" });
            });
        }
        [Fact]
        public async Task OtpLoginWhenMissingPortal()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = await BuildService();
                await service.Login(new OtpLoginRequest() { Username = "notexist", Otp = "notexist" });
            });
        }
        [Fact]
        public async Task OtpLoginWhenUserNotExists()
        {
            await Assert.ThrowsAsync<InvalidUserOrPasswordException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "john.p",
                      Email = "john.p@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "john.p",
                      Roles = new[] { "VP Of Sales" }
                  }
            };
                var service = await BuildService(user);
                await service.Login(new OtpLoginRequest() { Username = "notexist", Otp = "notexist", Portal = "notExist" });
            });
        }
        [Fact]
        public async Task OtpLoginWhenOtpCodeNotExists()
        {
            await Assert.ThrowsAsync<InvalidUserOrPasswordException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "john.p",
                      Email = "john.p@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "john.p",
                      Roles = new[] { "VP Of Sales" }
                  }
            };
                var service = await BuildService(user);
                await service.Login(new OtpLoginRequest() { Username = "john.p", Otp = "notexist", Portal = "notExist" });
            });
        }

        [Fact]
        public async Task OtpLoginWhenOtpCodeSuceeded()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "john.p",
                      Email = "john.p@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "john.p",
                      Roles = new[] { "VP Of Sales" }
                  }
            };
            var service = await BuildService(user);
            var result = await service.Login(new OtpLoginRequest() { Username = "john.p", Otp = "1", Portal = "back-office" });
            Assert.NotNull(result);

        }

        [Fact]
        public async Task IsAuthorizedWhenTokenNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();
                await service.IsAuthorized(null, "http://localhost");
            });
        }
        [Fact]
        public async Task IsAuthorizedWhenScopeNull()
        {

            await SessionManager.Create(new Token { Value = "scope-null" });

            TokenReader.Setup(c => c.Read()).Returns("scope-null");
            var user =

                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                };

            var service = await BuildService(new List<IUser> { user });
            var result = await service.IsAuthorized("scope-null", "http://lenfoundry.com");

            Assert.False(result);
        }

        [Fact]
        public async Task IsAuthorizedWhenSuceeded()
        {

            await SessionManager.Create(new Token
            {
                Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY",
                Scope = new string[] { "{*any}",
    "back-office" }
            });

            TokenReader.Setup(c => c.Read()).Returns("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY");
            var user =

                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                };

            var service = await BuildService(new List<IUser> { user });
            var result = await service.IsAuthorized("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY", "http://lenfoundry.com");
            Assert.True(result);
        }

        [Fact]
        public async Task IsExpiringWhenTokenNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                await SessionManager.Create(new Token
                {
                    Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY",
                    Scope = new string[] { "{*any}",
    "back-office" }
                });

                TokenReader.Setup(c => c.Read()).Returns(string.Empty);


                var service = await BuildService();
                await service.IsExpiring();
            });


        }

        [Fact]
        public async Task IsExpiringWhenInvalidToken()
        {
            await Assert.ThrowsAsync<InvalidTokenException>(async () =>
            {

                await SessionManager.Create(new Token
                {
                    Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY",
                    Scope = new string[] { "{*any}",
    "back-office" }
                });

                TokenReader.Setup(c => c.Read()).Returns("Invalid");


                var service = await BuildService();
                await service.IsExpiring();
            });


        }

        [Fact]
        public async Task IsExpiringWhenSucceded()
        {


            await SessionManager.Create(new Token
            {
                Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY",
                Scope = new string[] { "{*any}",
    "back-office" }
            });

            TokenReader.Setup(c => c.Read()).Returns("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY");


            var service = await BuildService();
            var result = await service.IsExpiring();
            Assert.False(result);


        }
        [Fact]
        public async Task IsExpiringWhenTokenExpiried()
        {
            await Assert.ThrowsAsync<InvalidTokenException>(async () =>
            {

                await SessionManager.Create(new Token
                {
                    Value = "token-expiry",
                    Scope = new string[] { "{*any}",
    "back-office" }
                });

                TokenReader.Setup(c => c.Read()).Returns("token-expiry");


                var service = await BuildService();
                await service.IsExpiring();
            });


        }





        [Fact]
        public async Task RenewTokenWhenTokenNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                await SessionManager.Create(new Token
                {
                    Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY",
                    Scope = new string[] { "{*any}",
    "back-office" }
                });

                TokenReader.Setup(c => c.Read()).Returns(string.Empty);


                var service = await BuildService();
                await service.RenewToken();
            });


        }

        [Fact]
        public async Task RenewTokenWhenInvalidToken()
        {
            await Assert.ThrowsAsync<InvalidTokenException>(async () =>
            {

                await SessionManager.Create(new Token
                {
                    Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY",
                    Scope = new string[] { "{*any}",
    "back-office" }
                });

                TokenReader.Setup(c => c.Read()).Returns("Invalid");


                var service = await BuildService();
                await service.RenewToken();
            });


        }

        [Fact]
        public async Task RenewTokenWhenSucceded()
        {


            await SessionManager.Create(new Token
            {
                Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY",
                Scope = new string[] { "{*any}",
    "back-office" }
            });

            TokenReader.Setup(c => c.Read()).Returns("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY");


            var service = await BuildService();
            var result = await service.RenewToken();
            Assert.NotNull(result);

        }
        [Fact]
        public async Task LogoutWhenTokenNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {



                var service = await BuildService();
                await service.Logout(string.Empty);
            });


        }


        [Fact]
        public async Task LogoutWhenSucceded()
        {


            await SessionManager.Create(new Token
            {
                Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY",
                Scope = new string[] { "{*any}",
    "back-office" }
            });

            TokenReader.Setup(c => c.Read()).Returns("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY");

            var user =
new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};
            var service = await BuildService(user);
            await service.Logout("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJsZW5kZm91bmRyeSIsImlhdCI6IjIwMTctMDgtMTFUMTI6NTA6MjUuMzc1OTQxWiIsImV4cCI6IjIwMTctMDgtMTFUMTM6MDA6MjUuMzcyOTUxWiIsInN1YiI6InN5c3RlbSIsInRlbmFudCI6ImZjLTM2MCIsInNjb3BlIjpbInsqYW55fSIsImJhY2stb2ZmaWNlIl0sIklzVmFsaWQiOnRydWV9.b4Z-GnoTxHAeHkfus3sCSjxZ0VeaBsgHpJU6GcaVRXY");

        }

        [Fact]
        public async Task GetAllRolesFromCurrentUserWhenUserNotFound()
        {

            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {

                await SessionManager.Create(new Token { Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM" });

                TokenReader.Setup(c => c.Read()).Returns("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");


                var service = await BuildService();

                await service.GetAllRolesFromCurrentUser();
            });
        }
        [Fact]
        public async Task GetAllRolesFromCurrentUserWhenSucceded()
        {


            await SessionManager.Create(new Token { Value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM" });

            TokenReader.Setup(c => c.Read()).Returns("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM");
            var user =
new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

            var service = await BuildService(user);

            var result = await service.GetAllRolesFromCurrentUser();
            Assert.NotNull(result);

        }



        [Fact]
        public async Task GetUserRolesWhenUsernameNull()
        {

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {



                var service = await BuildService();

                await service.GetUserRoles(string.Empty);
            });
        }
        [Fact]
        public async Task GetUserRolesWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {


                var user =
    new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

                var service = await BuildService(user);

                await service.GetUserRoles("kinjal1.s");
            });

        }

        [Fact]
        public async Task GetUserRolesWhenSucceded()
        {


            var user =
new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

            var service = await BuildService(user);

            var result = await service.GetUserRoles("kinjal.s");
            Assert.NotNull(result);


        }

        [Fact]
        public async Task GetAllRolesWhenSucceded()
        {

            var service = await BuildService();

            var result = await service.GetAllRoles();
            Assert.NotNull(result);


        }

        [Fact]
        public async Task GetChildRolesWhenSucceded()
        {

            var service = await BuildService();

            var result = await service.GetChildRoles("VP Of Sales");
            Assert.NotNull(result);


        }
        [Fact]
        public async Task GetParentRolesWhenSucceded()
        {

            var service = await BuildService();

            var result = await service.GetParentRoles("Loan Coordinator");
            Assert.NotNull(result);


        }

        [Fact]
        public async Task GetChildrenRolesWhenRolesNull()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var service = await BuildService();

                await service.GetChildrenRoles(null);
            });

        }
        [Fact]
        public async Task GetChildrenRolesWhenSucceded()
        {

            var service = await BuildService();

            var result = await service.GetChildrenRoles(new string[] { "VP Of Sales" });
            Assert.NotNull(result);

        }

        [Fact]
        public async Task GetRoleMembersWhenRoleNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {
                var service = await BuildService();

                await service.GetRoleMembers(null);
            });

        }
        [Fact]
        public async Task GetRoleMembersWhenSucceded()
        {

            var service = await BuildService();

            var result = await service.GetRoleMembers("VP Of Sales");
            Assert.NotNull(result);

        }

        [Fact]
        public async Task ActivateUserWhenUsernameNull()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {

                var service = await BuildService();
                await service.ActivateUser(string.Empty);
            });


        }
        [Fact]
        public async Task ActivateUserWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {
                var user =
                   new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

                var service = await BuildService(user);
                await service.ActivateUser("notexists");
            });


        }
        [Fact]
        public async Task ActivateUserWhenUserSucceded()
        {

            var user =
               new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

            var service = await BuildService(user);
            await service.ActivateUser("kinjal.s");

            var result = await service.GetUser("kinjal.s");
            Assert.NotNull(result);
            Assert.True(result.IsActive);

        }



        [Fact]
        public async Task DeActivateUserWhenUsernameNull()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {

                var service = await BuildService();
                await service.DeActivateUser(string.Empty);
            });


        }
        [Fact]
        public async Task DeActivateUserWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {
                var user =
                   new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

                var service = await BuildService(user);
                await service.DeActivateUser("notexists");
            });


        }
        [Fact]
        public async Task DeActivateUserWhenUserSucceded()
        {

            var user =
               new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

            var service = await BuildService(user);
            await service.DeActivateUser("kinjal.s");

            var result = await service.GetUser("kinjal.s");
            Assert.NotNull(result);
            Assert.False(result.IsActive);

        }

        [Fact]
        public async Task AssignRolesToUserWhenUsernameNull()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {

                var service = await BuildService();
                await service.AssignRolesToUser(string.Empty, new List<string> { "VP Of Sales" });
            });


        }
        [Fact]
        public async Task AssignRolesToUserWhenRoleNull()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var user =
                  new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

                var service = await BuildService(user);
                await service.AssignRolesToUser("kinjal.s", null);
            });


        }
        [Fact]
        public async Task AssignRolesToUserWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {
                var user =
                   new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

                var service = await BuildService(user);
                await service.AssignRolesToUser("notexists", new List<string> { "VP Of Sales" });
            });


        }
        [Fact]
        public async Task AssignRolesToUserWhenSucceded()
        {

            var user =
               new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

            var service = await BuildService(user);
            await service.AssignRolesToUser("kinjal.s", new List<string> { "VP Of Operations" });
            var result = await service.GetUser("kinjal.s");
            Assert.NotNull(result);
            Assert.Contains("VP Of Operations", result.Roles);


        }




        [Fact]
        public async Task RemoveRolesToUserWhenUsernameNull()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {

                var service = await BuildService();
                await service.RemoveRolesFromUser(string.Empty, new List<string> { "VP Of Sales" });
            });


        }
        [Fact]
        public async Task RemoveRolesToUserWhenRoleNull()
        {
            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                var user =
                new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

                var service = await BuildService(user);
                await service.RemoveRolesFromUser("kinjal.s", null);
            });


        }
        [Fact]
        public async Task RemoveRolesToUserWhenWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {
                var user =
                   new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

                var service = await BuildService(user);
                await service.RemoveRolesFromUser("notexists", new List<string> { "VP Of Sales" });
            });


        }
        [Fact]
        public async Task RemoveRolesToUserWhenSucceded()
        {

            var user =
               new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};

            var service = await BuildService(user);
            await service.RemoveRolesFromUser("kinjal.s", new List<string> { "VP Of Operations" });
            var result = await service.GetUser("kinjal.s");
            Assert.NotNull(result);
            Assert.DoesNotContain("VP Of Operations", result.Roles);


        }

        [Fact]
        public async Task GetUserWhenUsernameNull()
        {

            var user =
         new List<IUser> {
                new User
                {
                    Email = "kinjal.s@sigmainfo.net",
                    Name = "kinjal",
                    Password = "test123",
                    PasswordSalt = "test",
                    Username = "kinjal.s",
                    Roles = new[] { "VP Of Sales" }

                }};
            var service = await BuildService(user);


            var result = await service.GetUser("kinjal.s");
            Assert.NotNull(result);



        }
        [Fact]
        public async Task GetUserWhenSucceded()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();


                await service.GetUser(null);



            });
        }
        [Fact]
        public async Task ChangeNameWhenUsernameNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();

                await service.ChangeName(string.Empty, "kinjal.s");

            });
        }
        [Fact]
        public async Task ChangeNameWhenNameNull()
        {
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>
            {

                var service = await BuildService();

                await service.ChangeName("kinjal.s", string.Empty);

            });
        }
        [Fact]
        public async Task ChangeNameWhenUserNotFound()
        {
            await Assert.ThrowsAsync<UserNotFoundException>(async () =>
            {
                var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" }

                  }
            };
                var service = await BuildService(user);

                await service.ChangeName("NotFound", "kinjal.s");

            });
        }
        [Fact]
        public async Task ChangeNameWhenSucceeded()
        {

            var user = new List<IUser> {

                  new User
                  {

                      Id = "1",
                      Name = "kinjal.s",
                      Email = "kinjal.s@sigmainfo.net",
                      Password = "test123",
                      PasswordSalt = "test",
                      Username = "kinjal.s",
                      Roles = new[] { "VP Of Sales" }

                  }
            };
            var service = await BuildService(user);

            await service.ChangeName("kinjal.s", "john.s");
            var result = await service.GetUser("kinjal.s");
            Assert.NotNull(result);
            Assert.NotEqual("john.s", result.Name);
        }
        private static Mock<ITokenReader> TokenReader = new Mock<ITokenReader>();
        private static FakeSessionManager SessionManager = new FakeSessionManager();
        private static FakeTokenHandler TokenHandler = new FakeTokenHandler(new SecurityTokenConfig(), Mock.Of<ITokenReader>(),
                Mock.Of<ITokenWriter>());

        private static async Task<IIdentityService> BuildService(IEnumerable<IUser> users = null, int passwordExpirationDays = 0, Mock<IPasswordComplexityRules> passwordComplexityRules = null, Configuration configuration = null)
        {

            var userRepository = new FakeUserRepository();
            if (configuration == null)
                configuration = GetConfiguration();

            var roleToPermissionConverter = new RoleToPermissionConverter(configuration);


            var mockTenant = new Mock<ITenantService>();

            mockTenant.Setup(c => c.Current).Returns(new TenantInfo() { Id = "my-tenant" });

            Mock<IPasswordComplexityRules> mockPasswordComplexityRules = null;

            if (passwordComplexityRules == null)
            {
                mockPasswordComplexityRules = new Mock<IPasswordComplexityRules>();
                mockPasswordComplexityRules.Setup(c => c.Complies(It.IsAny<string>())).Returns(true);
            }
            else
                mockPasswordComplexityRules = passwordComplexityRules;

            Mock<IConfigurationServiceFactory> mockConfigurationServiceFactory = new Mock<IConfigurationServiceFactory>();

            Mock<IConfigurationService<AlloyConfiguration>> configurationService = new Mock<IConfigurationService<AlloyConfiguration>>();
            configurationService.Setup(p => p.Get()).Returns(new AlloyConfiguration { Login = "Login", SecurityResetUrls = new CaseInsensitiveDictionary<string> { { "back-office", "back-office" } } });
            mockConfigurationServiceFactory.Setup(p => p.Create<AlloyConfiguration>(It.IsAny<string>(), TokenReader.Object)).Returns(configurationService.Object);

            var service = new IdentityService(configuration, userRepository, new Pbkdf2Crypt(),
                 SessionManager,
                 Mock.Of<IEventHubClient>(), TokenHandler, Mock.Of<IEmailService>(),
                mockConfigurationServiceFactory.Object, TokenReader.Object, mockTenant.Object, roleToPermissionConverter,
                 mockPasswordComplexityRules.Object, new FakeSmsService(), new FakeUserOtpRepository(new UserOtp { Username = "john.p", TenantId = "my-tenant", OtpCode = "1", ExpirationDate = DateTime.Now.AddHours(1) }));
            if (users != null)
            {
                foreach (var user in users)
                {
                    var userRequest = new CreateUserRequest()
                    {
                        Name = user.Name,
                        Email = user.Email,
                        Username = user.Username,
                        Roles = user.Roles,
                        Password = user.Password,
                        PasswordSalt = user.PasswordSalt,
                        IsActive = user.IsActive
                    };

                    await service.CreateUser(userRequest);
                }
            }

            return service;
        }


        private static Configuration GetTemplateConfiguration()
        {

            const string configurationJson = "{											     " +

                "	\"PhonePrefix\": \"1234\",												 " +
                "	\"Otpsmstemplatename\": \"sendOtp\",												 " +
                                             "	\"sessionTimeout\": 10,												 " +
                                             "	\"tokenTimeout\": 60,                                                " +
                                              "	\"MaxFailedLoginAttempts\": 1,                                                " +
                                               "	\"AccountLockoutPeriod\": 1,                                                " +
                                             "\"AccountLockoutMessage\": \"Locked\",                                                " +
                                              "\"PasswordComplexityErrorMessage\": \"PasswordComplexityErrorMessage\",                                                " +
                                              "\"MaxFailedLoginAttemptsErrorMessage\": \"Locked\",                                                " +
                                             "	\"roles\": {                                                         " +
                                             "		\"VP Of Sales\": {                                               " +
                                             "			\"permissions\": [\"vpOfSales\",\"back-office\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Loan Coordinator Manager\": {                          " +
                                             "					\"permissions\": [\"LoanCoordinatorManager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Loan Coordinator\": {                          " +
                                             "							\"permissions\": [\"loancordinator\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Sr. Relationship Manager\": {                          " +
                                             "					\"permissions\": [\"srrelationshipmanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Relationship Manager\": {                      " +
                                             "							\"permissions\": [\"relationshipmanager\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"VP Of Operations\": {                                          " +
                                             "			\"permissions\": [\"vpofoperations\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Sr. Loan Processor\": {                                " +
                                             "					\"permissions\": [\"srloanprocessor\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Loan Processor\": {                            " +
                                             "							\"permissions\": [\"loanprocessor\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Credit Specialist Manager\": {                         " +
                                             "					\"permissions\": [\"creditspecialistmanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Credit Specialist\": {                         " +
                                             "							\"permissions\": [\"creditspecialist\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Merchant Underwriter Manager\": {                      " +
                                             "					\"permissions\": [\"merchantunderwritermanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Merchant Underwriter\": {                      " +
                                             "							\"permissions\": [\"merchantunderwriter\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Customer Service Manager\": {                          " +
                                             "					\"permissions\": [\"customerservicemanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Customer Service Representative\": {           " +
                                             "							\"permissions\": [\"customerservicerepresentative\",\"customerrepresentative1\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"QA Manager\": {                                        " +
                                             "					\"permissions\": [\"qamanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"QA\": {                                        " +
                                             "							\"permissions\": [\"qa\",\"q2\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"Director Of Merchant Support\": {                              " +
                                             "			\"permissions\": [\"{*any}\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Merchant Support Specialist\": {                       " +
                                             "					\"permissions\": [\"{*any}\"]                        " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"Marketing\": {                                                 " +
                                             "			\"permissions\": [\"{*any}\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Social Media Coordinator\": {                          " +
                                             "					\"permissions\": [\"{*any}\"]                        " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		}                                                                " +
                                             "	}                                                                    " +
                                             "}                                                                       ";
            var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Configuration>(configurationJson);

            configuration.PasswordHistoryLength = 2;
            return configuration;
        }

        private static Configuration GetConfiguration()
        {

            const string configurationJson = "{											     " +
                "	\"OtpExpirationMinutes\": \"48\",												 " +
                                             "	\"sessionTimeout\": 10,												 " +
                                             "	\"tokenTimeout\": 60,                                                " +
                                              "	\"MaxFailedLoginAttempts\": 1,                                                " +
                                               "	\"AccountLockoutPeriod\": 1,                                                " +
                                             "\"AccountLockoutMessage\": \"Locked\",                                                " +
                                              "\"PasswordComplexityErrorMessage\": \"PasswordComplexityErrorMessage\",                                                " +
                                              "\"MaxFailedLoginAttemptsErrorMessage\": \"Locked\",                                                " +
                                             "	\"roles\": {                                                         " +
                                             "		\"VP Of Sales\": {                                               " +
                                             "			\"permissions\": [\"vpOfSales\",\"back-office\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Loan Coordinator Manager\": {                          " +
                                             "					\"permissions\": [\"LoanCoordinatorManager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Loan Coordinator\": {                          " +
                                             "							\"permissions\": [\"loancordinator\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Sr. Relationship Manager\": {                          " +
                                             "					\"permissions\": [\"srrelationshipmanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Relationship Manager\": {                      " +
                                             "							\"permissions\": [\"relationshipmanager\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"VP Of Operations\": {                                          " +
                                             "			\"permissions\": [\"vpofoperations\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Sr. Loan Processor\": {                                " +
                                             "					\"permissions\": [\"srloanprocessor\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Loan Processor\": {                            " +
                                             "							\"permissions\": [\"loanprocessor\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Credit Specialist Manager\": {                         " +
                                             "					\"permissions\": [\"creditspecialistmanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Credit Specialist\": {                         " +
                                             "							\"permissions\": [\"creditspecialist\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Merchant Underwriter Manager\": {                      " +
                                             "					\"permissions\": [\"merchantunderwritermanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Merchant Underwriter\": {                      " +
                                             "							\"permissions\": [\"merchantunderwriter\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Customer Service Manager\": {                          " +
                                             "					\"permissions\": [\"customerservicemanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Customer Service Representative\": {           " +
                                             "							\"permissions\": [\"customerservicerepresentative\",\"customerrepresentative1\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"QA Manager\": {                                        " +
                                             "					\"permissions\": [\"qamanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"QA\": {                                        " +
                                             "							\"permissions\": [\"qa\",\"q2\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"Director Of Merchant Support\": {                              " +
                                             "			\"permissions\": [\"{*any}\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Merchant Support Specialist\": {                       " +
                                             "					\"permissions\": [\"{*any}\"]                        " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"Marketing\": {                                                 " +
                                             "			\"permissions\": [\"{*any}\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Social Media Coordinator\": {                          " +
                                             "					\"permissions\": [\"{*any}\"]                        " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		}                                                                " +
                                             "	}                                                                    " +
                                             "}                                                                       ";
            var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Configuration>(configurationJson);

            configuration.PasswordHistoryLength = 2;
            return configuration;
        }


    }
}