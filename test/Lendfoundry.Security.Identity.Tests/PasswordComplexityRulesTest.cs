﻿using LendFoundry.Security.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Lendfoundry.Security.Identity.Tests
{
    public class PasswordComplexityRulesTest
    {
       
      

        [Fact]
        public void NullPasswordDoesNotComply()
        {
            var Rules = GetComplexPasswordRule();
            Assert.False(Rules.Complies(null));
        }

        [Fact]
        public void BlankPasswordDoesNotComply()
        {
            var Rules = GetComplexPasswordRule();
            Assert.False(Rules.Complies("   "));
        }
        
        [Fact]
        public void DoesNotComplyWhenItHasLessThanEightCharacters()
        {
            var Rules = GetComplexPasswordRule();
            Assert.False(Rules.Complies("a9b#cDe"));
        }

        [Fact]
        public void DoesNotComplyWhenItDoesNotContainAnyNumber()
        {
            var Rules = GetComplexPasswordRule();
            Assert.False(Rules.Complies("a_b#cDef"));
        }

        [Fact]
        public void DoesNotComplyWhenItDoesNotContainAnySymbol()
        {
            var Rules = GetComplexPasswordRule();
            Assert.False(Rules.Complies("a9bacDef"));
        }

        [Fact]
        public void DoesNotComplyWhenItDoesNotContainAnyCapitalLetter()
        {
            var Rules = GetComplexPasswordRule();
            Assert.False(Rules.Complies("a9b#cdef"));
        }

        [Fact]
        public void CompliesWhenItHasAtLeastEightCharactersOneNumberOneSymbolAndOneCapitalLetter()
        {
            var Rules = GetComplexPasswordRule();
            Assert.True(Rules.Complies("a9b#cDef"));
        }

        [Fact]
        public void CompliesWhenItHasMoreThanOneOfTheRequiredCharacters()
        {
            var Rules = GetComplexPasswordRule();
            Assert.True(Rules.Complies("a9b23c#$dEFgh"));
        }

        [Fact]
        public void CompliesWhenThereisNoConfigurationSetupForPassword()
        {
            var Rules = GetEmptyComplexPasswordRule();
            Assert.True(Rules.Complies("12345"));
        }
        private static  IPasswordComplexityRules BuildService(Configuration configuration)
        {      
            var service = new PasswordComplexityRules(configuration);           
            return service;
        }

        private static IPasswordComplexityRules GetComplexPasswordRule()
        {
            var configuration = GetConfiguration();
            var Rules = BuildService(configuration);
            return Rules;
        }

        private static IPasswordComplexityRules GetEmptyComplexPasswordRule()
        {
            var configuration = GetConfiguration();
            configuration.PasswordComplexityPattern = null;
            var Rules = BuildService(configuration);
            return Rules;
        }
        private static Configuration GetConfiguration()
        {

            const string configurationJson = "{											     " +
                                             "	\"sessionTimeout\": 10,												 " +
                                             "	\"tokenTimeout\": 60,                                                " +
                                             "\"AccountLockoutMessage\": \"Locked\",                                                " +
                                              "\"MaxFailedLoginAttemptsErrorMessage\": \"Locked\",                                                " +
                                             "	\"roles\": {                                                         " +
                                             "		\"VP Of Sales\": {                                               " +
                                             "			\"permissions\": [\"vpOfSales\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Loan Coordinator Manager\": {                          " +
                                             "					\"permissions\": [\"LoanCoordinatorManager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Loan Coordinator\": {                          " +
                                             "							\"permissions\": [\"loancordinator\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Sr. Relationship Manager\": {                          " +
                                             "					\"permissions\": [\"srrelationshipmanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Relationship Manager\": {                      " +
                                             "							\"permissions\": [\"relationshipmanager\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"VP Of Operations\": {                                          " +
                                             "			\"permissions\": [\"vpofoperations\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Sr. Loan Processor\": {                                " +
                                             "					\"permissions\": [\"srloanprocessor\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Loan Processor\": {                            " +
                                             "							\"permissions\": [\"loanprocessor\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Credit Specialist Manager\": {                         " +
                                             "					\"permissions\": [\"creditspecialistmanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Credit Specialist\": {                         " +
                                             "							\"permissions\": [\"creditspecialist\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Merchant Underwriter Manager\": {                      " +
                                             "					\"permissions\": [\"merchantunderwritermanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Merchant Underwriter\": {                      " +
                                             "							\"permissions\": [\"merchantunderwriter\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"Customer Service Manager\": {                          " +
                                             "					\"permissions\": [\"customerservicemanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"Customer Service Representative\": {           " +
                                             "							\"permissions\": [\"customerservicerepresentative\",\"customerrepresentative1\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				},                                                       " +
                                             "				\"QA Manager\": {                                        " +
                                             "					\"permissions\": [\"qamanager\"],                       " +
                                             "					\"roles\": {                                         " +
                                             "						\"QA\": {                                        " +
                                             "							\"permissions\": [\"qa\",\"q2\"]                " +
                                             "						}                                                " +
                                             "					}                                                    " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"Director Of Merchant Support\": {                              " +
                                             "			\"permissions\": [\"{*any}\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Merchant Support Specialist\": {                       " +
                                             "					\"permissions\": [\"{*any}\"]                        " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		},                                                               " +
                                             "		\"Marketing\": {                                                 " +
                                             "			\"permissions\": [\"{*any}\"],                               " +
                                             "			\"roles\": {                                                 " +
                                             "				\"Social Media Coordinator\": {                          " +
                                             "					\"permissions\": [\"{*any}\"]                        " +
                                             "				}                                                        " +
                                             "			}                                                            " +
                                             "		}                                                                " +
                                             "	}                                                                    " +
                                             "}                                                                       ";
            var configuration = Newtonsoft.Json.JsonConvert.DeserializeObject<Configuration>(configurationJson);
            if (configuration.Roles == null || !configuration.Roles.Any())
                throw new InvalidOperationException("Roles are required");
            configuration.PasswordHistoryLength = 2;
            configuration.PasswordComplexityPattern = @"^(?=.*[0-9])(?=.*[!@#$%^&*_])(?=.*[A-Z]).{8,}$";
            configuration.PasswordComplexityErrorMessage = "Error message";
            return configuration;
        }
    }
}
