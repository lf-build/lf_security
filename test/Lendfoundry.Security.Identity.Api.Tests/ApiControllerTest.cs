﻿
using LendFoundry.Security.Identity;
using LendFoundry.Security.Identity.Api.Controllers;
using Moq;
using Xunit;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Security.Identity.Api.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using LendFoundry.Foundation.Services;
using System.Security.Authentication;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;

namespace Lendfoundry.Security.Identity.Api.Tests
{
   public class ApiControllerTest
    {
        

        [Fact]
        public async Task CheckUsername_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.IsUsernameAvailable(It.IsAny<string>())).Returns(Task.FromResult(true));

         var controller=   new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await   controller.CheckUsername(new CheckUsernameRequest { Username=  "kinjal.s" });
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }
        [Fact]
        public async Task CheckUsername_Should_UserNameNull()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.IsUsernameAvailable(It.IsAny<string>())).ThrowsAsync(new Exception());
            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());
            var result = await controller.CheckUsername(It.IsAny<CheckUsernameRequest>());

            Assert.NotNull(result);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);

        }
        [Fact]
        public async Task CheckUsername_Should_ThrownAnException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.IsUsernameAvailable(It.IsAny<string>())).ThrowsAsync(new Exception());
            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());
            var result =await  controller.CheckUsername(new CheckUsernameRequest { Username = "kinjal.s" });

            Assert.NotNull(result);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
          
        }

        [Fact]
        public async Task GetCurrentUser_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetCurrentUser()).Returns(Task.FromResult<IUserInfo>( new UserInfo { Id="1",Username=It.IsAny<string>() }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetCurrentUser();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetCurrentUser_Should_ThrownAnException_AuthenticationException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetCurrentUser()).ThrowsAsync(new AuthenticationException());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetCurrentUser();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);

        }

        [Fact]
        public async Task GetCurrentUser_Should_ThrownAnException_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetCurrentUser()).ThrowsAsync(new InvalidTokenException("Invalid"));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetCurrentUser();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);

        }

        [Fact]
        public async Task GetAllRoles_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRoles()).Returns(Task.FromResult<IEnumerable<string>>(new List<string> {"VP of Sales","VP of Operations" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllRoles();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);

        }



        [Fact]
        public async Task GetAllChildRolesFromCurrentUser_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetChildrenRoles(It.IsAny<List<string>>())).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllChildRolesFromCurrentUser();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetAllChildRolesFromCurrentUser_Should_ThrownAnException_AuthenticationException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));
            mockIdentityservice.Setup(p => p.GetChildrenRoles(It.IsAny<List<string>>())).ThrowsAsync(new AuthenticationException());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllChildRolesFromCurrentUser();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);

        }

        [Fact]
        public async Task GetAllChildRolesFromCurrentUser_Should_ThrownAnException_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));
            mockIdentityservice.Setup(p => p.GetChildrenRoles(It.IsAny<List<string>>())).ThrowsAsync(new InvalidTokenException("Invalid"));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllChildRolesFromCurrentUser();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);

        }

        [Fact]
        public async Task GetAllChildRolesFromCurrentUser_Should_ThrownAnException_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));
            mockIdentityservice.Setup(p => p.GetChildrenRoles(It.IsAny<List<string>>())).ThrowsAsync(new ArgumentException { });

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllChildRolesFromCurrentUser();
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }

        [Fact]
        public async Task GetAllChildRolesFromCurrentUser_Should_ThrownAnException_UserNotFoundException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));
            mockIdentityservice.Setup(p => p.GetChildrenRoles(It.IsAny<List<string>>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllChildRolesFromCurrentUser();
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }



        [Fact]
        public async Task GetAllRolesFromCurrentUser_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllRolesFromCurrentUser();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetAllRolesFromCurrentUser_Should_ThrownAnException_AuthenticationException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).ThrowsAsync(new AuthenticationException());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllRolesFromCurrentUser();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);

        }

        [Fact]
        public async Task GetAllRolesFromCurrentUser_Should_ThrownAnException_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).ThrowsAsync(new InvalidTokenException("Invalid"));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllRolesFromCurrentUser();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);

        }

        [Fact]
        public async Task GetAllRolesFromCurrentUser_Should_ThrownAnException_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).ThrowsAsync(new ArgumentException { });

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllRolesFromCurrentUser();
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }

        [Fact]
        public async Task GetAllRolesFromCurrentUser_Should_ThrownAnException_UserNotFoundException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetAllRolesFromCurrentUser()).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllRolesFromCurrentUser();
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }

        

        [Fact]
        public async Task GetUserRoles_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetUserRoles(It.IsAny<string>())).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUserRoles("kinjal.s");
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

       

        [Fact]
        public async Task GetUserRoles_Should_ThrownAnException_AuthenticationException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetUserRoles(It.IsAny<string>())).ThrowsAsync(new UserNotFoundException("User not found"));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUserRoles("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }

        [Fact]
        public async Task GetUserRoles_Should_ThrownAnException_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetUserRoles(It.IsAny<string>())).ThrowsAsync(new InvalidTokenException("Invalid"));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUserRoles("kinjal.s");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);

        }

        [Fact]
        public async Task GetUserRoles_Should_ThrownAnException_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetUserRoles(It.IsAny<string>())).ThrowsAsync(new ArgumentException { });

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUserRoles("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }

        [Fact]
        public async Task GetUserRoles_Should_ThrownAnException_UserNotFoundException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetUserRoles(It.IsAny<string>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUserRoles(It.IsAny<string>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }


        [Fact]
        public async Task GetAllChildRoles_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetChildRoles(It.IsAny<string>())).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllChildRoles("kinjal.s");
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetAllChildRoles_Should_ThrownAnException_RoleNull()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetChildRoles(It.IsAny<string>())).ThrowsAsync(new ArgumentException() { });

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllChildRoles(It.IsAny<string>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }

        [Fact]
        public async Task GetAllParentRoles_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetParentRoles(It.IsAny<string>())).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllParentRoles("kinjal.s");
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetAllParentRoles_Should_ThrownAnException_RoleNull()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetParentRoles(It.IsAny<string>())).ThrowsAsync(new ArgumentException() { });

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetAllParentRoles(It.IsAny<string>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }


        [Fact]
        public async Task GetChildrenRoles_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetChildrenRoles(It.IsAny<List<string>>())).Returns(Task.FromResult<IEnumerable<string>>(new List<string> { "VP of Sales", "VP of Operations" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetChildrenRoles( "VP of Sales" );
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetChildrenRoles_Should_ThrownAnException_RoleNull()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetChildrenRoles(It.IsAny<List<string>>())).ThrowsAsync(new ArgumentException() { });

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetChildrenRoles(It.IsAny<string>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }




        [Fact]
        public async Task GetRoleMembers_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
           

            mockIdentityservice.Setup(p => p.GetRoleMembers(It.IsAny<string>())).Returns(Task.FromResult<IEnumerable<IUser>>(new List<User> { new User { Id="1" } }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetRoleMembers("VP of Sales");
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetRoleMembers_Should_ThrownAnException_RoleNull()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetRoleMembers(It.IsAny<string>())).ThrowsAsync(new ArgumentException() { });

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetRoleMembers(It.IsAny<string>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }

        [Fact]
        public async Task GetRoleMembers_Should_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetRoleMembers(It.IsAny<string>())).ThrowsAsync(new InvalidTokenException("Invalid"));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetRoleMembers("VP of Sales");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);

        }

        [Fact]
        public async Task GetRoleMembers_Should_Exception()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
            mockIdentityservice.Setup(p => p.GetRoleMembers(It.IsAny<string>())).ThrowsAsync(new Exception("Invalid"));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetRoleMembers("VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);

        }

        [Fact]
        public async Task CreateUser_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.CreateUser(It.IsAny<CreateUserRequest>())).Returns(Task.FromResult<IUserInfo>(new UserInfo { Id = "1" } ));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.CreateUser(It.IsAny<CreateUserRequest>());
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task CreateUser_Should_Return_ArgumentNullException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.CreateUser(It.IsAny<CreateUserRequest>())).ThrowsAsync(new ArgumentNullException());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.CreateUser(It.IsAny<CreateUserRequest>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task CreateUser_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
         
            mockIdentityservice.Setup(p => p.CreateUser(It.IsAny<CreateUserRequest>())).ThrowsAsync(new ArgumentException());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.CreateUser(It.IsAny<CreateUserRequest>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task CreateUser_Should_Return_UsernameAlreadyExists()
        {
            var mockIdentityservice = new Mock<IIdentityService>();
         
            mockIdentityservice.Setup(p => p.CreateUser(It.IsAny<CreateUserRequest>())).ThrowsAsync(new UsernameAlreadyExists(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.CreateUser(new CreateUserRequest { Username = "kinjal.s" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task CreateUser_Should_Return_InvalidUserOrPasswordException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.CreateUser(It.IsAny<CreateUserRequest>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.CreateUser(It.IsAny<CreateUserRequest>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }


        [Fact]
        public async Task UpdateUser_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.UpdateUser(It.IsAny<User>())).Returns(Task.FromResult<IUserInfo>(new UserInfo { Id = "1" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.UpdateUser(It.IsAny<UserInfo>());
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

       
        [Fact]
        public async Task UpdateUser_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.UpdateUser(It.IsAny<User>())).ThrowsAsync(new ArgumentNullException());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.UpdateUser(It.IsAny<UserInfo>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task UpdateUser_Should_Return_UsernameAlreadyExists()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.UpdateUser(It.IsAny<UserInfo>())).ThrowsAsync(new UsernameAlreadyExists(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.UpdateUser(new UserInfo { Username = "kinjal.s" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task UpdateUser_Should_Return_InvalidUserOrPasswordException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.UpdateUser(It.IsAny<User>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.UpdateUser(It.IsAny<UserInfo>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }


        [Fact]
        public async Task UpdateUser_Should_Return_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.UpdateUser(It.IsAny<User>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.UpdateUser(It.IsAny<UserInfo>());
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }

        [Fact]
        public async Task Login_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(new LoginRequest { })).Returns(Task.FromResult<ILoginResponse>(new LoginResponse { UserId="1",Token="Token" }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.Login(new LoginRequest { });
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task Login_Should_Return_InvalidUserOrPasswordException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(It.IsAny<LoginRequest>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.Login(It.IsAny<LoginRequest>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        

        [Fact]
        public async Task Login_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(It.IsAny<LoginRequest>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.Login(new LoginRequest { Username = "kinjal.s", Password = "paasword", Portal = "back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task Login_Should_Return_AccountLockedException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(It.IsAny<LoginRequest>())).ThrowsAsync(new AccountLockedException(It.IsAny<string>(),It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.Login(new LoginRequest { Username = "kinjal.s", Password = "paasword", Portal = "back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task Login_Should_Return_PasswordExpiredException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(It.IsAny<LoginRequest>())).ThrowsAsync(new PasswordExpiredException(It.IsAny<string>(),It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.Login(new LoginRequest { Username = "kinjal.s", Password = "paasword", Portal = "back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task SendOtp_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.SendOtp(new SendOtpRequest { })).Returns(Task.FromResult<IOtpRequest>(new SendOtpRequest {  }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.SendOtp(new SendOtpRequest { });
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task SendOtp_Should_Return_InvalidUserOrPasswordException_WhenNull()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.SendOtp(It.IsAny<SendOtpRequest>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.SendOtp(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task SendOtp_Should_Return_InvalidUserOrPasswordException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.SendOtp(It.IsAny<SendOtpRequest>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.SendOtp(new SendOtpRequest {Username="kinjal.s" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task SendOtp_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.SendOtp(It.IsAny<SendOtpRequest>())).ThrowsAsync(new ArgumentException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.SendOtp(new SendOtpRequest { Username = "kinjal.s" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task SendOtp_Should_Return_AccountLockedException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.SendOtp(It.IsAny<SendOtpRequest>())).ThrowsAsync(new AccountLockedException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.SendOtp(new SendOtpRequest { Username = "kinjal.s" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }


        [Fact]
        public async Task LoginWithOtp_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(new OtpLoginRequest { })).Returns(Task.FromResult<ILoginResponse>(new LoginResponse { }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.LoginWithOtp(new OtpLoginRequest { });
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }
        [Fact]
        public async Task LoginWithOtp_Should_Return_InvalidUserOrPasswordExceptionWhenOtpRequestNull()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(It.IsAny<OtpLoginRequest>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.LoginWithOtp(It.IsAny<OtpLoginRequest>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task LoginWithOtp_Should_Return_InvalidUserOrPasswordException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(It.IsAny<OtpLoginRequest>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.LoginWithOtp(new OtpLoginRequest());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task LoginWithOtp_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(It.IsAny<OtpLoginRequest>())).ThrowsAsync(new ArgumentException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.LoginWithOtp(new OtpLoginRequest());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task LoginWithOtp_Should_Return_AccountLockedException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Login(It.IsAny<OtpLoginRequest>())).ThrowsAsync(new AccountLockedException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.LoginWithOtp(new OtpLoginRequest ());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task Logout_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Logout(It.IsAny<string>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.Logout();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(204, result.StatusCode);
        }

        [Fact]
        public async Task Logout_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Logout(It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>(), It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.Logout();
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task Logout_Should_Return_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.Logout(It.IsAny<string>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.Logout();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }


        [Fact]
        public async Task IsAuthorized_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.IsAuthorized(It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.IsAuthorized(It.IsAny<string>());
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task IsAuthorized_Should_Return_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.IsAuthorized(It.IsAny<string>(),It.IsAny<string>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.IsAuthorized(It.IsAny<string>());
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }

        [Fact]
        public async Task IsAuthorized_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.IsAuthorized(It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.IsAuthorized(It.IsAny<string>());
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task IsExpiring_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.IsExpiring()).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.IsExpiring();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }
        [Fact]
        public async Task IsExpiring_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.IsExpiring()).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.IsExpiring();
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task IsExpiring_Should_Return_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.IsExpiring()).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.IsExpiring();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }
        [Fact]
        public async Task RenewToken_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RenewToken()).Returns(Task.FromResult(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RenewToken();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task RenewToken_Should_Return_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RenewToken()).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RenewToken();
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }

        [Fact]
        public async Task RenewToken_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RenewToken()).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RenewToken();
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RequestPasswordReset_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RequestPasswordReset(It.IsAny< RequestPasswordResetRequest>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RequestPasswordReset(new RequestPasswordResetRequest { Username = "kinjal.s", Portal = "back-office" });
            Assert.IsType<StatusCodeResult >(response);
            var result = response as StatusCodeResult ;
            Assert.Equal(202, result.StatusCode);
        }
        [Fact]
        public async Task RequestPasswordReset_Should_Return_Username_Null()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RequestPasswordReset(It.IsAny<RequestPasswordResetRequest>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RequestPasswordReset(new RequestPasswordResetRequest {  Portal = "back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RequestPasswordReset_Should_Return_Portal_Null()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RequestPasswordReset(It.IsAny<RequestPasswordResetRequest>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RequestPasswordReset(new RequestPasswordResetRequest { Username = "kinjal.s" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RequestPasswordReset_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RequestPasswordReset(It.IsAny<RequestPasswordResetRequest>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RequestPasswordReset(new RequestPasswordResetRequest { Username = "kinjal.s",Portal="back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RequestPasswordReset_Should_Return_UserNotFoundException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RequestPasswordReset(It.IsAny<RequestPasswordResetRequest>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RequestPasswordReset(new RequestPasswordResetRequest { Username = "kinjal.s", Portal = "back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RequestPasswordReset_Should_Return_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RequestPasswordReset(It.IsAny<RequestPasswordResetRequest>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.RequestPasswordReset(new RequestPasswordResetRequest { Username = "kinjal.s", Portal = "back-office" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }

        [Fact]
        public async Task ResetPassword_Should_Return_Ok()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ResetPassword(It.IsAny<string>(),It.IsAny<string>(),It.IsAny<string>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.ResetPassword("token",new LoginRequest { Username = "kinjal.s",Password="password",Portal="back-office" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult ;
            Assert.Equal(204, result.StatusCode);
        }

        [Fact]
        public async Task ResetPassword_Should_Return_InvalidUserOrPasswordException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ResetPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.ResetPassword("token", null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ResetPassword_Should_Return_ArgumentException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ResetPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.ResetPassword("token", new LoginRequest { Username = "kinjal.s", Password = "password", Portal = "back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ResetPassword_Should_Return_AccountLockedException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ResetPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new AccountLockedException(It.IsAny<string>(),It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.ResetPassword("token", new LoginRequest { Username = "kinjal.s", Password = "password", Portal = "back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ResetPassword_Should_Return_UserNotFoundException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ResetPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.ResetPassword("token", new LoginRequest { Username = "kinjal.s", Password = "password", Portal = "back-office" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ResetPassword_Should_Return_InvalidTokenException()
        {
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ResetPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.ResetPassword("token", new LoginRequest { Username = "kinjal.s", Password = "password", Portal = "back-office" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }
        [Fact]
        public async Task ChangePassword_Should_Return_Ok()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangePassword(new PasswordChangeRequest {CurrentPassword= "oldpassword", Password = "password",ConfirmPassword="password" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(204, result.StatusCode);
        }


        [Fact]
        public async Task ChangePassword_Should_Return_InvalidUserOrPasswordException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = string.Empty });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangePassword(new PasswordChangeRequest { CurrentPassword = "oldpassword", Password = "password", ConfirmPassword = "password" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task ChangePassword_Should_Return_ArgumentException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject ="kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangePassword(new PasswordChangeRequest { CurrentPassword = "oldpassword", Password = "password", ConfirmPassword = "password" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ChangePassword_Should_Return_ArgumentNullException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new ArgumentNullException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangePassword(new PasswordChangeRequest { CurrentPassword = "oldpassword", Password = "password", ConfirmPassword = "password" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task ChangePassword_Should_Return_UserNotFoundException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangePassword(new PasswordChangeRequest { CurrentPassword = "oldpassword", Password = "password", ConfirmPassword = "password" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ChangePassword_Should_Return_InvalidTokenException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangePassword(new PasswordChangeRequest { CurrentPassword = "oldpassword", Password = "password", ConfirmPassword = "password" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }
        [Fact]
        public async Task ChangePassword_Should_Return_InvalidUserOrPasswordException_When_ResetNull()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangePassword(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ChangePassword_Should_Return_InvalidUserOrPasswordException_When_PassMismatch()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangePassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangePassword(new PasswordChangeRequest { CurrentPassword = "oldpassword", Password = "password", ConfirmPassword = "password1" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ChangeName_Should_Return_Ok()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangeName(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangeName(new NameChangeRequest {Name= "change" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(204, result.StatusCode);
        }

        [Fact]
        public async Task ChangeName_Should_Return_InvalidUserOrPasswordException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangeName(It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangeName(new NameChangeRequest { Name = "change" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ChangeName_Should_Return_InvalidUserOrPasswordExceptionWhenUserNull()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangeName(It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangeName(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ChangeName_Should_Return_ArgumentNullException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangeName(It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new ArgumentNullException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangeName(new NameChangeRequest { Name = "change" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task ChangeName_Should_Return_ArgumentException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangeName(It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangeName(new NameChangeRequest { Name = "change" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ChangeName_Should_Return_UserNotFoundException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangeName(It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangeName(new NameChangeRequest { Name = "change" });
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ChangeName_Should_Return_InvalidTokenException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ChangeName(It.IsAny<string>(), It.IsAny<string>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ChangeName(new NameChangeRequest { Name = "change" });
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }
        [Fact]
        public async Task ActivateUser_Should_Return_Ok()
        {
            
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject="kinjal.s" }); 
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ActivateUser(It.IsAny<string>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ActivateUser("kinjal.s");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(204, result.StatusCode);
        }

        [Fact]
        public async Task ActivateUser_Should_Return_InvalidUserOrPasswordException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ActivateUser(It.IsAny<string>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ActivateUser("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ActivateUser_Should_Return_ArgumentException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ActivateUser(It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ActivateUser(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ActivateUser_Should_Return_ArgumentNullException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ActivateUser(It.IsAny<string>())).ThrowsAsync(new ArgumentNullException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ActivateUser("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ActivateUser_Should_Return_UserNotFoundException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ActivateUser(It.IsAny<string>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ActivateUser("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task ActivateUser_Should_Return_InvalidTokenException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.ActivateUser(It.IsAny<string>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.ActivateUser("kinjal.s");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }
        [Fact]
        public async Task DeActivateUser_Should_Return_Ok()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.DeActivateUser(It.IsAny<string>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(),Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.DeActivateUser("kinjal.s");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(204, result.StatusCode);
        }


        [Fact]
        public async Task DeActivateUser_Should_Return_InvalidUserOrPasswordException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.DeActivateUser(It.IsAny<string>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.DeActivateUser("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task DeActivateUser_Should_Return_ArgumentExceptionWhenUsernameNull()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.DeActivateUser(It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.DeActivateUser(null);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }


        [Fact]
        public async Task DeActivateUser_Should_Return_ArgumentException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.DeActivateUser(It.IsAny<string>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.DeActivateUser("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task DeActivateUser_Should_Return_ArgumentNullException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.DeActivateUser(It.IsAny<string>())).ThrowsAsync(new ArgumentNullException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.DeActivateUser("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task DeActivateUser_Should_Return_UserNotFoundException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.DeActivateUser(It.IsAny<string>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.DeActivateUser("kinjal.s");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task DeActivateUser_Should_Return_InvalidTokenException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.DeActivateUser(It.IsAny<string>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.DeActivateUser("kinjal.s");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }
        [Fact]
        public async Task AssignRolesToUser_Should_Return_Ok()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.AssignRolesToUser(It.IsAny<string>(),It.IsAny<List<string>>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.AssignRolesToUser("kinjal.s","VP of Sales");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(204, result.StatusCode);
        }

        [Fact]
        public async Task AssignRolesToUser_Should_Return_InvalidToken()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.AssignRolesToUser(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.AssignRolesToUser("kinjal.s", "VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task AssignRolesToUser_Should_Return_ArgumentExceptionWhenUserNameNull()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.AssignRolesToUser(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.AssignRolesToUser("", "VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task AssignRolesToUser_Should_Return_ArgumentExceptionWhenUserRolesNull()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.AssignRolesToUser(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.AssignRolesToUser("kinjal.s", "");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task AssignRolesToUser_Should_Return_ArgumentNullException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.AssignRolesToUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new ArgumentNullException ());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.AssignRolesToUser("kinjal.s", "Vp Of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task AssignRolesToUser_Should_Return_UserNotFoundException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.AssignRolesToUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.AssignRolesToUser("kinjal.s", "Vp Of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task AssignRolesToUser_Should_Return_InvalidTokenException()
        {

            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.AssignRolesToUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.AssignRolesToUser("kinjal.s", "Vp Of Sales");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }
        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_Ok()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult(true));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(),Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser ("kinjal.s",  "VP of Sales" );
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(204, result.StatusCode);
        }

        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_InvalidUserOrPasswordExceptionWhenTokenInvalid()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser("kinjal.s", "VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_ArgumentExceptionWhenUsernameNull()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser("", "VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_ArgumentExceptionWhenRoleNull()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser("kinjal.s", "");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_ArgumentNullException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new ArgumentNullException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser("kinjal.s", "VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_ArgumentException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new ArgumentException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser("kinjal.s", "VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_InvalidUserOrPasswordException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new InvalidUserOrPasswordException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser("kinjal.s", "VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_UserNotFoundException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new UserNotFoundException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser("kinjal.s", "VP of Sales");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
        [Fact]
        public async Task RemoveRoleFromUser_Should_Return_InvalidTokenException()
        {
            var tokenHandler = new Mock<ITokenHandler>();
            tokenHandler.Setup(p => p.Parse(It.IsAny<string>())).Returns(new Token { Subject = "kinjal.s" });
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.RemoveRolesFromUser(It.IsAny<string>(), It.IsAny<List<string>>())).ThrowsAsync(new InvalidTokenException(It.IsAny<string>()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), tokenHandler.Object);

            var response = await controller.RemoveRoleFromUser("kinjal.s", "VP of Sales");
            Assert.IsType<StatusCodeResult>(response);
            var result = response as StatusCodeResult;
            Assert.Equal(403, result.StatusCode);
        }

        [Fact]
        public async Task GetUsers_Should_Return_Ok()
        {
            
            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.GetAllUsers()).Returns( Task.FromResult<IEnumerable<IUserInfo>>( new List<UserInfo> { new UserInfo () } ));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(),Mock.Of<ITokenReader>() , Mock.Of<ITokenHandler>());

            var response = await controller.GetUsers();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetUsersByRoles_Should_Return_Ok()
        {

            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.GetUsersByRoles(It.IsAny<List<string>>())).Returns( Task.FromResult<IEnumerable<IUserInfo>>(new List<UserInfo> { new UserInfo () } ));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUsersByRoles(It.IsAny<string>());
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetUsersByPasswordExpiring_Should_Return_Ok()
        {

            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.GetUsersByPasswordExpiring()).Returns(Task.FromResult<IEnumerable<IUserInfo>>(new List<UserInfo> { new UserInfo() }));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUsersByPasswordExpiring();
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetUsersByPasswordExpiring_Should_Return_ArgumentOutOfRangeException()
        {

            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.GetUsersByPasswordExpiring()).ThrowsAsync(new ArgumentOutOfRangeException());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUsersByPasswordExpiring();
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }


        [Fact]
        public async Task GetUserById_Should_Return_Ok()
        {

            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.GetUserById(It.IsAny<string>())).Returns(Task.FromResult<IUserInfo>( new UserInfo() ));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUserById(It.IsAny<string>());
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }


        [Fact]
        public async Task GetUserByUsername_Should_Return_Ok()
        {

            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.GetUser(It.IsAny<string>())).Returns(Task.FromResult<IUser>(new User()));

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUserByUsername("kinjal.s");
            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task GetUserByUsername_Should_Return_ArgumentExceptionWhenUsernameNull()
        {

            var mockIdentityservice = new Mock<IIdentityService>();


            mockIdentityservice.Setup(p => p.GetUser(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

            var controller = new ApiController(mockIdentityservice.Object, Mock.Of<ILogger>(), Mock.Of<ITokenReader>(), Mock.Of<ITokenHandler>());

            var response = await controller.GetUserByUsername("");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.Equal(400, result.StatusCode);
        }
    }
}
