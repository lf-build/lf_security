﻿using System;
using System.Threading;
using Moq;
using Xunit;

namespace LendFoundry.Security.Tokens.Tests
{
    public class TokenHandlerTest
    {
        private static readonly byte[] SecretKey = { 83, 49, 103, 110, 49, 110, 103, 61, 115, 51, 99, 114, 51, 116 };
        
        [Fact]
        public void ShouldFailWhenReadingEmptyToken()
        {
            var config = Mock.Of<ISecurityTokenConfig>(c => c.SecretKey == SecretKey);
            var reader = Mock.Of<ITokenReader>(r => r.Read() == string.Empty);
            var writer = Mock.Of<ITokenWriter>();
            var handler = new TokenHandler(config, reader, writer);
            Assert.Throws<ArgumentException>(() => handler.Parse());
        }

        [Fact]
        public void ShouldParseFromReaderWhenNoValueIsPassed()
        {
            var expectedIssuer = "my-app";
            var expectedValue = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJcL0RhdGUoMTQ0MzgzNDE2MzgzNylcLyIsInRlbmFudCI6IiIsImlzcyI6Im15LWFwcCJ9.88e4Jq4AL24_b4Cv4lAXxWzIGGD44Wt8dIM01EQAoF4";

            var config = Mock.Of<ISecurityTokenConfig>(c => c.SecretKey == SecretKey);
            var reader = Mock.Of<ITokenReader>(r => r.Read() == expectedValue);
            var writer = Mock.Of<ITokenWriter>();
            var handler = new TokenHandler(config, reader, writer);
            var token = handler.Parse();

            Assert.NotNull(token);
            Assert.Equal(expectedIssuer, token.Issuer);
            Assert.Equal(expectedValue, token.Value);
        }

        [Fact]
        public void ShouldIssueSimpleToken()
        {
            var config = Mock.Of<ISecurityTokenConfig>(c => c.SecretKey == SecretKey);
            var reader = Mock.Of<ITokenReader>();
            var writer = Mock.Of<ITokenWriter>();
            var handler = new TokenHandler(config, reader, writer);

            var expectedTenant = "my-tenant";
            var expectedIssuer = "my-app";
            var token = handler.Issue(expectedTenant, expectedIssuer);

            Assert.NotNull(token);
            Assert.Equal(expectedTenant, token.Tenant);
            Assert.Equal(expectedIssuer, token.Issuer);
        }


        [Fact]
        public void ShouldIssueSimpleTokenWithEmptyDefaultConfig()
        {
            var reader = Mock.Of<ITokenReader>();
            var writer = Mock.Of<ITokenWriter>();
            var handler = new TokenHandler(null, reader, writer);

            var expectedTenant = "rockloans";
            var expectedIssuer = "rocketloans";
            var token = handler.Issue(expectedTenant, expectedIssuer);

            Assert.NotNull(token);
            Assert.Equal(expectedIssuer, token.Issuer);
        }

        [Fact]
        public void ShouldIssueTokenWithExpirationSubjectAndScope()
        {
            var config = Mock.Of<ISecurityTokenConfig>(c => c.SecretKey == SecretKey);
            var reader = Mock.Of<ITokenReader>();
            var writer = Mock.Of<ITokenWriter>();
            var handler = new TokenHandler(config, reader, writer);

            var expectedTenant = "my-tenant";
            var expectedIssuer = "my-app";
            var expectedExpiration = DateTime.UtcNow.AddYears(1); //.AddMinutes(3);
            var expectedSubject = "johnsmith";
            var expectedScope = new[] {"amortization/schedule", "loans/documents/upload"};

            var token = handler.Issue(expectedTenant, expectedIssuer, expectedExpiration, expectedSubject, expectedScope);

            Assert.NotNull(token);
            Assert.Equal(expectedIssuer, token.Issuer);
            Assert.Equal(expectedExpiration, token.Expiration);
            Assert.Equal(expectedSubject, token.Subject);
            Assert.Equal(expectedScope, token.Scope);
        }

        [Fact]
        public void ShouldBeExpired()
        {
            var config = Mock.Of<ISecurityTokenConfig>(c => c.SecretKey == SecretKey);
            var reader = Mock.Of<ITokenReader>();
            var writer = Mock.Of<ITokenWriter>();
            var handler = new TokenHandler(config, reader, writer);

            var expectedTenant = "my-tenant";
            var expectedIssuer = "my-app";
            var expectedExpiration = DateTime.UtcNow;
            var expectedSubject = "johnsmith";
            var expectedScope = new[] { "amortization/schedule", "loans/documents/upload" };

            var token = handler.Issue(expectedTenant, expectedIssuer, expectedExpiration, expectedSubject, expectedScope);

            Assert.NotNull(token);
            Assert.Equal(expectedIssuer, token.Issuer);
            Assert.Equal(expectedExpiration, token.Expiration);
            Assert.Equal(expectedSubject, token.Subject);
            Assert.Equal(expectedScope, token.Scope);

            Thread.Sleep(5);

            Assert.True(DateTime.UtcNow.CompareTo(token.Expiration) > 0);
        }
    }
}
