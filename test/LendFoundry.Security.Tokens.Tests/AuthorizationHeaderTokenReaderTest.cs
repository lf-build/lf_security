﻿using System;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
#else
using Microsoft.AspNet.Http;

using Microsoft.Framework.Primitives;
#endif

using Moq;
using Xunit;

namespace LendFoundry.Security.Tokens.Tests
{
    public class AuthorizationHeaderTokenReaderTest
    {
        [Fact]
        public void ShouldFailWithoutHttpContext()
        {
            var accessor = Mock.Of<IHttpContextAccessor>();
            var reader = new AuthorizationHeaderTokenReader(accessor);
            Assert.Throws<InvalidTokenException>(() => reader.Read());
        }

        [Fact]
        public void ShouldFailWithoutHeader()
        {
            var headers = Mock.Of<IHeaderDictionary>();
            var request = Mock.Of<HttpRequest>(r => r.Headers == headers);
            var context = Mock.Of<HttpContext>(c => c.Request == request);
            var accessor = Mock.Of<IHttpContextAccessor>(a => a.HttpContext == context);
            var reader = new AuthorizationHeaderTokenReader(accessor);
            Assert.Throws<InvalidTokenException>(() => reader.Read());
        }

        [Fact]
        public void ShouldFailWithInvalidHeader()
        {
            var headers = Mock.Of<IHeaderDictionary>(h => h.ContainsKey("Authorization") && h["Authorization"] == new StringValues("INVALID"));
            var request = Mock.Of<HttpRequest>(r => r.Headers == headers);
            var context = Mock.Of<HttpContext>(c => c.Request == request);
            var accessor = Mock.Of<IHttpContextAccessor>(a => a.HttpContext == context);
            var reader = new AuthorizationHeaderTokenReader(accessor);
            Assert.Throws<InvalidTokenException>(() => reader.Read());
        }

        [Fact]
        public void ShouldReturnTokenFromValidHeader()
        {
            var expectedToken = "TOKEN";
            var expectedHeader = "Bearer " + expectedToken;
            var headers = Mock.Of<IHeaderDictionary>(h=>h.ContainsKey("Authorization") && h["Authorization"] == new StringValues(expectedHeader));
            var request = Mock.Of<HttpRequest>(r => r.Headers == headers);
            var context = Mock.Of<HttpContext>(c => c.Request == request);
            var accessor = Mock.Of<IHttpContextAccessor>(a => a.HttpContext == context);

            var reader = new AuthorizationHeaderTokenReader(accessor);
            var actualToken = reader.Read();

            Assert.Equal(expectedToken, actualToken);
        }
    }
}