﻿using LendFoundry.Foundation.Client;
using Moq;
using RestSharp;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Security.Identity.Client.Tests
{

    public class IdentityServiceClientTest
    {
        private IIdentityService IdentityServiceClient { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> MockServiceClient { get; }

        public IdentityServiceClientTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            IdentityServiceClient = new IdentityService(MockServiceClient.Object);
        }

        [Fact]
      public async void CreateUser()
        {
            
            MockServiceClient.Setup(s => s.ExecuteAsync<UserInfo>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new UserInfo {Username="kinjal.s",Email="kinjal.s@sigmainfo.net",IsActive=true,Roles=new[] { "VP of Sales","VP of Operations" } })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.CreateUser(It.IsAny<User>());
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void GetUserRoles()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<List<string>>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new List<string> { "VP of Sales", "VP of Operations" } )
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.GetUserRoles(It.IsAny<string>());
            Assert.Equal("/roles/user/{name}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void GetChildRoles()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<List<string>>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new List<string> { "VP of Sales", "VP of Operations" })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.GetChildRoles(It.IsAny<string>());

          
            Assert.Equal("/roles/{role}/children", Request.Resource);
            
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void GetRoleMembers()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<List<User>>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new List<User> { new User {

                   Username="kinjal.s",Email="kinjal.s@sigmainfo.net",IsActive=true,Roles=new[] { "VP of Sales","VP of Operations" } 
                   } })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.GetRoleMembers(It.IsAny<string>());

          
            Assert.Equal("/roles/{role}/members", Request.Resource);

            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void GetChildrenRoles()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<List<string>>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new List<string> { "VP of Sales", "VP of Operations" })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.GetChildRoles(It.IsAny<List<string>>());


            Assert.Equal("/roles/children", Request.Resource);

            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

        }


        [Fact]
        public async void GetAllRolesFromCurrentUser()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<List<string>>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new List<string> { "VP of Sales", "VP of Operations" })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.GetAllRolesFromCurrentUser();


            Assert.Equal("/roles/user", Request.Resource);

            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void GetUserById()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<UserInfo>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new UserInfo { Username = "kinjal.s", Email = "kinjal.s@sigmainfo.net", IsActive = true, Roles = new[] { "VP of Sales", "VP of Operations" } })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.GetUserById(It.IsAny<string>());


            Assert.Equal("/user/{id}", Request.Resource);

            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void GetUserByUsername()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<UserInfo>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new UserInfo { Username = "kinjal.s", Email = "kinjal.s@sigmainfo.net", IsActive = true, Roles = new[] { "VP of Sales", "VP of Operations" } })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.GetUserByUsername(It.IsAny<string>());


            Assert.Equal("/user/by-username/{username}", Request.Resource);

            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void GetUsersByPasswordExpiring()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<List<UserInfo>>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new List<UserInfo> { new UserInfo { Username = "kinjal.s", Email = "kinjal.s@sigmainfo.net", IsActive = true, Roles = new[] { "VP of Sales", "VP of Operations" } } })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.GetUsersByPasswordExpiring();


            Assert.Equal("users/password-expiring", Request.Resource);

            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void Login()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync<LoginResponse>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(new LoginResponse {Token = "token",UserId="1" })
                .Callback<IRestRequest>(r => Request = r);

            var result = await IdentityServiceClient.Login(It.IsAny<LoginRequest>());


            Assert.Equal("/login", Request.Resource);

            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);

        }

        [Fact]
        public async void RequestPasswordReset()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
               .ReturnsAsync(true)
                .Callback<IRestRequest>(r => Request = r);

            await IdentityServiceClient.RequestPasswordReset(It.IsAny<RequestPasswordResetRequest>());


            Assert.Equal("/request-token", Request.Resource);

            Assert.Equal(Method.POST, Request.Method);
           

        }
    }
}
