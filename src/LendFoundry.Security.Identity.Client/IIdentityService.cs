using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity.Client
{
    public interface IIdentityService
    {
        Task<IUserInfo> CreateUser(IUser user);

        Task<IEnumerable<string>> GetUserRoles(string username);

        Task<IEnumerable<string>> GetChildRoles(string role);

        Task<ICollection<IUser>> GetRoleMembers(string role);

        Task<IEnumerable<string>> GetChildRoles(List<string> roles);

        Task<IEnumerable<string>> GetAllRolesFromCurrentUser();

        Task<IUserInfo> GetUserById(string id);

        Task<IEnumerable<IUserInfo>> GetUsersByPasswordExpiring();

        Task<ILoginResponse> Login(ILoginRequest loginRequest);

        Task<IUserInfo> GetUserByUsername(string username);

        Task RequestPasswordReset(IRequestPasswordResetRequest requestPasswordResetRequest);

        Task<IEnumerable<IUserInfo>> GetAllUsers();

        Task<IEnumerable<IUserInfo>> GetUsersByRoles(List<string> roles);
    }
}