﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace LendFoundry.Security.Identity.Client
{
    public class IdentityService : IIdentityService
    {
        public IdentityService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IUserInfo> CreateUser(IUser user)
        {
            var request = new RestRequest("/", Method.PUT);
            request.AddJsonBody(user);
            var result = await Client.ExecuteAsync<UserInfo>(request);
            return result;
        }

        public async Task<IEnumerable<string>> GetUserRoles(string name)
        {
            var request = new RestRequest("/roles/user/{name}", Method.GET);
            request.AddUrlSegment(nameof(name), name);
            var result = await Client.ExecuteAsync<List<string>>(request);
            return result;
        }

        public async Task<IEnumerable<string>> GetChildRoles(string role)
        {
            var request = new RestRequest("/roles/{role}/children", Method.GET);
            request.AddUrlSegment(nameof(role), role);
            var result = await Client.ExecuteAsync<List<string>>(request);
            return result;
        }

        public async Task<ICollection<IUser>> GetRoleMembers(string role)
        {
            var request = new RestRequest("/roles/{role}/members", Method.GET);
            request.AddUrlSegment(nameof(role), role);
            var result = await Client.ExecuteAsync<List<User>>(request);
            return new List<IUser>(result);
        }

        public async Task<IEnumerable<string>> GetChildRoles(List<string> roles)
        {
            var request = new RestRequest("/roles/children", Method.GET);
            AppendRoles(request, roles);
            return await Client.ExecuteAsync<List<string>>(request);
        }

        public async Task<IEnumerable<string>> GetAllRolesFromCurrentUser()
        {
            var request = new RestRequest("/roles/user", Method.GET);

            return await Client.ExecuteAsync<List<string>>(request);
        }

        public async Task<IUserInfo> GetUserById(string id)
        {
            var request = new RestRequest("/user/{id}", Method.GET);
            request.AddUrlSegment(nameof(id), id);
            return await Client.ExecuteAsync<UserInfo>(request);
        }

        public async Task<IUserInfo> GetUserByUsername(string username)
        {
            var request = new RestRequest("/user/by-username/{username}", Method.GET);
            request.AddUrlSegment(nameof(username), username);
            return await Client.ExecuteAsync<UserInfo>(request);
        }

        public async Task<IEnumerable<IUserInfo>> GetUsersByPasswordExpiring()
        {
            var request = new RestRequest("users/password-expiring", Method.GET);
            return await Client.ExecuteAsync<List<UserInfo>>(request);
        }

        public async Task<ILoginResponse> Login(ILoginRequest loginRequest)
        {
            var request = new RestRequest("/login", Method.POST);
            request.AddJsonBody(loginRequest);
            return await Client.ExecuteAsync<LoginResponse>(request);
        }

        private static void AppendRoles(IRestRequest request, IReadOnlyList<string> roles)
        {
            if (roles == null || !roles.Any())
                return;
            var url = request.Resource;
            for (var index = 0; index < roles.Count; index++)
            {
                var role = roles[index];

                url = url + $"/{{role{ index}}}";
                request.AddUrlSegment($"role{index}", role);
            }
            request.Resource = url;
        }

        public async Task RequestPasswordReset(IRequestPasswordResetRequest requestPasswordResetRequest)
        {
            var request = new RestRequest("/request-token", Method.POST);
            request.AddJsonBody(requestPasswordResetRequest);
            await Client.ExecuteAsync(request);
        }

        public async Task<IEnumerable<IUserInfo>> GetAllUsers()
        {
            var request = new RestRequest("/all/users", Method.GET);
            return await Client.ExecuteAsync<List<UserInfo>>(request);
        }

        public async Task<IEnumerable<IUserInfo>> GetUsersByRoles(List<string> roles)
        {
            var request = new RestRequest("all/users", Method.GET);
            AppendRoles(request, roles);
            return await Client.ExecuteAsync<List<UserInfo>>(request);
        }

    }
}