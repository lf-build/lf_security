using LendFoundry.Security.Tokens;

namespace LendFoundry.Security.Identity.Client
{
    public interface IIdentityServiceFactory
    {
        IIdentityService Create(ITokenReader reader);
    }
}