namespace LendFoundry.Security.Identity.Api.Models
{
    /// <summary>
    /// Username check request
    /// </summary>
    public class CheckUsernameRequest
    {
        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }
    }
}