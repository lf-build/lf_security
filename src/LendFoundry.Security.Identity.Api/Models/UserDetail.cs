﻿using System;

namespace LendFoundry.Security.Identity.Api.Models
{
    /// <summary>
    /// User details
    /// </summary>
    public class UserDetail : IUserInfo
    {
        /// <summary>
        /// User Deail Constructor
        /// </summary>
        /// <param name="user"></param>
        public UserDetail(IUserInfo user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            Id = user.Id;
            Email = user.Email;
            Name = user.Name;
            Username = user.Username;
            Roles = user.Roles;
            IsActive = user.IsActive;
            PasswordExpirationDate = user.PasswordExpirationDate;
        }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Roles
        /// </summary>
        public string[] Roles { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// IsActive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Password Expiration Date
        /// </summary>
        public DateTimeOffset? PasswordExpirationDate { get; set; }
    }
}
