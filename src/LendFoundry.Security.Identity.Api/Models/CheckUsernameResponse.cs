namespace LendFoundry.Security.Identity.Api.Models
{

    /// <summary>
    /// Check username response
    /// </summary>
    public class CheckUsernameResponse 
    {
        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Provided username is available or not
        /// </summary>
        public bool Available { get; set; }
    }
}