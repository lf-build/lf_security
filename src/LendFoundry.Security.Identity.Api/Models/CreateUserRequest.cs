namespace LendFoundry.Security.Identity.Api.Models
{

    /// <summary>
    /// Request object for user Creation
    /// </summary>
    public class CreateUserRequest :ICreateUserRequest
     {
        /// <summary>
        /// Name of user
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// User's email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// User's Roles
        /// </summary>
        public string[] Roles { get; set; }

        /// <summary>
        /// User's password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Password Salt
        /// </summary>
        public string PasswordSalt { get; set; } 

        /// <summary>
        /// User is active
        /// </summary>
        public bool IsActive { get; set; }

     }
}