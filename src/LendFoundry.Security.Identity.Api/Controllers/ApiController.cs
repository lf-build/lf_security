﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity.Api.Models;
using LendFoundry.Security.Tokens;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Security.Identity.Api.Controllers
{
    /// <summary>
    /// API
    /// </summary>
    [Route("/")]
    public class ApiController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identityService"></param>
        /// <param name="logger"></param>
        /// <param name="tokenReader"></param>
        /// <param name="tokenParser"></param>
        public ApiController(IIdentityService identityService, ILogger logger, ITokenReader tokenReader,
            ITokenHandler tokenParser)
        {
            IdentityService = identityService;
            Logger = logger;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
        }

        private IIdentityService IdentityService { get; }

        private ILogger Logger { get; }

        private ITokenReader TokenReader { get; }

        private ITokenHandler TokenParser { get; set; }



        private IActionResult GetStatusCodeResult(int statusCode)
        {
#if DOTNET2
            return new StatusCodeResult(statusCode);
#else
            return new HttpStatusCodeResult(statusCode);
#endif
        }


        /// <summary>
        /// Checks the username.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost("check")]
        [ProducesResponseType(typeof(CheckUsernameResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> CheckUsername([FromBody] CheckUsernameRequest request)
        {
            try
            {
                if (request == null || string.IsNullOrWhiteSpace(request.Username))
                    return ErrorResult.BadRequest("request with name is required");

                return Ok(new CheckUsernameResponse
                {
                    Username = request.Username,
                    Available = await IdentityService.IsUsernameAvailable(request.Username)
                });
            }


            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// Gets the current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(UserDetail), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> GetCurrentUser()
        {
            try
            {
                var currentUser = await IdentityService.GetCurrentUser();
                return Ok(new UserDetail(currentUser));
            }
            catch (AuthenticationException)
            {
                return GetStatusCodeResult(403);
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);

            }
        }

        /// <summary>
        /// Gets all roles.
        /// </summary>
        /// <returns></returns>
        [HttpGet("roles/all")]
        [ProducesResponseType(typeof(string[]), 200)]
        public async Task<IActionResult> GetAllRoles()
        {
            return Ok(await IdentityService.GetAllRoles());
        }

        /// <summary>
        /// Gets all roles from current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet("roles/user")]
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> GetAllRolesFromCurrentUser()
        {
            try
            {
                return Ok(await IdentityService.GetAllRolesFromCurrentUser());
            }
            catch (AuthenticationException)
            {
                return GetStatusCodeResult(403);

            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);

            }
        }

        /// <summary>
        /// Gets all child roles from current user.
        /// </summary>
        /// <returns></returns>
        [HttpGet("roles/user/children")]
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]

        public async Task<IActionResult> GetAllChildRolesFromCurrentUser()
        {
            try
            {
                var roles = await IdentityService.GetAllRolesFromCurrentUser();
                var childRoles = await IdentityService.GetChildrenRoles(roles);
                return Ok(childRoles);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (AuthenticationException)
            {
                return GetStatusCodeResult(403);
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Gets the user roles.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        [HttpGet("roles/user/{name}")]
        [ProducesResponseType(typeof(string[]), 200)]

        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> GetUserRoles(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return ErrorResult.BadRequest("name is required");
                name = WebUtility.UrlDecode(name);
                return Ok(await IdentityService.GetUserRoles(name));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (UserNotFoundException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Gets all child roles.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns></returns>
        [HttpGet("roles/{role}/children")]
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> GetAllChildRoles(string role)
        {
            if (string.IsNullOrWhiteSpace(role))
                return ErrorResult.BadRequest("Role is required");
            role = WebUtility.UrlDecode(role);
            return Ok(await IdentityService.GetChildRoles(role));
        }

        /// <summary>
        /// Gets all parent roles.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns></returns>
        [HttpGet("roles/{role}/parents")]
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllParentRoles(string role)
        {
            if (string.IsNullOrWhiteSpace(role))
                return ErrorResult.BadRequest("Role is required");
            role = WebUtility.UrlDecode(role);
            return Ok(await IdentityService.GetParentRoles(role));
        }

        /// <summary>
        /// Gets the children roles.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        [HttpGet("roles/children/{*roles}")]
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetChildrenRoles(string roles)
        {
            if (string.IsNullOrWhiteSpace(roles))
                return ErrorResult.BadRequest("Value cannot be null or whitespace.", nameof(roles));
            roles = WebUtility.UrlDecode(roles);
            var roleList = SplitRoles(roles);
            return Ok(await IdentityService.GetChildrenRoles(roleList));
        }

        /// <summary>
        /// Gets the role members.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns></returns>
        [HttpGet("roles/{role}/members")]
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> GetRoleMembers(string role)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(role))
                    return ErrorResult.BadRequest("Role is required");

                role = WebUtility.UrlDecode(role);

                return Ok(await IdentityService.GetRoleMembers(role));
            }
            catch (InvalidTokenException)
            {

                return GetStatusCodeResult(403);
            }
            catch (Exception ex)
            {
                Logger.Error("GetRoleMembers raised an error: ", new { ex.Message });
                return new ErrorResult(400, $"Unexpected error occurred! Error: {ex.Message}.");
            }
        }

        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(typeof(UserInfo), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> CreateUser([FromBody] CreateUserRequest request)
        {
            try
            {
                return Ok(await IdentityService.CreateUser(request));
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, $"Invalid {exception.ParamName}.");
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, $"Invalid {exception.ParamName}.");
            }
            catch (UsernameAlreadyExists)
            {
                return ErrorResult.BadRequest($"Username {request.Username} already exists!");
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (MongoWriteException exception)
            {
                Logger.Warn("Mongo exception : ", new { exception.Message });
                return ErrorResult.BadRequest(exception.Message.Contains("duplicate key error collection") ? $"Username {request.Username} already exists!" : exception.Message);
            }
            catch (InvalidOperationException exception)
            {
                Logger.Warn("Invalid operation : ", new { exception.Message });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPut("update")]
        [ProducesResponseType(typeof(UserInfo), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> UpdateUser([FromBody] UserInfo request)
        {
            try
            {
                return Ok(await IdentityService.UpdateUser(request));
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.BadRequest($"Username {request?.Username} not found!");
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, $"Invalid {exception.ParamName}.");
            }
            catch (MongoWriteException exception)
            {
                Logger.Warn("Mongo exception : ", new { exception.Message });
                return ErrorResult.BadRequest(exception.Message.Contains("duplicate key error collection") ? $"Username {request.Username} already exists!" : exception.Message);
            }
            catch (InvalidTokenException)
            {

                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Logins the specified login.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException"></exception>
        [HttpPost("login")]
        [ProducesResponseType(typeof(UserInfo), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> Login([FromBody] LoginRequest login)
        {
            try
            {
                if (login == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                return Ok(await IdentityService.Login(login));
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
            catch (PasswordExpiredException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sendOtprequest"></param>
        /// <returns></returns>
        [HttpPost("/otp")]
        [ProducesResponseType(typeof(SendOtpRequest), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SendOtp([FromBody] SendOtpRequest sendOtprequest)
        {
            try
            {
                if (sendOtprequest == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                return Ok(await IdentityService.SendOtp(sendOtprequest));
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="otpLoginRequest"></param>
        /// <returns></returns>
        [HttpPost("/otp/login")]
        [ProducesResponseType(typeof(LoginResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> LoginWithOtp([FromBody] OtpLoginRequest otpLoginRequest)
        {
            try
            {
                if (otpLoginRequest == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                return Ok(await IdentityService.Login(otpLoginRequest));
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("/logout")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await IdentityService.Logout(TokenReader.Read());
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidTokenException)
            {

                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Determines whether the specified URI is authorized.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        [HttpGet("is-authorized/{*uri}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(403)]
        [ProducesResponseType(typeof(ErrorResult), 400)]

        public async Task<IActionResult> IsAuthorized([FromRoute]string uri)
        {
            try
            {
                return await IdentityService.IsAuthorized(TokenReader.Read(), uri)
                    ? GetStatusCodeResult(200)
                    : GetStatusCodeResult(403);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Determines whether the session is about to expire.
        /// </summary>
        /// <returns></returns>
        [HttpGet("is-expiring")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> IsExpiring()
        {
            try
            {
                return Ok(await IdentityService.IsExpiring());
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }

        }


        /// <summary>
        /// Determines whether the session is about to expire.
        /// </summary>
        /// <returns></returns>
        [HttpGet("renew-token")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> RenewToken()
        {
            try
            {
                return Ok(await IdentityService.RenewToken());
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Requests the password reset.
        /// </summary>
        /// <param name="requestPasswordResetRequest">The request password reset request.</param>
        /// <returns></returns>
        [HttpPost("request-token")]
        [ProducesResponseType(202)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> RequestPasswordReset([FromBody] RequestPasswordResetRequest requestPasswordResetRequest)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(requestPasswordResetRequest.Username))
                    return ErrorResult.BadRequest("name is required");
                if (string.IsNullOrWhiteSpace(requestPasswordResetRequest.Portal))
                    return ErrorResult.BadRequest("portal is required");

                await IdentityService.RequestPasswordReset(requestPasswordResetRequest);
                return GetStatusCodeResult(202);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.BadRequest("User not authenticated.");
            }
            catch (InvalidTokenException)
            {

                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="reset">The reset.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException"></exception>
        [HttpPost("reset-password/{token}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> ResetPassword([FromRoute] string token, [FromBody] LoginRequest reset)
        {
            try
            {
                if (reset == null)
                    throw new InvalidUserOrPasswordException(string.Empty);
                await IdentityService.ResetPassword(reset.Username, token, reset.Password);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (AccountLockedException exception)
            {
                Logger.Warn("Account is locked for {Username}", new { exception.Username });
                return new ErrorResult(400, exception.Message);
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.BadRequest("User not authenticated.");
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
            catch (InvalidOperationException exception)
            {
                Logger.Warn("Invalid operation : ", new { exception.Message });
                return new ErrorResult(400, exception.Message);
            }

        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="reset">The reset.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException">
        /// </exception>
        /// <exception cref="System.ArgumentException">Password and Confirm Password does not match</exception>
        [HttpPut("change-password")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(403)]

        public async Task<IActionResult> ChangePassword([FromBody] PasswordChangeRequest reset)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (reset == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (reset.Password != reset.ConfirmPassword)
                    throw new ArgumentException("Password and Confirm Password does not match");


                await IdentityService.ChangePassword(token.Subject, reset.CurrentPassword, reset.Password);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "There was an error with your Current Password.");
            }
            catch (UserNotFoundException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "There was an error with your Current Password.");
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
            catch (InvalidOperationException exception)
            {
                Logger.Warn("Invalid operation : ", new { exception.Message });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// Changes the name.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException">
        /// </exception>
        [HttpPut("change-name")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> ChangeName([FromBody] NameChangeRequest user)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (user == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                await IdentityService.ChangeName(token.Subject, user.Name);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.BadRequest("User not authenticated.");
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Activates the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException"></exception>
        /// <exception cref="System.ArgumentException">UserName is Required</exception>
        [HttpPut("activate/{userName}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> ActivateUser(string userName)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentException("UserName is Required");


                await IdentityService.ActivateUser(userName);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.BadRequest("User not authenticated.");
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Des the activate user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException"></exception>
        /// <exception cref="System.ArgumentException">UserName is Required</exception>
        [HttpPut("deactivate/{userName}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> DeActivateUser(string userName)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentException("UserName is Required");

                if (string.Equals(token.Subject, userName, StringComparison.InvariantCultureIgnoreCase))
                    throw new ArgumentException("User cannot deactivate itself");

                await IdentityService.DeActivateUser(userName);
                return GetStatusCodeResult(204);
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.BadRequest("User not authenticated.");
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }


        /// <summary>
        /// Assigns the roles to user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException"></exception>
        /// <exception cref="System.ArgumentException">
        /// UserName is Required
        /// or
        /// User Role is Required
        /// </exception>
        [HttpPut("{userName}/roles/{*roles}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> AssignRolesToUser(string userName, string roles)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentException("UserName is Required");

                if (string.IsNullOrWhiteSpace(roles))
                    throw new ArgumentException("User Role is Required");

                roles = WebUtility.UrlDecode(roles);
                await IdentityService.AssignRolesToUser(userName, SplitRoles(roles));
                return GetStatusCodeResult(204);
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.BadRequest("User not authenticated.");
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }

        /// <summary>
        /// Removes the role from user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException"></exception>
        /// <exception cref="System.ArgumentException">
        /// UserName is Required
        /// or
        /// User Role is Required
        /// </exception>
        [HttpDelete("{userName}/roles/{*roles}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> RemoveRoleFromUser(string userName, string roles)
        {
            try
            {
                var token = TokenParser.Parse(TokenReader.Read());
                if (string.IsNullOrWhiteSpace(token?.Subject))
                    throw new InvalidUserOrPasswordException(string.Empty);

                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentException("UserName is Required");

                if (string.IsNullOrWhiteSpace(roles))
                    throw new ArgumentException("User Role is Required");

                roles = WebUtility.UrlDecode(roles);
                await IdentityService.RemoveRolesFromUser(userName, SplitRoles(roles));
                return GetStatusCodeResult(204);
            }
            catch (ArgumentNullException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.BadRequest("User not authenticated.");
            }
            catch (InvalidTokenException)
            {
                return GetStatusCodeResult(403);
            }
        }


        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <returns></returns>
        [HttpGet("all/users")]

        [ProducesResponseType(typeof(List<UserInfo>), 200)]

        public async Task<IActionResult> GetUsers()
        {
            return Ok(await IdentityService.GetAllUsers());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        [HttpGet("all/users/{*roles}")]
        [ProducesResponseType(typeof(List<UserInfo>), 200)]

        public async Task<IActionResult> GetUsersByRoles(string roles)
        {
            return Ok(await IdentityService.GetUsersByRoles(SplitRoles(roles)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("users/password-expiring")]
        [ProducesResponseType(typeof(List<UserInfo>), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetUsersByPasswordExpiring()
        {
            try
            {
                return Ok(await IdentityService.GetUsersByPasswordExpiring());
            }
            catch (ArgumentOutOfRangeException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
        }

        /// <summary>
        /// Gets the user by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("user/{id}")]
        [ProducesResponseType(typeof(UserInfo), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetUserById(string id)
        {
            try
            {
                return Ok(await IdentityService.GetUserById(id));
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.NotFound($"User with id: {id} not found!");
            }
        }

        /// <summary>
        /// Gets the user by username.
        /// </summary>
        /// <param name="username">The Username.</param>
        /// <returns></returns>
        [HttpGet("user/by-username/{username}")]
        [ProducesResponseType(typeof(List<User>), 200)]
        public async Task<IActionResult> GetUserByUsername(string username)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(username))
                    return ErrorResult.BadRequest("username is required");
                username = WebUtility.UrlDecode(username);

                return Ok(await IdentityService.GetUser(username));
            }
            catch (UserNotFoundException)
            {
                return ErrorResult.NotFound($"Username {username} not found!");
            }
        }

        /// <summary>
        /// Splits the roles.
        /// </summary>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        private static List<string> SplitRoles(string roles)
        {
            return string.IsNullOrWhiteSpace(roles)
                ? new List<string>()
                : roles.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}