namespace LendFoundry.Security.Tokens
{
    public interface ISecurityTokenConfig
    {
        byte[] SecretKey { get; }
    }
}