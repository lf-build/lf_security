using System;

namespace LendFoundry.Security.Tokens
{
    public interface IToken
    {
        string Issuer { get; }

        DateTime IssuedAt { get; }

        DateTime? Expiration { get; }

        string Subject { get; }

        string Tenant { get; }

        string Value { get; }

        string[] Scope { get; }

        bool IsValid { get; }
    }
}