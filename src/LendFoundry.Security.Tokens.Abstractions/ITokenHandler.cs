using System;

namespace LendFoundry.Security.Tokens
{
    public interface ITokenHandler
    {
        IToken Issue(string tenant, string issuer);
        IToken Issue(string tenant, string issuer, DateTime? expiration, string subject, string[] scope);
        void Write(IToken token);
        IToken Parse(string value = null);
    }
}