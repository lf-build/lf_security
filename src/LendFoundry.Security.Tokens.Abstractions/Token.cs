using System;
using Newtonsoft.Json;
namespace LendFoundry.Security.Tokens
{
    public class Token : IToken
    {
        [JsonProperty(PropertyName = "iss")]
        public string Issuer { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        [JsonProperty(PropertyName = "iat")]
        public DateTime IssuedAt { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        [JsonProperty(PropertyName = "exp")]
        public DateTime? Expiration { get; set; }

        [JsonProperty(PropertyName = "sub")]
        public string Subject { get; set; }

        [JsonProperty(PropertyName = "tenant")]
        public string Tenant { get; set; }

        [JsonProperty(PropertyName = "scope")]
        public string[] Scope { get; set; }

        [JsonIgnore]
        public string Value { get; set; }

        public bool IsValid => !string.IsNullOrEmpty(Issuer) && !string.IsNullOrEmpty(Tenant) && (!Expiration.HasValue || Expiration > DateTime.UtcNow);
    }

    public class DateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Integer)
                return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds((long)reader.Value);
            else
                return reader.Value;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value.GetType() == typeof(DateTime))
            {
                DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var expiration = ((DateTime)value);
                TimeSpan result = expiration.Subtract(dt);
                int seconds = Convert.ToInt32(result.TotalSeconds);
                writer.WriteValue(seconds);
            }
            else
            {
                writer.WriteValue(value);
            }
        }
    }

}