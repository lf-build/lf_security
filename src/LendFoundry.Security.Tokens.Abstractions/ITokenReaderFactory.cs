namespace LendFoundry.Security.Tokens
{
    public interface ITokenReaderFactory
    {
        ITokenReader Create(string tenant, string issuer);
    }
}