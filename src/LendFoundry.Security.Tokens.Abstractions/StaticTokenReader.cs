namespace LendFoundry.Security.Tokens
{
    public class StaticTokenReader : ITokenReader
    {
        public StaticTokenReader(string token)
        {
            Token = token;
        }

        private string Token { get; }

        public string Read()
        {
            return Token;
        }
    }
}