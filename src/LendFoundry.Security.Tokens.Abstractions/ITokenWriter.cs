namespace LendFoundry.Security.Tokens
{
    public interface ITokenWriter
    {
        void Write(IToken token);
    }
}