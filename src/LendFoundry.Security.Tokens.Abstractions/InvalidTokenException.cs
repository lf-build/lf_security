﻿using System;

namespace LendFoundry.Security.Tokens
{
    public class InvalidTokenException : Exception
    {
        public InvalidTokenException(string message) : base(message)
        {

        }
    }
}
