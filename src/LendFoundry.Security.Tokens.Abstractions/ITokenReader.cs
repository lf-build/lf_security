namespace LendFoundry.Security.Tokens
{
    public interface ITokenReader
    {
        string Read();
    }
}