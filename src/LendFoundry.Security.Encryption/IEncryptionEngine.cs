﻿namespace LendFoundry.Security.Encryption
{
    public interface IEncryptionEngine
    {
        string Decrypt(string encryptedMessage, byte[] key);
        string Encrypt(string secretMessage, byte[] key);
    }
}