#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Security.Encryption
{
    public static class EncryptionHandlerExtensions
    {
        public static IServiceCollection AddEncryptionHandler(this IServiceCollection services)
        {
            services.AddSingleton<ISecurityTokenConfig, SecurityTokenConfig>();
            services.AddTransient<IEncryptionService, EncryptionService>();
            services.AddTransient<ISerializer, JsonSerializer>();
            services.AddTransient<IEncryptionEngine, AesGcm>();

            return services;
        }
    }
}