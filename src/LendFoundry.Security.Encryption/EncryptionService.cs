﻿namespace LendFoundry.Security.Encryption
{
    
    public class EncryptionService : IEncryptionService
    {
        public EncryptionService(ISerializer serializer, IEncryptionEngine encryptionEngine, ISecurityTokenConfig securityTokenConfig)
        {
            SecurityTokenConfig = securityTokenConfig;
            EncryptionEngine = encryptionEngine;
            Serializer = serializer;
        }

     
        private IEncryptionEngine EncryptionEngine { get; }

        private ISecurityTokenConfig SecurityTokenConfig { get; set; }

        private ISerializer Serializer { get; }

        public string Encrypt<T>(T input)
        {
            return EncryptionEngine.Encrypt(Serializer.Serialize(input), SecurityTokenConfig.SecretKey);
        }

        public T Decrypt<T>(string input)
        {
            return Serializer.Deserialize<T>(EncryptionEngine.Decrypt(input, SecurityTokenConfig.SecretKey));
        }

        
    }
}