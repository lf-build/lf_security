﻿namespace LendFoundry.Security.Encryption
{
    public interface ISerializer
    {
        T Deserialize<T>(string jsonString);
        string Serialize<T>(T value);
    }
}