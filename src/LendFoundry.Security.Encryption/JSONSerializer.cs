﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LendFoundry.Security.Encryption
{
    public class JsonSerializer: ISerializer
    {
        private JsonSerializerSettings Settings { get; } = new JsonSerializerSettings()
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public T Deserialize<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString, Settings);
        }

        public string Serialize<T>(T value)
        {
            return JsonConvert.SerializeObject(value, Settings);
        }
    }
}
