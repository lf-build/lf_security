namespace LendFoundry.Security.Encryption
{
    public interface ISecurityTokenConfig
    {
        byte[] SecretKey { get; }
    }
}