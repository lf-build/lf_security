﻿namespace LendFoundry.Security.Encryption
{
    public interface IEncryptionService
    {
        T Decrypt<T>(string report);
        string Encrypt<T>(T report);
    }
}