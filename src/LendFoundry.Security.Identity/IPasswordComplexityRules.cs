﻿namespace LendFoundry.Security.Identity
{
    public interface IPasswordComplexityRules
    {
        bool Complies(string password);
    
    }
}
