using System;
using System.Security.Cryptography;
#if DOTNET2
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
#else
using Microsoft.AspNet.Cryptography.KeyDerivation;
#endif
namespace LendFoundry.Security.Identity
{
    public class Pbkdf2Crypt : ICrypt
    {
        public string CreateSalt()
        {
            var salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
                rng.GetBytes(salt);
            return Convert.ToBase64String(salt);
        }

        public string Encrypt(string value, string salt)
        {
            var encrypted = KeyDerivation.Pbkdf2
            (
                password: value,
                salt: Convert.FromBase64String(salt),
                prf: KeyDerivationPrf.HMACSHA512,
                iterationCount: 10000,
                numBytesRequested: 2048/8
            );

            return Convert.ToBase64String(encrypted);
        }
    }
}