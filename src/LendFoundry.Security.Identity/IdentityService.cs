using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Security.Tokens;
using System.Linq;
using System.Security.Authentication;
using LendFoundry.Configuration;
using LendFoundry.Email;
using LendFoundry.Tenant.Client;
using System.Text.RegularExpressions;
#if DOTNET2
using Microsoft.AspNetCore.Routing.Template;
using Microsoft.AspNetCore.Routing;
#endif
using LendFoundry.Sms;

namespace LendFoundry.Security.Identity
{
    public class IdentityService : IIdentityService
    {
        public IdentityService(IConfiguration configuration, IUserRepository userRepository, ICrypt crypt, ISessionManager sessionManager, IEventHubClient eventHub, ITokenHandler tokenHandler, IEmailService emailService, IConfigurationServiceFactory configurationServiceFactory, ITokenReader tokenReader, ITenantService tenant, IRoleToPermissionConverter roleToPermissionConverter, IPasswordComplexityRules passwordComplexityRules, ISmsService smsService, IUserOtpRepository userOtpRepository)
        {
            Configuration = configuration;
            UserRepository = userRepository;
            EventHub = eventHub;
            Crypt = crypt;
            SessionManager = sessionManager;
            TokenHandler = tokenHandler;
            EmailService = emailService;
            ConfigurationServiceFactory = configurationServiceFactory;
            TokenReader = tokenReader;
            Tenant = tenant;
            RoleToPermissionConverter = roleToPermissionConverter;
            PasswordComplexityRules = passwordComplexityRules;
            SmsService = smsService;
            UserOtpRepository = userOtpRepository;
        }

        #region Variables

        private IUserOtpRepository UserOtpRepository { get; set; }
        private ISmsService SmsService { get; set; }
        private IPasswordComplexityRules PasswordComplexityRules { get; }
        private IConfiguration Configuration { get; }
        private IUserRepository UserRepository { get; }
        private IEventHubClient EventHub { get; }
        private ICrypt Crypt { get; }
        private ISessionManager SessionManager { get; }
        private ITokenHandler TokenHandler { get; }
        private IEmailService EmailService { get; }
        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private ITokenReader TokenReader { get; }
        private IRoleToPermissionConverter RoleToPermissionConverter { get; }
        private ITenantService Tenant { get; }

        #endregion

        public async Task<bool> IsUsernameAvailable(string username)
        {
            return (await UserRepository.Get(username)) == null;
        }

        public async Task<IUserInfo> GetCurrentUser()
        {
            var token = TokenReader.Read();
            if (await IsAuthenticated(token))
                return await GetUser(TokenHandler.Parse(token).Subject);
            throw new AuthenticationException();
        }

        public async Task<IUserInfo> CreateUser(ICreateUserRequest userRequest)
        {

            if (userRequest == null)
                throw new ArgumentNullException(nameof(userRequest));

            if (string.IsNullOrEmpty(userRequest.Name))
                throw new ArgumentNullException(nameof(userRequest.Name));

            if (string.IsNullOrEmpty(userRequest.Username))
                throw new ArgumentNullException(nameof(userRequest.Username));

            IUser user = new User()
            {
                Name = userRequest.Name,
                Email = userRequest.Email,
                Username = userRequest.Username,
                Roles = userRequest.Roles,
                Password = userRequest.Password,
                PasswordSalt = userRequest.PasswordSalt,
                IsActive = userRequest.IsActive
            };

            ValidateEmail(user.Email);

            ValidatePassword(user.Password, null);

            await ValidateRolesAsync(user.Roles);

            var anotherUser = await UserRepository.Get(user.Username);
            if (anotherUser != null)
                throw new UsernameAlreadyExists(user.Username);

            user.PasswordSalt = Crypt.CreateSalt();
            user.Password = Crypt.Encrypt(user.Password, user.PasswordSalt);

            user.IsActive = true;

            if (Configuration.PasswordExpirationDays > 0)
                user.PasswordExpirationDate = DateTimeOffset.UtcNow.Date.AddDays(Configuration.PasswordExpirationDays);

            await UserRepository.Add(user);
            await EventHub.Publish("UserCreated", new UserEvent(user.Username, user.Id));
            return user;
        }

        private async Task ValidateRolesAsync(string[] roles)
        {
            var existingRoles = await GetAllRoles();

            bool isAllRolesExists = !roles.Except(existingRoles).Any();

            if (!isAllRolesExists)
                throw new ArgumentException(string.Empty, nameof(roles));
        }

        private static void ValidateEmail(string email)
        {
            bool isValid = Regex.IsMatch(email,
                @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);

            if (!isValid)
                throw new ArgumentException("Email property doesn't have valid value", nameof(email));
        }

        public async Task<IEnumerable<IUserInfo>> GetAllUsers()
        {
            return await UserRepository.All();
        }

        public async Task<IEnumerable<IUserInfo>> GetUsersByRoles(List<string> roles)
        {
            if (roles == null || roles.Count == 0)
                throw new ArgumentNullException(nameof(roles));

            return await UserRepository.GetUsersByRoles(roles);
        }

        public async Task<IEnumerable<IUserInfo>> GetUsersByPasswordExpiring()
        {
            return await UserRepository.GetUsersByPasswordExpiring(Configuration.PasswordExpirationThreshold);
        }

        public async Task<IUser> GetUser(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await UserRepository.Get(username);
            if (user == null)
                throw new UserNotFoundException(username);

            return user;
        }

        public async Task<IUserInfo> GetUserById(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            var user = await UserRepository.GetById(id);
            if (user == null)
                throw new UserNotFoundException(id);

            return user;
        }

        public async Task<IUserInfo> UpdateUser(IUserInfo user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(user.Name))
                throw new ArgumentNullException(nameof(user.Name));

            if (string.IsNullOrEmpty(user.Username))
                throw new ArgumentNullException(nameof(user.Username));

            if (!string.IsNullOrEmpty(user.Email))
                ValidateEmail(user.Email);

            if (user.Roles != null && user.Roles.Any())
                await ValidateRolesAsync(user.Roles);

            var existUser = await UserRepository.Get(user.Username);
            if (existUser == null)
                throw new UserNotFoundException(user.Id);

            await UserRepository.Update(user);
            await EventHub.Publish("UserUpdated", new UserEvent(user.Username, user.Id));
            return user;
        }

        public async Task RemoveUser(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await UserRepository.Get(username);

            if (user == null)
                throw new UserNotFoundException(username);

            await UserRepository.Remove(user);
            await EventHub.Publish("UserRemoved", new UserEvent(user.Username, user.Id));
        }

        public async Task ChangePassword(string username, string currentPassword, string newPassword)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            if (string.IsNullOrEmpty(currentPassword))
                throw new ArgumentNullException(nameof(currentPassword));

            var user = await UserRepository.Get(username);

            if (user == null)
                throw new UserNotFoundException(username);

            ValidatePassword(newPassword, user);

            if (!user.Password.Equals(Crypt.Encrypt(currentPassword, user.PasswordSalt)))
                throw new ArgumentException("There was an error with your Current Password. Please try again.");

            await Authenticate(user, user.Password != Crypt.Encrypt(currentPassword, user.PasswordSalt));

            DateTimeOffset? passwordExpirationDate = null;
            if (Configuration.PasswordExpirationDays > 0)
                passwordExpirationDate = DateTimeOffset.UtcNow.Date.AddDays(Configuration.PasswordExpirationDays);

            await UserRepository.ChangePassword(user, Crypt.Encrypt(newPassword, user.PasswordSalt), passwordExpirationDate, GetOldPasswords(user));
            await EventHub.Publish("UserPasswordChanged", new UserEvent(user.Username, user.Id));
        }

        public async Task RequestPasswordReset(IRequestPasswordResetRequest requestPasswordResetRequest)
        {
            if (string.IsNullOrEmpty(requestPasswordResetRequest.Username))
                throw new ArgumentNullException(nameof(requestPasswordResetRequest.Username));

            if (string.IsNullOrEmpty(requestPasswordResetRequest.Portal))
                throw new ArgumentNullException(nameof(requestPasswordResetRequest.Portal));

            var user = await UserRepository.Get(requestPasswordResetRequest.Username);

            if (user == null)
                throw new UserNotFoundException(requestPasswordResetRequest.Username);

            var resetToken = $"{Guid.NewGuid():N}{Guid.NewGuid():N}";

            var alloyConfig = GetAlloyConfiguration();
            if (!alloyConfig.SecurityResetUrls.ContainsKey(requestPasswordResetRequest.Portal))
                throw new ArgumentNullException(nameof(requestPasswordResetRequest.Portal));

            await UserRepository.SetResetToken(user, resetToken, DateTime.UtcNow.AddMinutes(Configuration.TokenTimeout));

            if(Configuration.ActiveProcessTemplate)
            {
                await EmailService.Send(Configuration.ResetPasswordTemplate, Configuration.ResetPasswordTemplateVersion, new
                {
                    user.Name,
                    user.Email,
                    user.Username,
                    ResetToken = resetToken,
                    LoginUrl = string.Format(alloyConfig.SecurityResetUrls[requestPasswordResetRequest.Portal], resetToken),
                    Configuration.TokenTimeout,
                    Data = requestPasswordResetRequest.Data
                });
            }
            else
            {
                await EmailService.Send(Configuration.ResetPasswordTemplate, new
                {
                    user.Name,
                    user.Email,
                    user.Username,
                    ResetToken = resetToken,
                    LoginUrl = string.Format(alloyConfig.SecurityResetUrls[requestPasswordResetRequest.Portal], resetToken),
                    Configuration.TokenTimeout,
                    Data = requestPasswordResetRequest.Data
                });
            }
            await EventHub.Publish("UserPasswordResetRequested", new UserPasswordResetRequestedEvent(user.Username, user.Id, resetToken));
        }

        private AlloyConfiguration GetAlloyConfiguration()
        {
            var serviceName = "alloy";
            var configurationService = ConfigurationServiceFactory.Create<AlloyConfiguration>(serviceName, TokenReader);
            var configuration = configurationService.Get();
            if (configuration == null)
                throw new Exception($"Configuration not found for '{serviceName}'");
            return configuration;
        }

        public async Task ResetPassword(string username, string token, string password)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await UserRepository.Get(username);
            if (user == null)
                throw new UserNotFoundException(username);

            ValidatePassword(password, user);

            if (user.ResetToken != token)
                throw new ArgumentException("The reset password token is invalid.");

            if (user.ResetTokenExpiration < DateTime.UtcNow)
                throw new ArgumentException("The reset password token is expired.");

            DateTimeOffset? passwordExpirationDate = null;
            if (Configuration.PasswordExpirationDays > 0)
                passwordExpirationDate = DateTimeOffset.UtcNow.Date.AddDays(Configuration.PasswordExpirationDays);

            await UserRepository.ChangePassword(user, Crypt.Encrypt(password, user.PasswordSalt), passwordExpirationDate, GetOldPasswords(user));
            await EventHub.Publish("UserPasswordChanged", new UserEvent(user.Username, user.Id));
        }

        public async Task<ILoginResponse> Login(ILoginRequest loginRequest)
        {
            return await GenericLogin(loginRequest.Username, loginRequest.Password, loginRequest.Portal, null, false);
        }

        public async Task<ILoginResponse> Login(IOtpLoginRequest OtpLoginRequest)
        {
            return await GenericLogin(OtpLoginRequest.Username, null, OtpLoginRequest.Portal, OtpLoginRequest.Otp, true);
        }

        private async Task<LoginResponse> GenericLogin(string username, string password, string portal, string otp, bool isOtp)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            if (isOtp && string.IsNullOrEmpty(otp))
                throw new ArgumentNullException(nameof(otp));

            if (!isOtp && string.IsNullOrEmpty(password))
                throw new ArgumentNullException(nameof(password));

            if (string.IsNullOrEmpty(portal))
                throw new ArgumentNullException(nameof(portal));

            var user = await UserRepository.Get(username);

            if (user == null)
                throw new InvalidUserOrPasswordException(username);

            if (isOtp && !user.IsActive)
                throw new InvalidUserOrPasswordException(user.Username);

            bool isValid = false;
            IUserOtp userOtpDetails = null;
            double expiryTime = 0;
            if (isOtp)
            {

                if(!Configuration.ThirdPartyOtp)
                {
                    userOtpDetails = UserOtpRepository.GetByOtpCode(username, otp).Result;
                    if (userOtpDetails == null)
                        throw new InvalidUserOrPasswordException("Otp code not found in store");

                    expiryTime = (DateTimeOffset.UtcNow - userOtpDetails.ExpirationDate).TotalMinutes;
                    if(userOtpDetails == null || expiryTime > Configuration.OtpExpirationMinutes)
                    {
                        isValid = true;    
                    }
                }
                else
                {
                    try
                    {
                        await SmsService.VerifyOtp("user", user.Id, new OtpVerifyRequest() { Mobile = username, Otp = otp });    
                    }
                    catch (System.Exception)
                    {
                        isValid = true;
                    }                                     
                }
            }
            else if (!isOtp)
            {
                isValid = user.Password != Crypt.Encrypt(password, user.PasswordSalt);
            }

            await Authenticate(user, isValid);

            if (!isOtp && (user.PasswordExpirationDate != null && user.PasswordExpirationDate.Value < DateTimeOffset.UtcNow.Date))
                throw new PasswordExpiredException(username, "Password is expired please reset your password.");

            var expiration = DateTime.UtcNow.AddMinutes(Configuration.SessionTimeout);

            var scope = RoleToPermissionConverter.ToPermission(user.Roles.ToList()).ToArray();
            if (!scope.Contains(portal))
                throw new InvalidUserOrPasswordException(username);

            var token = TokenHandler.Issue(Tenant.Current.Id, "lendfoundry", expiration, user.Username, scope);

            var rawToken = await SessionManager.Create(token);
            await EventHub.Publish("UserLoggedIn", new UserEvent(user.Username, user.Id));

            if (isOtp)
                await UserOtpRepository.Delete(username);

            return new LoginResponse() { Token = rawToken, UserId = user.Id };
        }

        public async Task<IOtpRequest> SendOtp(IOtpRequest sendOtpRequest)
        {
            if (string.IsNullOrEmpty(sendOtpRequest.Username))
                throw new ArgumentNullException(nameof(sendOtpRequest.Username));

            var user = await UserRepository.Get(sendOtpRequest.Username);

            if (user == null)
                throw new InvalidUserOrPasswordException(sendOtpRequest.Username);

            if (!user.IsActive)
                throw new InvalidUserOrPasswordException(user.Username);

            if(!Configuration.GenerateOtp && !Configuration.ThirdPartyOtp)
            {
                throw new ArgumentNullException($"No support for {nameof(Configuration.GenerateOtp)} as {Configuration.GenerateOtp} and {nameof(Configuration.ThirdPartyOtp)} as {Configuration.ThirdPartyOtp}");
            }
            
            var randomGenerate = GenerateOtp();
            
            if(!Configuration.ThirdPartyOtp)
            {
                var otpPreFix = Configuration.PhonePrefix;
                var smsTemplateName = Configuration.Otpsmstemplatename;
                
                object payload = new { RecipientPhone = otpPreFix + sendOtpRequest.Username, Message = randomGenerate, otpcode = randomGenerate };
                
                if (!string.IsNullOrEmpty(smsTemplateName))
                {
                    if(Configuration.ActiveProcessTemplate)
                    {
                        await SmsService.Send("user", user.Id, smsTemplateName, payload);
                    }
                    else
                    {
                        await SmsService.Send("user", user.Id, smsTemplateName, "1.0", payload);
                    }
                    
                }
                else
                {
                    await SmsService.SendSms("user", user.Id, payload);
                }   
            }
            else
            {
                await SmsService.SendOtp("user", user.Id,new OtpSendRequest(){Mobile = sendOtpRequest.Username, Email = user.Email, Otp = Configuration.GenerateOtp ? randomGenerate : string.Empty});
            }
            
            var userDetails = UserOtpRepository.GetByUserName(sendOtpRequest.Username).Result;

            if (userDetails == null)
            {
                UserOtp userOtp = new UserOtp
                {
                    OtpCode = randomGenerate,
                    Username = sendOtpRequest.Username,
                    ExpirationDate = DateTimeOffset.UtcNow.AddMinutes(Configuration.OtpExpirationMinutes)
                };

                await UserOtpRepository.Add(userOtp);
            }
            else
            {
                userDetails.OtpCode = randomGenerate;
                userDetails.ExpirationDate = DateTimeOffset.UtcNow.AddMinutes(Configuration.OtpExpirationMinutes);
                await UserOtpRepository.Update(userDetails);
            }
            return sendOtpRequest;
        }

        public async Task<bool> IsAuthenticated(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            return await SessionManager.IsValid(token);
        }

        public async Task<bool> IsAuthorized(string token, string uri)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            return await SessionManager.IsValid(token) && HasAccess(TokenHandler.Parse(token), uri);
        }

        public async Task<bool> IsExpiring()
        {
            var token = TokenReader.Read();

            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            if (!await SessionManager.IsValid(token))
                throw new InvalidTokenException("Token is expire or invalid");

            var userToken = TokenHandler.Parse(token);
            if (userToken.Expiration == null)
                throw new InvalidTokenException("Token is expire or invalid");

            var remainingTime = (userToken.Expiration.Value - DateTime.UtcNow).TotalMinutes;
            if (remainingTime <= 0 || remainingTime <= Configuration.TokenExpirationThreshold)
                return true;
            return false;
        }

        public async Task<string> RenewToken()
        {
            var rawToken = TokenReader.Read();

            if (string.IsNullOrEmpty(rawToken))
                throw new ArgumentNullException(nameof(rawToken));

            if (!await SessionManager.IsValid(rawToken))
                throw new InvalidTokenException("Token is expire or invalid");

            await SessionManager.Expire(rawToken);

            var userToken = TokenHandler.Parse(rawToken);

            var expiration = DateTime.UtcNow.AddMinutes(Configuration.SessionTimeout);

            var token = TokenHandler.Issue(userToken.Tenant, userToken.Issuer, expiration, userToken.Subject, userToken.Scope);

            rawToken = await SessionManager.Create(token);

            return rawToken;
        }

        public async Task Logout(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            if (await SessionManager.Expire(token))
            {
                var username = TokenHandler.Parse(token).Subject;
                var user = await UserRepository.Get(username);
                if (user != null)
                    await EventHub.Publish("UserLoggedOut", new UserEvent(user.Username, user.Id));
            }
        }

        public async Task<IEnumerable<string>> GetAllRolesFromCurrentUser()
        {
            var user = await GetCurrentUser();
            if (user == null)
                throw new UserNotFoundException("");
            return user.Roles;
        }

        public async Task<IEnumerable<string>> GetUserRoles(string username)
        {
            var user = await GetUser(username);
            if (user == null)
                throw new UserNotFoundException(username);
            return user.Roles;
        }

        public async Task<IEnumerable<string>> GetAllRoles()
        {
            await Task.Yield();

            var allRoles = new List<string>();
            foreach (var role in Configuration.Roles)
                allRoles.AddRange(GetRoleNamesFromPermission(role));
            return allRoles;
        }

        public async Task<IEnumerable<string>> GetChildRoles(string role)
        {
            await Task.Yield();

            var childRoles = new List<string>();
            var found = GetRoleByName(Configuration.Roles, role);
            if (found.Value == null)
                return new List<string>();

            foreach (var child in found.Value.Roles)
                childRoles.AddRange(GetRoleNamesFromPermission(child));
            return childRoles;
        }

        public async Task<IEnumerable<string>> GetParentRoles(string role)
        {
            await Task.Yield();

            var result = GetParents(Configuration.Roles, role);
            return result.Item1 ? result.Item2 : new List<string>();
        }

        public async Task<IEnumerable<string>> GetChildrenRoles(IEnumerable<string> roles)
        {
            await Task.Yield();

            if (roles == null)
                throw new ArgumentException("Value cannot be null or empty", nameof(roles));

            var childRoles = new List<string>();
            foreach (var role in roles)
            {
                var found = GetRoleByName(Configuration.Roles, role);
                if (found.Value?.Roles != null)
                {
                    foreach (var child in found.Value.Roles)
                        childRoles.AddRange(GetRoleNamesFromPermission(child));
                }
            }
            return childRoles;
        }

        public async Task<IEnumerable<IUser>> GetRoleMembers(string role)
        {
            if (string.IsNullOrEmpty(role))
                throw new ArgumentNullException(nameof(role));

            return await UserRepository.AllWithRole(role);
        }

        public async Task ActivateUser(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("UserName is Required");

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            await UserRepository.SetIsActive(userName, true);
        }

        public async Task DeActivateUser(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("UserName is Required");

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);


            await UserRepository.SetIsActive(userName, false);
        }

        public async Task AssignRolesToUser(string userName, List<string> roles)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("UserName is Required");

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            if (roles == null)
                throw new ArgumentException("Role is Required");
            var rolesToUpdate = roles.Union(user.Roles);
            await UserRepository.AddOrUpdateUserRoles(userName, rolesToUpdate.ToArray());
        }

        public async Task RemoveRolesFromUser(string userName, List<string> roles)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("UserName is Required");

            var user = await UserRepository.Get(userName);

            if (user == null)
                throw new UserNotFoundException(userName);

            if (roles == null)
                throw new ArgumentException("Role is Required");
            var rolesToUpdate = user.Roles.Except(roles);
            await UserRepository.AddOrUpdateUserRoles(userName, rolesToUpdate.ToArray());
        }

        private async Task Authenticate(IUser user, bool isPasswordValid)
        {
            if (!user.IsActive)
                throw new InvalidUserOrPasswordException(user.Username);

            if (IsAccountLocked(user))
            {
                Configuration.AccountLockoutMessage = string.Format(Configuration.AccountLockoutMessage, Configuration.AccountLockoutPeriod);
                throw new AccountLockedException(user.Username, Configuration.AccountLockoutMessage ?? $"Invalid login credentials for {user.Username}");
            }

            if (HasReachedMaxFailedLoginAttempts(user) && !IsInLockoutPeriod(user))
                user.FailedLoginAttempts = 0;

            if (isPasswordValid)
            {
                await RecordFailedLoginAttempt(user);
                Configuration.MaxFailedLoginAttemptsErrorMessage = string.Format(Configuration.MaxFailedLoginAttemptsErrorMessage, Configuration.MaxFailedLoginAttempts);
                throw new InvalidUserOrPasswordException(user.Username, $"{Configuration.MaxFailedLoginAttemptsErrorMessage}");
            }

            if (user.FailedLoginAttempts > 0)
                await ResetFailedLoginAttempts(user);
        }

        private async Task ResetFailedLoginAttempts(IUser user)
        {
            user.FailedLoginAttempts = 0;
            user.LastFailedLoginAttempt = null;
            await UserRepository.UpdateFailedLoginAttempts(user);
        }

        private async Task RecordFailedLoginAttempt(IUser user)
        {
            user.FailedLoginAttempts++;
            user.LastFailedLoginAttempt = DateTimeOffset.UtcNow;
            await UserRepository.UpdateFailedLoginAttempts(user);
        }

        private bool IsInLockoutPeriod(IUser user)
        {
            if (user.LastFailedLoginAttempt == null)
                return false;

            var lockoutTime = (DateTimeOffset.UtcNow - user.LastFailedLoginAttempt.Value).TotalMinutes;

            return lockoutTime < Configuration.AccountLockoutPeriod;
        }

        private bool IsAccountLocked(IUser user)
        {
            return HasReachedMaxFailedLoginAttempts(user) && IsInLockoutPeriod(user);
        }

        private bool HasReachedMaxFailedLoginAttempts(IUser user)
        {
            return user.FailedLoginAttempts >= Configuration.MaxFailedLoginAttempts;
        }

#if DOTNET2

        private static bool HasAccess(IToken token, string uri)
        {
            if (token.Scope == null || !token.Scope.Any())
                return false;

            var baseUri = new Uri("http://lenfoundry.com");
            var request = new Uri($"{baseUri}{uri}");
            return token.Scope.Any(scope =>
            {
                var template = TemplateParser.Parse($"{scope}");
                var matcher = new TemplateMatcher(template, GetDefaults(template));
                var values = new RouteValueDictionary();
                return matcher.TryMatch(request.AbsolutePath, values) &&
                    template.Segments.Count() == values.Keys.Count();
            });
        }

        private static RouteValueDictionary GetDefaults(RouteTemplate parsedTemplate)
        {
            var result = new RouteValueDictionary();
            var parameters = parsedTemplate.Parameters.Where(p => p.DefaultValue != null);

            foreach (var parameter in parameters)
            {
                result.Add(parameter.Name, parameter.DefaultValue);
            }

            return result;
        }

#else

        private static bool HasAccess(IToken token, string uri)
        {
            if (token.Scope == null || !token.Scope.Any())
                return false;

            var baseUri = new Uri("http://lenfoundry.com");
            var request = new Uri($"{baseUri}{uri}");
            return token.Scope.Any(scope =>
            {
                var template = new UriTemplate($"{scope}");
                var match = template.Match(baseUri, request);
                return match != null && template.PathSegmentVariableNames.Count == match.BoundVariables.Count;
            });
        }

#endif

        private void ValidatePassword(string password, IUser user)
        {
            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException(nameof(password));

            if (!PasswordComplexityRules.Complies(password))
                throw new InvalidOperationException(Configuration.PasswordComplexityErrorMessage);

            //It's not new user
            if (user != null)
            {
                if (user.Password == Crypt.Encrypt(password, user.PasswordSalt))
                    throw new ArgumentException($"The new password cannot be accepted. The entered value is the same as current passwords");
                if (user.PasswordHistory != null && user.PasswordHistory.Any())
                {
                    foreach (var oldPassword in user.PasswordHistory)
                    {
                        if (oldPassword.Password == Crypt.Encrypt(password, oldPassword.PasswordSalt))
                            throw new ArgumentException($"The new password cannot be accepted. The entered value is the same as one of the {Configuration.PasswordHistoryLength} previous passwords");
                    }
                }
            }
        }

        private static IEnumerable<string> GetRoleNamesFromPermission(KeyValuePair<string, Permission> role)
        {
            var roles = new List<string>
            {
                role.Key
            };
            if (role.Value.Roles != null)
                foreach (var childRole in role.Value.Roles)
                    roles.AddRange(GetRoleNamesFromPermission(childRole));
            return roles;
        }

        private KeyValuePair<string, Permission> GetRoleByName(CaseInsensitiveDictionary<Permission> roles, string name)
        {
            foreach (var role in roles)
            {
                if (role.Key.Equals(name,StringComparison.InvariantCultureIgnoreCase))
                    return role;
                if (role.Value.Roles != null)
                {
                    var child = GetRoleByName(role.Value.Roles, name);
                    if (!default(KeyValuePair<string, Permission>).Equals(child))
                        return child;
                }
            }
            return default(KeyValuePair<string, Permission>);
        }

        private Tuple<bool, List<string>> GetParents(CaseInsensitiveDictionary<Permission> roles, string name, string parent = null)
        {
            var parents = new List<string>();
            if (parent != null)
                parents.Add(parent);

            foreach (var role in roles)
            {
                if (role.Key.Equals(name))
                {
                    return new Tuple<bool, List<string>>(true, parents);
                }
                if (role.Value.Roles != null)
                {
                    var result = GetParents(role.Value.Roles, name, role.Key);
                    if (result.Item1)
                    {
                        parents.AddRange(result.Item2);
                        return new Tuple<bool, List<string>>(true, parents);
                    }
                }
            }
            return new Tuple<bool, List<string>>(false, parents);
        }

        public async Task ChangeName(string username, string name)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            var user = await UserRepository.Get(username);

            if (user == null)
                throw new UserNotFoundException(username);

            await UserRepository.ChangeName(user, name);
        }

        private List<UserPassword> GetOldPasswords(IUser user)
        {
            List<UserPassword> oldPasswords = new List<UserPassword>();
            if (Configuration.PasswordHistoryLength > 0)
            {
                if (user.PasswordHistory != null)
                    oldPasswords = user.PasswordHistory;
                if (oldPasswords.Count() >= Configuration.PasswordHistoryLength)
                    oldPasswords.RemoveAt(0);
                oldPasswords.Add(new UserPassword() { Password = user.Password, PasswordSalt = user.PasswordSalt });
            }

            return oldPasswords;
        }

        private string GenerateOtp()
        {
            int lenthofpass = 6;
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string otp = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < lenthofpass; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                otp += temp;
            }
            return otp;
        }


    }
}