using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Security.Identity
{
    public class RoleToPermissionConverter : IRoleToPermissionConverter
    {
        public RoleToPermissionConverter(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public IEnumerable<string> ToPermission(List<string> roles)
        {
            if (roles == null || !roles.Any()) throw new ArgumentNullException(nameof(roles));

            var permissions =
                roles.Select(role => GetPermissionForRole(Configuration.Roles, role))
                    .Where(permission => permission != null)
                    .ToArray();

            return GetAllowedUrls(permissions);
        }

        public string[] GetAllowedUrls(params Permission[] permissions)
        {
            var allowedUrls = new List<string>();
            foreach (var permission in permissions)
            {
                allowedUrls.AddRange(permission.Permissions);
                if (permission.Roles != null)
                {
                    foreach (var role in permission.Roles)
                    {
                        allowedUrls.AddRange(GetAllowedUrls(role.Value));
                    }
                }
            }
            return allowedUrls.Distinct().ToArray();
        }

        private static Permission GetPermissionForRole(IReadOnlyDictionary<string, Permission> configurationRoles, string userRole)
        {

            Permission permission;
            if (configurationRoles.TryGetValue(userRole, out permission)) //Search in base
                return permission;
            
            //Search in all child
            foreach (var role in configurationRoles)
            {
                if (role.Value?.Roles == null) continue;
                var value = GetPermissionForRole(role.Value.Roles, userRole);
                if (value != null)
                    return value;
            }
            return null;
        }
    }
}