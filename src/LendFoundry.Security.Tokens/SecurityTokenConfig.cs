using System;

namespace LendFoundry.Security.Tokens
{
    public class SecurityTokenConfig : ISecurityTokenConfig
    {
        private string RawSecretKey { get; set; }

        public SecurityTokenConfig()
        {
            RawSecretKey = Environment.GetEnvironmentVariable($"SECRET_KEY");
            if (string.IsNullOrWhiteSpace(RawSecretKey))
                throw new ArgumentNullException(nameof(RawSecretKey));
        }

        public byte[] SecretKey => Convert.FromBase64String(RawSecretKey);
    }
}