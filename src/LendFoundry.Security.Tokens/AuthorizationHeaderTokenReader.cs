#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
using System.Linq;

namespace LendFoundry.Security.Tokens
{
    public class AuthorizationHeaderTokenReader : AuthorizationHeaderBase, ITokenReader
    {
        public AuthorizationHeaderTokenReader(IHttpContextAccessor accessor)
        {
            Accessor = accessor;
        }

        private IHttpContextAccessor Accessor { get; }

        public string Read()
        {
            if (Accessor.HttpContext == null)
                throw new InvalidTokenException("Cannot read Token from Authorization Header because HTTP context is not available.");

            var headers = Accessor.HttpContext.Request.Headers;
            if (!headers.ContainsKey(Authorization) || string.IsNullOrEmpty(headers[Authorization]))
                throw new InvalidTokenException("Cannot read Token because Authorization Header not present or it is null/empty.");

            var value = headers[Authorization].FirstOrDefault();

            if (string.IsNullOrEmpty(value) || !value.StartsWith(Bearer))
                throw new InvalidTokenException("The Authorization Header is not a valid Bearer Token.");

            return value.Replace(Bearer, string.Empty).Trim();
        }
    }
}