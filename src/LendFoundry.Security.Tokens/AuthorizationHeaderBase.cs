namespace LendFoundry.Security.Tokens
{
    public abstract class AuthorizationHeaderBase
    {
        protected const string Authorization = "Authorization";
        protected const string Bearer = "Bearer ";
    }
}