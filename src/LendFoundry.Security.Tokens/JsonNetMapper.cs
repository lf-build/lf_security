using Jose;
using Newtonsoft.Json;

namespace LendFoundry.Security.Tokens
{
    public class JsonNetMapper : IJsonMapper
    {
        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public T Parse<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}