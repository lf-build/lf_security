﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
using System;
using System.Linq;

namespace LendFoundry.Security.Tokens
{
    public class PreCachedAuthorizationHeaderTokenReader : AuthorizationHeaderBase, ITokenReader
    {
        public PreCachedAuthorizationHeaderTokenReader(IHttpContextAccessor accessor)
        {
            if (accessor == null)
                throw new ArgumentNullException(nameof(accessor));

            IsHttpContextAvailable = accessor.HttpContext != null;
            IsAuthorizationHeaderAvailable = IsHttpContextAvailable && ContainsAuthorizationHeader(accessor.HttpContext);

            if (IsAuthorizationHeaderAvailable)
            {
                var authorizationHeader = GetAuthorizationHeader(accessor.HttpContext);

                IsAuthorizationHeaderValid = !string.IsNullOrWhiteSpace(authorizationHeader) && authorizationHeader.StartsWith(Bearer);

                if (IsAuthorizationHeaderValid)
                {
                    Token = authorizationHeader.Replace(Bearer, string.Empty).Trim();
                }
            }
        }

        private string Token { get; }

        private bool IsHttpContextAvailable { get; }

        private bool IsAuthorizationHeaderAvailable { get; }

        private bool IsAuthorizationHeaderValid { get; }

        public string Read()
        {
            if (!IsHttpContextAvailable)
                throw new InvalidTokenException("Cannot read Token from Authorization Header because HTTP context is not available.");

            if (!IsAuthorizationHeaderAvailable)
                throw new InvalidTokenException("Cannot read Token because Authorization Header not present or it is null/empty.");

            if (!IsAuthorizationHeaderValid)
                throw new InvalidTokenException("The Authorization Header is not a valid Bearer Token.");

            return Token;
        }

        private bool ContainsAuthorizationHeader(HttpContext httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));

            return httpContext.Request.Headers.ContainsKey(Authorization)
                && !string.IsNullOrWhiteSpace(GetAuthorizationHeader(httpContext));
        }

        private string GetAuthorizationHeader(HttpContext httpContext)
        {
            return httpContext.Request.Headers[Authorization].FirstOrDefault();
        }
    }
}
