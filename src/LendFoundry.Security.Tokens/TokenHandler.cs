using System;
using Jose;

namespace LendFoundry.Security.Tokens
{
    public class TokenHandler : ITokenHandler
    {
        public TokenHandler(ISecurityTokenConfig config, ITokenReader reader, ITokenWriter writer)
        {
            Reader = reader;
            Writer = writer;
#if DOTNET2
            JWT.DefaultSettings.JsonMapper = new JsonNetMapper();
#else
            JWT.JsonMapper = new JsonNetMapper();
#endif
            Configuration = config ?? new SecurityTokenConfig();
        }

        private ITokenReader Reader { get; }

        private ITokenWriter Writer { get; }

        private ISecurityTokenConfig Configuration { get; }

        public IToken Issue(string tenant, string issuer)
        {
            return Issue(tenant, issuer, null, null, null);
        }

        public IToken Issue(string tenant, string issuer, DateTime? expiration, string subject, string[] scope)
        {
            var token = new Token
            {
                Tenant = tenant,
                Issuer = issuer,
                IssuedAt = DateTime.UtcNow,
                Expiration = expiration,
                Subject = subject,
                Scope = scope
            };

            token.Value = JWT.Encode(token, Configuration.SecretKey, JwsAlgorithm.HS256);

            return token;
        }

        public void Write(IToken token)
        {
            Writer.Write(token);
        }

        public IToken Parse(string value = null)
        {
            try
            {
                value = value ?? Reader.Read();
                var token = JWT.Decode<Token>(value, Configuration.SecretKey);
                token.Value = value;
                return token;
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch(Exception)
            {
                return null;
            }
        }
    }
}