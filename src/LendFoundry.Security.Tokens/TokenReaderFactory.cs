namespace LendFoundry.Security.Tokens
{
    public class TokenReaderFactory : ITokenReaderFactory
    {
        public TokenReaderFactory(ITokenHandler handler)
        {
            Handler = handler;
        }

        private ITokenHandler Handler { get; }

        public ITokenReader Create(string tenant, string issuer)
        {
            var token = Handler.Issue(tenant, issuer);
            return new StaticTokenReader(token.Value);
        }
    }
}