using System;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
namespace LendFoundry.Security.Tokens
{
    public class AuthorizationHeaderTokenWriter : AuthorizationHeaderBase, ITokenWriter
    {
        public AuthorizationHeaderTokenWriter(IHttpContextAccessor accessor)
        {
            Accessor = accessor;
        }

        private IHttpContextAccessor Accessor { get; set; }


        public void Write(IToken token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            if (Accessor.HttpContext == null)
                throw new Exception("Cannot write Token on Authorization Header because HTTP context is not available.");

            var headers = Accessor.HttpContext.Request.Headers;
            var bearerToken = string.Concat(Bearer, token.Value);
            if (headers.ContainsKey(Authorization))
                headers[Authorization] = bearerToken;
            else
                headers.Add(Authorization, new[] {bearerToken});
        }
    }
}