#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Security.Tokens
{
    public static class TokenHandlerExtensions
    {
        public static IServiceCollection AddTokenHandler(this IServiceCollection services)
        {
            services.AddTransient<ITokenReader, AuthorizationHeaderTokenReader>();
            return AddCommonDependencies(services);
        }

        public static IServiceCollection AddPreCachedHttpTokenHandler(this IServiceCollection services)
        {
            services.AddTransient<ITokenReader, PreCachedAuthorizationHeaderTokenReader>();
            return AddCommonDependencies(services);
        }

        private static IServiceCollection AddCommonDependencies(this IServiceCollection services)
        {
            services.AddTransient<ISecurityTokenConfig, SecurityTokenConfig>();
            services.AddTransient<ITokenWriter, AuthorizationHeaderTokenWriter>();
            services.AddTransient<ITokenHandler, TokenHandler>();
            services.AddTransient<ITokenReaderFactory, TokenReaderFactory>();
            return services;
        }
    }
}