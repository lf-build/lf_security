﻿namespace LendFoundry.Security.Identity
{
    public interface IRequestPasswordResetRequest
    {
        string Username { get; set; }
        string Portal { get; set; }
        object Data { get; set; }
    }
}
