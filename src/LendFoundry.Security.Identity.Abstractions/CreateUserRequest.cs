namespace LendFoundry.Security.Identity
{
    public interface ICreateUserRequest
    {
         string Name { get; set; }
         string Email { get; set; }
         string Username { get; set; }
         string[] Roles { get; set; }
         string Password { get; set; }
         string PasswordSalt { get; set; } 
         bool IsActive { get; set; }
    }
}