﻿using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public class UpdateUserRoles
    {
        public string UserName { get; set; }
        public List<string> Roles { get; set; }
      
    }
}