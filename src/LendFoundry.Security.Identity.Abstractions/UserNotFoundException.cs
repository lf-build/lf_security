using System;

namespace LendFoundry.Security.Identity
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(string username, string message): base(message)
        {
            Username = username;
        }

        public UserNotFoundException(string username) 
        {
            Username = username;
        }

        public string Username { get; set; }
    }
}