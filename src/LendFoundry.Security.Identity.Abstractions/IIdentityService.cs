﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IIdentityService
    {
        Task<bool> IsUsernameAvailable(string username);

        Task<IUserInfo> CreateUser(ICreateUserRequest request);

        Task<IEnumerable<IUserInfo>> GetAllUsers();

        Task<IUser> GetUser(string username);

        Task<IUserInfo> UpdateUser(IUserInfo user);

        Task RemoveUser(string username);

        Task ChangePassword(string username, string currentPassword, string newPassword);

        Task RequestPasswordReset(IRequestPasswordResetRequest requestPasswordResetRequest);

        Task ResetPassword(string username, string token, string password);

        Task<ILoginResponse> Login(ILoginRequest loginRequest);

        Task<bool> IsAuthenticated(string token);

        Task<bool> IsAuthorized(string token, string uri);

        Task<bool> IsExpiring();
        
        Task<string> RenewToken();

        Task Logout(string token);

        Task<IEnumerable<string>> GetAllRolesFromCurrentUser();

        Task<IEnumerable<string>> GetUserRoles(string username);

        Task<IEnumerable<string>> GetAllRoles();

        Task<IEnumerable<string>> GetChildRoles(string role);

        Task<IEnumerable<string>> GetChildrenRoles(IEnumerable<string> role);

        Task<IEnumerable<IUser>> GetRoleMembers(string role);

        Task<IUserInfo> GetCurrentUser();
        Task<IEnumerable<string>> GetParentRoles(string role);
        Task ChangeName(string username, string name);

        Task<IEnumerable<IUserInfo>> GetUsersByRoles(List<string> roles);
        Task<IEnumerable<IUserInfo>> GetUsersByPasswordExpiring();
        Task ActivateUser(string userName);

        Task DeActivateUser(string userName);

        Task AssignRolesToUser(string userName, List<string> roles);

        Task RemoveRolesFromUser(string userName, List<string> roles);

        Task<IUserInfo> GetUserById(string id);
        
        Task<ILoginResponse> Login(IOtpLoginRequest OtpLoginRequest);
        Task<IOtpRequest> SendOtp(IOtpRequest sendOtpRequest);
    }
}