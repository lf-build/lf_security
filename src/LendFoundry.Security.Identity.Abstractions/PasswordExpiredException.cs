using System;

namespace LendFoundry.Security.Identity
{
    public class PasswordExpiredException: Exception
    {
        public string Username { get; }

        public PasswordExpiredException(string username,string message) : base(message)
        {
            Username = username;
        }
        
    }
}