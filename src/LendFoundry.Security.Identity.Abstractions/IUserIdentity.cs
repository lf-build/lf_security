﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IUserIdentity
    {
        string UserId { get; }
        string Token { get; }
        TimeBucket TokenExpiration { get; }
    }
}
