namespace LendFoundry.Security.Identity
{
    public class UserEvent
    {
        public UserEvent(string username,string userId)
        {
            Username = username;
            UserId = userId;
        }

        public string Username { get; set; }

        public string UserId { get; set; }
    }
}