namespace LendFoundry.Security.Identity
{
    public interface IRoleInfo
    {
        string Name { get; set; }
    }
}