using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public class Configuration : IConfiguration, IDependencyConfiguration
    {
        public int SessionTimeout { get; set; }
        public int TokenTimeout { get; set; }
        public CaseInsensitiveDictionary<Permission> Roles { get; set; }
        public string ResetPasswordTemplate { get; set; }
        public string ResetPasswordTemplateVersion { get; set; }
        public string MaxFailedLoginAttemptsErrorMessage { get; set; }
        public int MaxFailedLoginAttempts { get; set; }
        public int AccountLockoutPeriod { get; set; }
        public string AccountLockoutMessage { get; set; }
        public int PasswordHistoryLength { get; set; } = 0;
        public string PasswordComplexityPattern { get; set; }
        public string PasswordComplexityErrorMessage { get; set; }
        public int PasswordExpirationDays { get; set; } = 0;
        public int PasswordExpirationThreshold { get; set; } = 5;
        public int OtpExpirationMinutes { get; set; }
        public string PhonePrefix { get; set; }
        public int TokenExpirationThreshold { get; set; } = 2;
        public string Otpsmstemplatename { get; set; }
        public bool GenerateOtp { get; set; } = true;
        public bool ThirdPartyOtp { get; set; } = false;
        public bool ActiveProcessTemplate { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}