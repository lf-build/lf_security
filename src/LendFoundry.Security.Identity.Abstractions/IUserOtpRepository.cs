using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IUserOtpRepository
    {
        Task Add(IUserOtp userOtp);

        Task Update(IUserOtp userOtp);

        Task<IUserOtp> GetByUserName(string userName);

        Task<IUserOtp> GetByOtpCode(string userName, string otpCode);

        Task Delete(string userName);
    }
}