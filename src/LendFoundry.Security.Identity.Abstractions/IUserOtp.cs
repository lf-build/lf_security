﻿using System;

namespace LendFoundry.Security.Identity
{
    public interface IUserOtp 
    {

         string Id { get; set; }
         string TenantId { get; set; }
        string Username { get; set; }
    
        string OtpCode { get; set; }
        DateTimeOffset ExpirationDate { get; set; }

    }
}
