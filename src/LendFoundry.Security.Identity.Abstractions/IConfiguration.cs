using LendFoundry.Foundation.Client;

namespace LendFoundry.Security.Identity
{
    public interface IConfiguration : IDependencyConfiguration
    {
        int SessionTimeout { get; set; }
        int TokenTimeout { get; set; }
        CaseInsensitiveDictionary<Permission> Roles { get; set; }
        string ResetPasswordTemplate { get; set; }
        string ResetPasswordTemplateVersion { get; set; }
        string MaxFailedLoginAttemptsErrorMessage { get; set; }
        int MaxFailedLoginAttempts { get; set; }
        int AccountLockoutPeriod { get; set; }
        string AccountLockoutMessage { get; set; }
        int PasswordHistoryLength { get; set; }
        string PasswordComplexityPattern { get; set; }
        string PasswordComplexityErrorMessage { get; set; }
        int PasswordExpirationDays { get; set; }
        int PasswordExpirationThreshold { get; set; }
        int OtpExpirationMinutes { get; set; }
        string PhonePrefix { get; set; }
        int TokenExpirationThreshold { get; set; }
        string Otpsmstemplatename { get; set; }
        bool GenerateOtp { get; set; }
        bool ThirdPartyOtp { get; set; }
        bool ActiveProcessTemplate { get; set; }
        string ConnectionString { get; set; }
    }
}