namespace LendFoundry.Security.Identity
{
    public interface ICrypt
    {
        string CreateSalt();

        string Encrypt(string value, string salt);
    }
}