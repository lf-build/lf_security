using System;

namespace LendFoundry.Security.Identity
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "security-identity";
    }
}