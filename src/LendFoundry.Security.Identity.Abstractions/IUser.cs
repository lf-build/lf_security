﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public interface IUser : IUserInfo
    {
        string Password { get; set; }
        string PasswordSalt { get; set; }
        string ResetToken { get; set; }
        DateTime? ResetTokenExpiration { get; set; }
        int FailedLoginAttempts { get; set; }
        DateTimeOffset? LastFailedLoginAttempt { get; set; }       
        List<UserPassword> PasswordHistory {get;set;}
    }
}
