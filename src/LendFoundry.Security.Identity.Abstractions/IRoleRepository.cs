using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public interface IRoleRepository
    {
        ICollection<IRoleInfo> GetUserRoles(string username);

        IRoleInfo CreateRole(string name);

        ICollection<IRoleInfo> GetAllRoles();

        void AddMembers(string role, ICollection<IUserInfo> users);

        void RemoveMembers(string role, ICollection<IUserInfo> users);

        ICollection<IUserInfo> GetRoleMembers(string role);

        void RemoveRole(string name);
    }
}