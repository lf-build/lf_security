﻿namespace LendFoundry.Security.Identity
{
    public class UserPassword
    {
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
    }
}
