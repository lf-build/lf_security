﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IOtpRequest
    {
        string Username { get; set; }
    }
}
