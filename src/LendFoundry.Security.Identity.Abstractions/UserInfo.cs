﻿using System;

namespace LendFoundry.Security.Identity
{
    public class UserInfo : IUserInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string[] Roles { get; set; }
        public bool IsActive { get; set; }
        public DateTimeOffset? PasswordExpirationDate { get; set; }
    }
}