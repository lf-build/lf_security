﻿namespace LendFoundry.Security.Identity
{
    public class PasswordChangeRequest
    {
        public string CurrentPassword { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}