﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public class SendOtpRequest : IOtpRequest
    {
        public string Username { get; set; }
    }
}
