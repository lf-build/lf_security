﻿namespace LendFoundry.Security.Identity
{
    public class AlloyConfiguration
    {
        public string Login { get; set; }

        public CaseInsensitiveDictionary<string> SecurityResetUrls { get; set; }
    }
}
