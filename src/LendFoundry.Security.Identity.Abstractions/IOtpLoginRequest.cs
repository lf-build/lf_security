﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IOtpLoginRequest
    {
        string Username { get; set; }

        string Otp { get; set; }

        string Portal { get; set; }
    }
}
