namespace LendFoundry.Security.Identity
{
    public interface ILoginRequest
    {
        string Username { get; set; }

        string Password { get; set; }

        string Portal { get; set; }
    }
}