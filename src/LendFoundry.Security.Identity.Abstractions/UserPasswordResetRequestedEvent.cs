namespace LendFoundry.Security.Identity
{
    public class UserPasswordResetRequestedEvent : UserEvent
    {
        public UserPasswordResetRequestedEvent(string username,string userid, string token) : base(username,userid)
        {
            Token = token;
        }

        public string Token { get; set; }
    }
}