﻿using System.Collections.Generic;

namespace LendFoundry.Security.Identity
{
    public interface IPermission
    {
        List<string> Permissions { get; set; }

        Dictionary<string, Permission> Roles { get; set; }
    }
}
