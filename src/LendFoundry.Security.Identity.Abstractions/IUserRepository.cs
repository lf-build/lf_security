using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public interface IUserRepository
    {
        Task Add(IUser user);

        Task<ICollection<IUser>> All();

        Task<IUser> Get(string username);

        Task Update(IUserInfo user);

        Task Remove(IUserInfo user);

        Task ChangePassword(IUserInfo user, string password, DateTimeOffset? passwordExpirationDate, List<UserPassword> oldPasswords);

        Task SetResetToken(IUser user, string token, DateTime expiration);

        Task<IEnumerable<IUser>> AllWithRole(string role);

        Task ChangeName(IUserInfo user, string name);

        Task SetIsActive(string name, bool isActive);     

        Task AddOrUpdateUserRoles(string name, string[] roles);    

        Task<ICollection<IUser>> GetUsersByRoles(List<string> roles);

        Task<IUser> GetById(string id);

        Task UpdateFailedLoginAttempts(IUser user);

        Task<IEnumerable<IUserInfo>> GetUsersByPasswordExpiring(int threshold);
    }
}