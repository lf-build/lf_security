﻿namespace LendFoundry.Security.Identity
{
    public interface ILoginResponse
    {
        string UserId { get; set; }
        string Token { get; set; }
    }
}