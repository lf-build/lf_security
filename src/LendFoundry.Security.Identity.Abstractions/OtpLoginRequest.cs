﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Security.Identity
{
    public class OtpLoginRequest : IOtpLoginRequest
    {
       public string Username { get; set; }
   

       public string Portal { get; set; }

        public string Otp { get; set; }
    }
}
