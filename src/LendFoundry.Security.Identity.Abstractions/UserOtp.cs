using System;

namespace LendFoundry.Security.Identity
{
    public class UserOtp :  IUserOtp
    {
        public string Id { get; set; }
        public string TenantId { get; set; }

        public string Username { get; set; }

        public string OtpCode { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }

    }
}