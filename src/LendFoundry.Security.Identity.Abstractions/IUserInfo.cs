﻿using System;

namespace LendFoundry.Security.Identity
{
    public interface IUserInfo
    {
        string Id { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        string Username { get; set; }
        string[] Roles { get; set; }
        bool IsActive { get; set; }
        DateTimeOffset? PasswordExpirationDate { get; set; }
    }
}