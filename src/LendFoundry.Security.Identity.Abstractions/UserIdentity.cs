﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Security.Identity
{
    public class UserIdentity : IUserIdentity
    {
        public string UserId { get; set; }

        public string Token { get; set; }

        public TimeBucket TokenExpiration { get; set; }
    }
}
