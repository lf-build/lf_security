﻿namespace LendFoundry.Security.Identity
{
    public class LoginResponse : ILoginResponse
    {
        public string UserId { get; set; }
        public string Token { get; set; }
    }
}