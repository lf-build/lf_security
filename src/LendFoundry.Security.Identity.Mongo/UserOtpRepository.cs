﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson;
using System.Collections.Concurrent;
using LendFoundry.Foundation.Persistence.Mongo;

namespace LendFoundry.Security.Identity.Mongo
{
    public class UserOtpRepository : IUserOtpRepository
    {
        private static ConcurrentDictionary<string, IMongoCollection<UserOtp>> Collections = new ConcurrentDictionary<string, IMongoCollection<UserOtp>>();

        static UserOtpRepository()
        {
            BsonClassMap.RegisterClassMap<UserOtp>(map =>
            {
                map.AutoMap();
                map.MapIdField(m => m.Id).SetIdGenerator(StringObjectIdGenerator.Instance);
            });
        }

        public UserOtpRepository(IMongoConfiguration configuration, ITenantService tenantService)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tenantService == null)
                throw new ArgumentNullException(nameof(tenantService));

            if (string.IsNullOrEmpty(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrEmpty(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");

            var key = configuration.ConnectionString + configuration.Database;
            IMongoCollection<UserOtp> tmpCollection;
            if (!Collections.TryGetValue(key, out tmpCollection))
            {
                var client = new MongoClient(configuration.ConnectionString);
                var database = client.GetDatabase(configuration.Database);
                tmpCollection = database.GetCollection<UserOtp>("user-otp");

                tmpCollection.Indexes.CreateOneAsync(Builders<UserOtp>.IndexKeys.Ascending(u => u.TenantId).Ascending(u => u.Username), new CreateIndexOptions { Unique = true });
                Collections.TryAdd(key, tmpCollection);
            }

            Collection = tmpCollection;


            TenantService = tenantService;

        }

        private IMongoCollection<UserOtp> Collection { get; }

        private ITenantService TenantService { get; }

        public async Task Add(IUserOtp userOtp)
        {
            if (userOtp == null)
                throw new ArgumentNullException(nameof(userOtp));
            userOtp.Id = ObjectId.GenerateNewId().ToString();

            var entry = new UserOtp
            {
                Id = userOtp.Id,
                TenantId = TenantService.Current.Id,
                OtpCode = userOtp.OtpCode,
                Username = userOtp.Username.ToLower(),
                ExpirationDate = DateTimeOffset.UtcNow
            };

            await Collection.InsertOneAsync(entry);
        }

        public async Task Update(IUserOtp userOtp)
        {
            if (userOtp == null)
                throw new ArgumentNullException(nameof(userOtp));

            UpdateDefinition<UserOtp> definition = null;
            var builder = new UpdateDefinitionBuilder<UserOtp>();

            if (!string.IsNullOrEmpty(userOtp.OtpCode))
                definition = builder.Set(u => u.OtpCode, userOtp.OtpCode);


            definition = definition.Set(u => u.ExpirationDate, userOtp.ExpirationDate);

            if (definition != null)
                await Collection.UpdateOneAsync(FindByUsername(userOtp.Username), definition);
        }

        private Expression<Func<UserOtp, bool>> FindByUsername(string username)
        {
            username = username.ToLower();
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Username == username;
        }

        public async Task<IUserOtp> GetByOtpCode(string userName,string otpCode)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

            if (string.IsNullOrEmpty(otpCode))
                throw new ArgumentNullException(nameof(otpCode));

        
            return await Collection.Find(GetByUserNameOtp(userName, otpCode)).FirstOrDefaultAsync();

         
        }

        public async Task Delete(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

            await Collection.DeleteOneAsync(FindByUsername(userName));

        }

        public async Task<IUserOtp> GetByUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException(nameof(userName));

        
            return await Collection.Find(FindByUsername(userName)).FirstOrDefaultAsync();


        }

        private Expression<Func<UserOtp, bool>> GetByUserNameOtp(string userName,string otpCode)
        {
            userName = userName.ToLower();
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Username == userName && candidate.OtpCode == otpCode;
        }
    }
}
