using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace LendFoundry.Security.Identity.Mongo
{
    public class SessionManager : ISessionManager
    {
        private static ConcurrentDictionary<string, IMongoCollection<Session>> Collections = new ConcurrentDictionary<string, IMongoCollection<Session>>();

        static SessionManager()
        {
            BsonClassMap.RegisterClassMap<Session>(map =>
            {
                map.AutoMap();
                map.MapIdField(m => m.Id).SetIdGenerator(StringObjectIdGenerator.Instance);
            });
        }

        public SessionManager(IConfiguration configuration, IMongoConfiguration mongoConfiguration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if(mongoConfiguration == null)
                throw new ArgumentNullException(nameof(mongoConfiguration));

            var key = mongoConfiguration.ConnectionString + mongoConfiguration.Database;
            IMongoCollection<Session> tmpCollection;
            if (!Collections.TryGetValue(key, out tmpCollection))
            {
                var client = new MongoClient(mongoConfiguration.ConnectionString);
                var database = client.GetDatabase(mongoConfiguration.Database);
                tmpCollection = database.GetCollection<Session>("sessions");
                tmpCollection.Indexes.CreateOneAsync(Builders<Session>.IndexKeys.Ascending(u => u.Value), new CreateIndexOptions { Unique = true });
                Collections.TryAdd(key, tmpCollection);
            }

            Collection = tmpCollection;

        }

        private IMongoCollection<Session> Collection { get; }

        public async Task<string> Create(IToken token)
        {
            var session = new Session
            {
                Issuer = token.Issuer,
                IssuedAt = token.IssuedAt,
                Expiration = token.Expiration,
                Subject = token.Subject,
                Tenant = token.Tenant,
                Scope = token.Scope,
                Value = token.Value
            };

            await Collection.DeleteManyAsync(s => s.Subject == session.Subject);
            await Collection.InsertOneAsync(session);

            return token.Value;
        }

        public async Task<bool> IsValid(string token)
        {
            var session = await Collection.AsQueryable().Where(s => s.Value == token).FirstOrDefaultAsync();
            if (session == null || session.Expiration <= DateTime.UtcNow)
            {
                await Expire(token);
                return false;
            }
            return true;
        }

        public async Task<bool> Expire(string token)
        {
            return await Collection.FindOneAndDeleteAsync(s => s.Value == token) != null;
        }
    }
}