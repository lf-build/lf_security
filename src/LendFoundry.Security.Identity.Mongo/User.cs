using System;
using System.Collections.Generic;

namespace LendFoundry.Security.Identity.Mongo
{
    public class User : IUser
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string[] Roles { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string ResetToken { get; set; }
        public DateTime? ResetTokenExpiration { get; set; }
        public bool IsActive { get; set; }
        public int FailedLoginAttempts { get; set; }
        public DateTimeOffset? LastFailedLoginAttempt { get; set; }
        public DateTimeOffset? PasswordExpirationDate { get; set; }
        public List<UserPassword> PasswordHistory { get; set; }
    }
}