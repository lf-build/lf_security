using System;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Security.Identity.Mongo
{
    public class Session : IToken
    {
        
        public string Id { get; set; }

        public string Issuer { get; set; }

        public DateTime IssuedAt { get; set; }

        public DateTime? Expiration { get; set; }

        public string Subject { get; set; }

        public string Tenant { get; set; }

        public string Value { get; set; }

        public string[] Scope { get; set; }

        public bool IsValid { get;  }
    }
}