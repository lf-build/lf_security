﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Serializers;
using System.Collections.Concurrent;
using LendFoundry.Foundation.Persistence.Mongo;

namespace LendFoundry.Security.Identity.Mongo
{
    public class UserRepository : IUserRepository
    {
        private static ConcurrentDictionary<string, IMongoCollection<User>> Collections = new ConcurrentDictionary<string, IMongoCollection<User>>();

        static UserRepository()
        {
            BsonClassMap.RegisterClassMap<User>(map =>
            {
                map.AutoMap();
                map.MapIdField(m => m.Id).SetIdGenerator(StringObjectIdGenerator.Instance);
                map.MapMember(m => m.LastFailedLoginAttempt).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.PasswordExpirationDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
            });
        }

        public UserRepository(IMongoConfiguration configuration, ITenantService tenantService)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (tenantService == null)
                throw new ArgumentNullException(nameof(tenantService));

            if (string.IsNullOrEmpty(configuration.ConnectionString))
                throw new ArgumentException("Connection String cannot be empty");

            if (string.IsNullOrEmpty(configuration.Database))
                throw new ArgumentException("Database name cannot be empty");



            var key = configuration.ConnectionString + configuration.Database;
            IMongoCollection<User> tmpCollection;
            if (!Collections.TryGetValue(key, out tmpCollection))
            {
                var client = new MongoClient(configuration.ConnectionString);
                var database = client.GetDatabase(configuration.Database);
                tmpCollection = database.GetCollection<User>("users");

                tmpCollection.Indexes.CreateOneAsync(Builders<User>.IndexKeys.Ascending(u => u.TenantId).Ascending(u => u.Username), new CreateIndexOptions { Unique = true });
                Collections.TryAdd(key, tmpCollection);
            }

            Collection = tmpCollection;

            TenantService = tenantService;
        }

        private IMongoCollection<User> Collection { get; }

        private ITenantService TenantService { get; }

        public async Task Add(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            user.Id = ObjectId.GenerateNewId().ToString();

            var entry = new User
            {
                Id = user.Id,
                TenantId = TenantService.Current.Id,
                Email = user.Email,
                Name = user.Name,
                Username = user.Username.ToLower(),
                Roles = user.Roles,
                Password = user.Password,
                PasswordSalt = user.PasswordSalt,
                ResetToken = user.ResetToken,
                ResetTokenExpiration = user.ResetTokenExpiration,
                IsActive = user.IsActive,
                PasswordExpirationDate = user.PasswordExpirationDate
            };

            await Collection.InsertOneAsync(entry);
        }

        public async Task<ICollection<IUser>> All()
        {
            var builder = new FilterDefinitionBuilder<User>();
            var filter = builder.Eq(candidate => candidate.TenantId, TenantService.Current.Id);

            var found = await Collection.FindAsync(filter);
            var users = await found.ToListAsync();

            return MapUserData(users);
        }

        public async Task<ICollection<IUser>> GetUsersByRoles(List<string> roles)
        {
            var builder = new FilterDefinitionBuilder<User>();
            var tenant = builder.Eq(candidate => candidate.TenantId, TenantService.Current.Id);
            var rolesFilter = builder.AnyIn(candidate => candidate.Roles, roles);
            var filter = builder.And(tenant, rolesFilter);
            var found = await Collection.FindAsync(filter);
            var users = await found.ToListAsync();

            return MapUserData(users);
        }

        public async Task<IUser> Get(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException(nameof(username));

            var user = await Collection
                .AsQueryable()
                .FirstOrDefaultAsync(FindByUsername(username));

            return MapUserData(user);
        }

        public async Task<IUser> GetById(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            var user = await Collection
                .AsQueryable()
                .FirstOrDefaultAsync(FindByUserId(id));

            return MapUserData(user);
        }

        public async Task Update(IUserInfo user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));


            UpdateDefinition<User> definition = null;
            var builder = new UpdateDefinitionBuilder<User>();

            if (!string.IsNullOrEmpty(user.Name))
                definition = builder.Set(u => u.Name, user.Name);

            if (!string.IsNullOrEmpty(user.Email))
                definition = definition.Set(u => u.Email, user.Email);

            if (user.Roles != null && user.Roles.Any())
                definition = definition.Set(u => u.Roles, user.Roles);

            if (definition != null)
                await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task Remove(IUserInfo user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            await Collection.DeleteOneAsync(FindByUsername(user.Username));
        }

        public async Task ChangePassword(IUserInfo user, string password, DateTimeOffset? passwordExpirationDate, List<UserPassword> oldPasswords)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException(nameof(password));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.Password, password)
                .Set(u => u.ResetToken, string.Empty)
                .Set(u => u.ResetTokenExpiration, null)
                .Set(u => u.PasswordExpirationDate, passwordExpirationDate)
                .Set(u => u.PasswordHistory, oldPasswords);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task SetResetToken(IUser user, string token, DateTime expiration)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(token))
                throw new ArgumentNullException(nameof(token));

            if (expiration.ToUniversalTime() < DateTime.UtcNow)
                throw new ArgumentException("Invalid token expiration date. It is in the past.");

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.ResetToken, token)
                .Set(u => u.ResetTokenExpiration, expiration);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task<IEnumerable<IUser>> AllWithRole(string role)
        {
            if (TenantService.Current == null)
                throw new ArgumentNullException($"The TenantInfo is empty. {nameof(TenantService.Current)}");

            var tenantId = TenantService.Current.Id;
            if (string.IsNullOrWhiteSpace(tenantId))
                throw new ArgumentException("Was not possible to get the tenant id.");

            var builder = new FilterDefinitionBuilder<User>();
            var tenant = builder.Eq(candidate => candidate.TenantId, tenantId);
            var roles = builder.AnyIn(candidate => candidate.Roles, new[] { role });
            var filter = builder.And(tenant, roles);

            var found = await Collection.FindAsync(filter);
            var users = await found.ToListAsync();
            return MapUserData(users);
        }

        public async Task ChangeName(IUserInfo user, string name)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.Name, name);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task SetIsActive(string name, bool isActive)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.IsActive, isActive);

            await Collection.UpdateOneAsync(FindByUsername(name), definition);
        }

        public async Task AddOrUpdateUserRoles(string name, string[] roles)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.Roles, roles);

            await Collection.UpdateOneAsync(FindByUsername(name), definition);
        }

        public async Task UpdateFailedLoginAttempts(IUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            var definition = new UpdateDefinitionBuilder<User>()
                .Set(u => u.FailedLoginAttempts, user.FailedLoginAttempts)
                .Set(u => u.LastFailedLoginAttempt, user.LastFailedLoginAttempt);

            await Collection.UpdateOneAsync(FindByUsername(user.Username), definition);
        }

        public async Task<IEnumerable<IUserInfo>> GetUsersByPasswordExpiring(int threshold)
        {
            var passwordExpirationThreasHold = DateTimeOffset.UtcNow.Date.AddDays(threshold);
            var builder = new FilterDefinitionBuilder<User>();
            var tenant = builder.Eq(user => user.TenantId, TenantService.Current.Id);
            var rolesFilter = builder.Lte(user => user.PasswordExpirationDate, passwordExpirationThreasHold);
            var excludeFilter = builder.Gte(user => user.PasswordExpirationDate, DateTimeOffset.UtcNow.Date);
            var filter = builder.And(tenant, rolesFilter, excludeFilter);
            var found = await Collection.FindAsync(filter);
            var users = await found.ToListAsync();
            return MapUserData(users);
        }

        private Expression<Func<User, bool>> FindByUsername(string username)
        {
            username = username.ToLower();
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Username == username;
        }

        private Expression<Func<User, bool>> FindByUserId(string userId)
        {
            return candidate => candidate.TenantId == TenantService.Current.Id && candidate.Id == userId;
        }

        private User MapUserData(User user)
        {
            if (user == null)
                return null;

            User mappedUserData = new User();
            mappedUserData.Id = user.Id;
            mappedUserData.TenantId = user.TenantId;
            mappedUserData.Name = user.Name;
            mappedUserData.Email = user.Email;
            mappedUserData.Username = user.Username;
            mappedUserData.Roles = user.Roles;
            mappedUserData.Password = user.Password;
            mappedUserData.PasswordSalt = user.PasswordSalt;
            mappedUserData.ResetToken = user.ResetToken;
            mappedUserData.ResetTokenExpiration = user.ResetTokenExpiration;
            mappedUserData.IsActive = user.IsActive;
            mappedUserData.FailedLoginAttempts = user.FailedLoginAttempts;
            mappedUserData.LastFailedLoginAttempt = user.LastFailedLoginAttempt;
            mappedUserData.PasswordExpirationDate = user.PasswordExpirationDate;
            mappedUserData.PasswordHistory = user.PasswordHistory;

            return mappedUserData;
        }

        private List<IUser> MapUserData(List<User> users)
        {
            List<IUser> mappedUserDatas = new List<IUser>();
            foreach (var user in users)
            {
                IUser mappedUserData = new User();
                mappedUserData.Id = user.Id;
                // mappedUserData.TenantId = user.TenantId;
                mappedUserData.Name = user.Name;
                mappedUserData.Email = user.Email;
                mappedUserData.Username = user.Username;
                mappedUserData.Roles = user.Roles;
                mappedUserData.Password = user.Password;
                mappedUserData.PasswordSalt = user.PasswordSalt;
                mappedUserData.ResetToken = user.ResetToken;
                mappedUserData.ResetTokenExpiration = user.ResetTokenExpiration;
                mappedUserData.IsActive = user.IsActive;
                mappedUserData.FailedLoginAttempts = user.FailedLoginAttempts;
                mappedUserData.LastFailedLoginAttempt = user.LastFailedLoginAttempt;
                mappedUserData.PasswordExpirationDate = user.PasswordExpirationDate;
                mappedUserData.PasswordHistory = user.PasswordHistory;

                mappedUserDatas.Add(mappedUserData);
            }

            return mappedUserDatas;
        }
    }
}
