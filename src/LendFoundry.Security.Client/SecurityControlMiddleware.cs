using System;
using System.Threading.Tasks;
using Jil;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System.Linq;

#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Routing.Template;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;

#endif

namespace LendFoundry.Security.Client
{
    public class SecurityControlMiddleware
    {
        public SecurityControlMiddleware(RequestDelegate next, ITokenHandler tokenHandler)
        {
            Next = next;
            TokenHandler = tokenHandler;
        }

        private RequestDelegate Next { get; }

        private ITokenHandler TokenHandler { get; }

        public async Task Invoke(HttpContext context)
        {
            var token = TokenHandler.Parse();

            if (token == null || !token.IsValid)
            {
                var response = context.Response;
                if (response.HasStarted)
                    return;

                response.StatusCode = 403;
                response.ContentType = "application/json";

                var body = JSON.Serialize(new Error(403, "A valid Token is required to access this service."));
                await response.WriteAsync(body);
                return;
            }
            await Next.Invoke(context);
        }

#if DOTNET2
        //private static bool HasAccess(IToken token, HttpContext context)
        //{
        //    if (token.Scope == null || !token.Scope.Any())
        //        return false;

        //    var baseUri = new Uri(context.Request.PathBase.ToUriComponent());
        //    var request = new Uri(context.Request.Path.ToUriComponent());
        //    return token.Scope.Any(scope =>
        //    {
        //        var template = TemplateParser.Parse($"{scope}");
        //        var matcher = new TemplateMatcher(template, GetDefaults(template));
        //        var values = new RouteValueDictionary();
        //        return matcher.TryMatch(request.AbsolutePath, values) &&
        //            template.Segments.Count() == values.Keys.Count();
        //    });
        //}

        //private static RouteValueDictionary GetDefaults(RouteTemplate parsedTemplate)
        //{
        //    var result = new RouteValueDictionary();
        //    var parameters = parsedTemplate.Parameters.Where(p => p.DefaultValue != null);

        //    foreach (var parameter in parameters)
        //    {
        //        result.Add(parameter.Name, parameter.DefaultValue);
        //    }

        //    return result;
        //}

#else
        private static bool HasAccess(IToken token, HttpContext context)
        {
            if (token.Scope == null || !token.Scope.Any())
                return false;

            var baseUri = new Uri(context.Request.PathBase.ToUriComponent());
            var request = new Uri(context.Request.Path.ToUriComponent());
            return token.Scope.Any(scope =>
            {
                var template = new UriTemplate($"{baseUri}/{scope}");
                var match = template.Match(baseUri, request);
                return match != null && template.PathSegmentVariableNames.Count == match.BoundVariables.Count;
            });
        }


#endif

    }
}